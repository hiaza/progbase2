#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <progbase/console.h>
#include <progbase/events.h>

/* custom constant event type ids*/
enum {
    GolosnaLiteraId,
    GolosnaLiteraPovtId
};

typedef struct{
	char prevLetter;
}Letters;

typedef struct{
    char buffer[300];
    int readed;
}Str;
/* event handler functions prototypes */

void fileReader_update(EventHandler * self, Event * event);
void FinderDL_update(EventHandler * self, Event * event);
void PrinterDL_update(EventHandler * self, Event * event);

Letters * Letters_new();

void Letters_free(Letters * x);

Str * Str_new();

void Str_free(Str * temp);

int main(void) {
    // startup event system
    EventSystem_init();

    // add event handlers
    
    EventSystem_addHandler(EventHandler_new(Str_new(), (DestructorFunction)Str_free, fileReader_update));
    EventSystem_addHandler(EventHandler_new(Letters_new(),(DestructorFunction)Letters_free, FinderDL_update));
    EventSystem_addHandler(EventHandler_new(NULL, NULL, PrinterDL_update));

    // start infinite event loop

    EventSystem_loop();

    // cleanup event system

    EventSystem_cleanup();

    return 0;
}


long getFileSize(const char *fileName)
{
    FILE *f = fopen(fileName, "rb");
    if (!f)
        return -1;         
    fseek(f, 0, SEEK_END); 
    long fsize = ftell(f); 
    fclose(f);
    return fsize;
}

int readFileToBuffer(const char *fileName, char *buffer, long bufferLength)
{
    FILE *f = fopen(fileName, "rb");
    
    if (!f) return 0; 
    long readBytes = fread(buffer, 1, bufferLength, f);
    fclose(f);
    return readBytes; 
}

void fileReader_update(EventHandler * self, Event * event){
    char *filePath = "events.txt";
    Str * temp = self -> data;
    switch(event->type){
        case StartEventTypeId:{
            FILE * f = fopen(filePath, "r");
            if(!f){
                printf("before exit\n");
                EventSystem_exit();
            } else{
                    puts(" ");
                    puts("=============================================================================================");
                    puts("Обробник 1 по події старту програми зчитує довільний текстовий файл і на зчитування\nкожної голосної букви генерує повідомлення, що містить цю голосну букву як дані.\nОбробник 2 обробляє такі події і у випадку повторення голосної букви (незалежно від регістру)\nгенерує іншу подію про повторення з інформацією про букви, які повторювались.\nОбробник 3 слухає події обробника 2 і виводить інформацію про них у консоль..");
                    puts("=============================================================================================");
                    long size = getFileSize(filePath);
                    char gt[size + 1];
                    readFileToBuffer(filePath, gt, size);
                    gt[size] = '\0';
                    strcpy(temp ->buffer,gt);
                    fclose(f);
            }
            break;
        }
        case UpdateEventTypeId: {
            char golosni_letters[] = {"aeuioyAEYUIO"};
            char * x = temp -> buffer;
            x = x + temp -> readed;
            if(strchr(golosni_letters,*x)!=0){
                  
                    EventSystem_emit(Event_new(self,GolosnaLiteraId,x,NULL));
        
            }  

            temp -> readed = temp ->readed + 1 ;
            if(strlen(temp->buffer) == temp->readed){
                EventSystem_exit();
            }
            break;
        }
        case ExitEventTypeId: {
            printf("\non exit\n");
            break;
        }
    }
}
void FinderDL_update(EventHandler * self, Event * event){
    Letters * temp = self -> data;
    switch(event->type) {
        case GolosnaLiteraId: {
           
            char letter = *(char *)event->data;
        
            if (temp->prevLetter == letter){

                EventSystem_emit(Event_new(self,GolosnaLiteraPovtId,event->data,NULL));
                temp->prevLetter = '0';

            }
            else temp->prevLetter = letter;
            break;
        }
    }
}
void PrinterDL_update(EventHandler * self, Event * event){
    switch(event->type) {
        case GolosnaLiteraPovtId: {
            char letter = *(char *)event->data;
            printf("\nNew Event >> Key `%c` was doubled\n", letter);
            break;
        }
    }
}

Letters * Letters_new(){
    Letters * temp = malloc(sizeof(Letters));
	temp->prevLetter = '0';
    return temp;
}
void Letters_free(Letters * x){
    free(x);
}

Str * Str_new(){
    Str*temp = malloc(sizeof(Str));
    temp->buffer[0] = '\0';
    temp->readed = 0;
    return temp;
}
void Str_free(Str * temp){
    free(temp);
}