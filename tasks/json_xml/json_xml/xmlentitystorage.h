#ifndef XMLENTITYSTORAGE_H
#define XMLENTITYSTORAGE_H

#include <QtXml>
#include <vector>
#include <iostream>
#include "actor.h"
#include "entitys.h"

using namespace std;

class XmlEntityStorage: public EntityStorage {
public:
     XmlEntityStorage(string & name):EntityStorage(name){}
     vector <Actor> load();
     void save(std::vector<Actor> & entities);
};
#endif // XMLENTITYSTORAGE_H
