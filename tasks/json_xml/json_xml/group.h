#ifndef GROUP
#define GROUP
#include <QDebug>
#include <QtXml>
#include <vector>
#include <iostream>
#include "actor.h"

using namespace std;

class Student {
public:
    QString name;
    int     age;
};

class Group {
public:
    QString name;
    vector <Student> students;
};


#endif // GROUP

