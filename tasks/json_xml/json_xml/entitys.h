#ifndef ENTITYS_H
#define ENTITYS_H
#include <QtXml>
#include <vector>
#include <iostream>
#include "actor.h"

class EntityStorage {
     std::string _name;
protected:
     EntityStorage(std::string & name) { this->_name = name; }
public:
     std::string & name() { return this->_name; }

     virtual std::vector<Actor> load() = 0;
     virtual void save(std::vector<Actor> & entities) = 0;
};

#endif // ENTITYS_H
