#ifndef STORAGEMANAGER_H
#define STORAGEMANAGER_H
#include "entitys.h"

class StorageManager {
public:
    static EntityStorage * createStorage(std::string & storageFileName);
};

#endif // STORAGEMANAGER_H
