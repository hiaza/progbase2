#include "xmlentitys.h"
#include <QDebug>
#include <QtXml>
#include <vector>
#include <iostream>
using namespace std;

void printActor(const Actor & g) {
          cout << " actor: " << g.name.toStdString() << " | " << g.age << " | " << g.rating <<endl;
          cout << " films: (";
          for(const QString & s:g.films){
              cout << s.toStdString() << ", ";
          }
          cout << ")" << endl;
}


string actorToXmlString(const Actor & g) {
    QDomDocument doc;
    QDomElement actorEl = doc.createElement("actor");
    actorEl.setAttribute("name", g.name);
    actorEl.setAttribute("age", g.age);
    actorEl.setAttribute("rating", g.rating);

    for (const QString & s: g.films) {
          QDomElement filmEl = doc.createElement("films");
          filmEl.setAttribute("name", s);
          actorEl.appendChild(filmEl);
    }

    doc.appendChild(actorEl);
    return doc.toString().toStdString();
}

Actor xmlStringToActor(string & str) {
    QDomDocument doc;
    if (!doc.setContent(QString::fromStdString(str))) {
          cerr << "error parsing xml!" << endl;
          return Actor();
    }
    Actor g;
    QDomElement root = doc.documentElement();
    QString nameAttrValue = root.attribute("name");
    g.name = nameAttrValue;
    g.age = root.attribute("age").toInt();
    g.rating = root.attribute("rating").toFloat();

    for (int i = 0; i < root.childNodes().length(); i++) {
          QDomNode filmNode = root.childNodes().at(i);
          QDomElement filmEl = filmNode.toElement();
          QString x = filmEl.attribute("name");
          g.films.push_back(x);
    }

    return g;
}
