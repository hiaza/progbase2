#include <QDebug>
#include <QtXml>
#include <vector>
#include <iostream>
#include "actor.h"
#include "entitys.h"
#include "xmlentitystorage.h"
#include "storagemanager.h"
#include "file.h"
#include "messageexception.h"
using namespace std;


int main(void)
{
    Actor g;
    g.name = "Leonardo Di Caprio";
    g.age = 43;
    g.rating = 9.9;
    g.films.push_back("The Titanic");
    g.films.push_back("The wasted");

    Actor c;
    c.name = "Johnny Depp";
    c.age = 54;
    c.rating = 9.9;
    c.films.push_back("The pirates of the Carribian");
    c.films.push_back("Murder on the Orient Express");

    Actor o;
    o.name = "William Bradley Pitt";
    o.age = 54;
    o.rating = 9.9;
    o.films.push_back("Snach");
    o.films.push_back("The fight club");

    vector<Actor> m;
    m.push_back(g);
    m.push_back(c);
    m.push_back(o);

    vector <EntityStorage * > self;


    string a = "data.json";
    string b = "data.xml";
    string d = "data.fake";
    try{
        self.push_back(StorageManager::createStorage(a));
    }
    catch(MessageException & err){
        cerr << err.what()<<endl;
    }
    try{
        self.push_back(StorageManager::createStorage(b));
    }
    catch(MessageException & err){
        cerr << err.what()<<endl;
    }
    try{
        self.push_back(StorageManager::createStorage(d));
    }
    catch(MessageException & err){
        cerr << err.what()<<endl;
    }

    for(EntityStorage* & s:self){
        cout << "trying to write into file:" << s->name() << " - ";
        try{
             s->save(m);
        }
        catch(MessageException & err){
            cerr << err.what()<<endl;
        }
        cout << "Done!"<<endl << endl;
    }

    cout << "Press Enter to continue";
    getchar();

    for(EntityStorage* & s:self){
         cout << "trying to read from file:" << s->name() << endl;
        try{
             auto x = s->load();
             Actor::printActors(x);
        }
        catch(MessageException & err){
            cerr << err.what()<<endl;
        }
        cout << "Done!"<<endl << endl;
    }

    return 0;
}

