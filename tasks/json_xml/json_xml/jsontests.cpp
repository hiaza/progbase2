#include "jsontests.h"

#include <QDebug>
#include "file.h"
#include <vector>
#include <iostream>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>

using namespace std;


string actorToJsonString(const Actor & g) {
    QJsonDocument doc;
    QJsonObject actorObj;
    actorObj.insert("name", g.name);
    actorObj.insert("age", g.age);
    actorObj.insert("rating", g.rating);
    QJsonArray filmArr;
    for (auto & s: g.films) {
          filmArr.append(s);
    }

    actorObj.insert("films", filmArr);
    doc.setObject(actorObj);

    return doc.toJson().toStdString();
}

Actor jsonStringToActor(string & str) {
    QJsonParseError err;
    QJsonDocument doc = QJsonDocument::fromJson(
           QByteArray::fromStdString(str),
           &err);
    if (err.error != QJsonParseError::NoError) {
            cerr << err.errorString().toStdString() << endl;
    }
    Actor g;
    QJsonObject actorObj = doc.object();
    g.name = actorObj.value("name").toString();
    g.age = actorObj.value("age").toInt();
    g.rating = actorObj.value("rating").toDouble();
    QJsonArray filmArr = actorObj.value("films").toArray();
    for (int i = 0; i < filmArr.size(); i++) {
          QJsonValue value = filmArr.at(i);
          g.films.push_back( value.toString());
    }

    return g;
}

