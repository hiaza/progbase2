#ifndef FILE_H
#define FILE_H

#include <fstream> /* файловий клас */
#include <iostream>
using namespace std;

void saveToFile(const string & fileName, const string & strToSave);
string fromFileToStr(const string & fileName);

bool isSupportedFile(const string & fileName);

#endif // FILE_H
