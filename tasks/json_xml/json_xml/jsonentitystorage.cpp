#include "jsonentitystorage.h"
#include "file.h"
#include <fstream>
#include "messageexception.h"
vector <Actor> JsonEntityStorage :: load(){

    string str = fromFileToStr(this->name());

    ifstream temp(name());
    if(!temp.is_open()){
        string error = " File didn't exist";
        throw(MessageException(error));
        return vector <Actor>();
    }
    temp.close();
    QJsonParseError err;
    QJsonDocument doc = QJsonDocument::fromJson(
           QByteArray::fromStdString(str),
           &err);
    if (err.error != QJsonParseError::NoError) {
            string errr = err.errorString().toStdString();
            throw(MessageException(errr));
            return vector <Actor>();
    }

    vector <Actor> m;
    QJsonObject vectorObj = doc.object();
    QJsonArray  actorArr = vectorObj.value("actors").toArray();
        for (int i = 0; i < actorArr.size(); i++) {
              QJsonValue value = actorArr.at(i);
              QJsonObject actorObj = value.toObject();
              Actor g;
              g.name = actorObj.value("name").toString();
              g.age = actorObj.value("age").toInt();
              g.rating = actorObj.value("rating").toString().toDouble();
              QJsonArray filmArr = actorObj.value("films").toArray();
              for (int i = 0; i < filmArr.size(); i++) {
                    QJsonValue value = filmArr.at(i);
                    g.films.push_back( value.toString());
              }
              m.push_back(g);
        }

    return m;
}

void JsonEntityStorage :: save(std::vector<Actor> & entities){
    if (entities.empty()){
        string err = "error empty vector!";
        throw(MessageException(err));
        return;
    }
        QJsonDocument doc;
        QJsonObject actorsObj;
        QJsonArray actorsArr;
        for (auto & m: entities) {
              QJsonObject mo;
              mo.insert("name", m.name);
              mo.insert("age", m.age);
              mo.insert("rating", QString::number(m.rating));
              QJsonArray filmArr;
              for (auto & s: m.films) {
                    filmArr.append(s);
              }
              mo.insert("films", filmArr);
              actorsArr.append(mo);
        }
        actorsObj.insert("actors", actorsArr);

        doc.setObject(actorsObj);
        saveToFile(this->name(),doc.toJson().toStdString());

}
