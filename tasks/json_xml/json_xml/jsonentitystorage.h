#ifndef JSONENTITYSTORAGE_H
#define JSONENTITYSTORAGE_H
#include "entitys.h"

#include <QtXml>
#include <vector>
#include <iostream>
#include "actor.h"

class JsonEntityStorage: public EntityStorage {
public:
     JsonEntityStorage(string & name):EntityStorage(name){}
     vector <Actor> load();
     void save(std::vector<Actor> & entities);
};

#endif // JSONENTITYSTORAGE_H
