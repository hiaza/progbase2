#ifndef XMLENTITYS_H
#define XMLENTITYS_H
#include <QDebug>
#include <QtXml>
#include <vector>
#include <iostream>
#include "actor.h"

using namespace std;

    string actorToXmlString(const Actor & g);
    Actor xmlStringToActor(string & str);
    void printActor(const Actor & g);

#endif // XMLENTITYS_H
