#include "storagemanager.h"
#include "xmlentitystorage.h"
#include "jsonentitystorage.h"
#include "file.h"
#include "messageexception.h"

EntityStorage * StorageManager :: createStorage(std::string & storageFileName){
    bool x = isSupportedFile(storageFileName);
    string error = "Unknown fileName";
    if(x){
        if(storageFileName.c_str()[storageFileName.length()-4] == '.'){
            return new XmlEntityStorage(storageFileName);
        }
        else return new JsonEntityStorage(storageFileName);

    }
    else {
       throw MessageException(error);
       return NULL;
    }
}

