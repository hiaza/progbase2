#include "xmlentitystorage.h"
#include <QtXml>
#include <vector>
#include <iostream>
#include "actor.h"
#include "file.h"
#include "messageexception.h"

using namespace std;
vector <Actor> XmlEntityStorage :: load(){

    string str = fromFileToStr(this->name());

    ifstream temp(name());
    if(!temp.is_open()){
        string error = " File didn't exist";
        throw(MessageException(error));
        return vector <Actor>();
    }
    temp.close();

    QDomDocument doc;
    if (!doc.setContent(QString::fromStdString(str))) {
          string err = "error parsing xml!";
          throw(MessageException(err));
          return vector<Actor>();
    }
    vector <Actor> g;
    QDomElement root = doc.documentElement();

    for (int i = 0; i < root.childNodes().length(); i++) {
          Actor x;
          QDomNode actorNode = root.childNodes().at(i);
          QDomElement actorEl = actorNode.toElement();
          x.name = actorEl.attribute("name");
          x.age = actorEl.attribute("age").toInt();
          x.rating = actorEl.attribute("rating").toDouble();
          for (int i = 0; i < actorEl.childNodes().length(); i++) {
                QDomNode filmNode = actorEl.childNodes().at(i);
                QDomElement filmEl = filmNode.toElement();
                QString y = filmEl.attribute("name");
                x.films.push_back(y);
          }
          g.push_back(x);
    }
    return g;
}

void XmlEntityStorage :: save(std::vector<Actor> & entities){
    if (entities.empty()){
        string err = "error empty vector!";
        throw(MessageException(err));
        return;
    }
    QDomDocument doc;
    QDomElement actorListEl = doc.createElement("actorList");
    for (size_t i = 0; i < entities.size(); i++) {
          QDomElement actorEl = doc.createElement("actor");
          Actor temp = entities.at(i);
          actorEl.setAttribute("name", temp.name);
          actorEl.setAttribute("age", temp.age);
          actorEl.setAttribute("rating", temp.rating);
          for (const QString & s: temp.films) {
                QDomElement filmEl = doc.createElement("films");
                filmEl.setAttribute("name", s);
                actorEl.appendChild(filmEl);
          }
          actorListEl.appendChild(actorEl);
    }
    doc.appendChild(actorListEl);
    saveToFile(this->name(),doc.toString().toStdString());
}
