#ifndef ACTOR
#define ACTOR


#include <QtXml>
#include <vector>
#include <iostream>

using namespace std;

class Actor {
public:
    QString name;
    int age;
    float rating;
    vector <QString> films;
    static void printActors(vector <Actor> &Act);
};



#endif // ACTOR
