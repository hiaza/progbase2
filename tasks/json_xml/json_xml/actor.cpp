#include "actor.h"

void Actor::printActors(vector <Actor> &Act){
    for(Actor & g: Act){
        cout << " actor: " << g.name.toStdString() << " | " << g.age << " | " << g.rating <<endl;
        cout << " films: (";
        for(const QString & s:g.films){
            cout << s.toStdString() << ", ";
        }
        cout << ")" << endl;
    }
}
