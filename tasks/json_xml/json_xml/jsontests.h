#ifndef JSONTESTS_H
#define JSONTESTS_H
#include "xmlentitys.h"
#include <QDebug>
#include <vector>
#include <iostream>
#include "actor.h"

string actorToJsonString(const Actor & g);
Actor jsonStringToActor(string & str);

#endif // JSONTESTS_H
