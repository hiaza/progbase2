QT += core
QT -= gui
QT += xml

TARGET = json_xml
CONFIG += console
CONFIG -= app_bundle
CONFIG += c++11

TEMPLATE = app

SOURCES += main.cpp \
    file.cpp \
    xmlentitystorage.cpp \
    storagemanager.cpp \
    jsonentitystorage.cpp \
    actor.cpp

HEADERS += \
    actor.h \
    entitys.h \
    file.h \
    xmlentitystorage.h \
    storagemanager.h \
    jsonentitystorage.h \
    messageexception.h
