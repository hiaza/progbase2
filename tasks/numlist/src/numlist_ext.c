#include <numlist_ext.h>

struct __NumList{
    size_t capasity;
    size_t length;
    int * items;

};

void NumList_print(NumList * self){
    if(self == NULL || self->length == 0){
        printf("Порожній список");
    }
    else{
        for(int i = 0;i < NumList_count(self);i++){
            printf("%4i",NumList_at(self,i));
        }
    }
    puts(" ");
}

void NumList_deleteNegative(NumList * self){
    for(int i = 0; i < NumList_count(self);i++){
        if(self->items[i]<0){
            NumList_removeAt(self,i);
            i--;
        }
    }
}

void NumList_fill(NumList * self){
    srand(time(0));
    for(int i = 0; i<NumList_count(self);i++){
        NumList_set(self,i,(rand() % (100 + 100 + 1)) -100);
    }
}

void NumList_addExtra(NumList * self,int numberOfExtraItems){
    srand(time(0));
    for(int i = 0; i<numberOfExtraItems;i++){
        NumList_add(self,(rand() % (100 + 100 + 1)) -100);
    }
}

