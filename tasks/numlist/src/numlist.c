#include <numlist.h>
#include <assert.h>

struct __NumList{
    
    size_t capasity;
    size_t length;//заповнені, існуючі елементи;
    int * array;

};

NumList * NumList_new(void){
    NumList * self = malloc(sizeof(NumList));
    if (self == NULL) {
        assert(0 && "Out of memory");
        abort();
        return NULL;
    }
    self->capasity = INITIAL_CAPASITY;

    self->length = 0;

    self->array = malloc(sizeof(int)*INITIAL_CAPASITY);

    if(self->array == NULL){
        free(self);
        assert(0 && "Out of memory");
        abort();
        return NULL;
    }

    return self;
}

void NumList_free(NumList * self){
    free(self->array);
    free(self);
}

void NumList_add(NumList * self, int value){

    if(self != NULL){
        if(self->capasity == self->length){
            self -> capasity = self -> capasity * 2;
            self = realloc(self, sizeof(NumList)*(self->capasity));
            if (self == NULL){
                assert(0 && "Out of memory");
                abort();
            }
        }

        self -> array[self->length] = value;
        self -> length = self->length + 1;
        
    }
}

void NumList_insert(NumList * self, size_t index, int value){
    if(self != NULL && index < self->length && index >= 0){
        if(index == self -> length - 1){
            NumList_add(self,self->array[0]);
            self->array[0] = value;
        }
        else{
            NumList_add(self,0);
            for(int i = index; i < self->length-1; i++){
                self->array[i+1] = self->array[i];
            }
            self->array[index] = value;
        }
    }
    else if(index>self->length || index < 0 ){
        assert(0 && "Invalid index");
    }
}

int NumList_removeAt(NumList * self, size_t index){
    if(self != NULL && index < self->length && index >= 0){
        if(self->length == 1){
            self->length = 0;
            return index;
        }
        else{
            for(int i = index; i < self->length-1; i++){
                self->array[i]=self->array[i+1];
            }

            self->length--; 

            if(self->capasity > 16 && (self->capasity)/2 == (self->length)){
                self->capasity = (self->capasity)/2;
                self = realloc(self, sizeof(NumList)*(self->capasity));
                
            }
            return index;
        }
    }
    else if(index>self->length || index < 0 ){
        assert(0 && "Invalid index");
    }
    return 0;
}

size_t NumList_count(NumList * self){
    if(self!=NULL){
        return self->length;
    }
    else return 0;
}

int NumList_at(NumList * self, size_t index){
    if(self == NULL){
        assert(0);
    }
    else{
        if(index >= 0 && index < self->length){
            return self->array[index];
        }
        else assert(0 && "Invalid index");
    }
    return EXIT_FAILURE; 
}

int NumList_set(NumList * self, size_t index, int value){

    if(self == NULL){
        assert(0);
    }
    else{
        if(index >= 0 && index < self->length){
            int removedData = self->array[index]; 
            self->array[index] = value;
            return removedData;
        }
        else {
            assert(0 && "Invalid index");
        }
    } 
    return 0;

}