#pragma once
#include <stdio.h>
#include <numlist.h>
#include <time.h>

void NumList_print(NumList * self);
void NumList_deleteNegative(NumList * self);
void NumList_fill(NumList * self);
void NumList_addExtra(NumList * self,int numberOfExtraItems);
