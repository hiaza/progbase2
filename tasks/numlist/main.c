#include <stdlib.h>
#include <progbase.h>
#include <numlist.h>
#include <numlist_ext.h>


int main(void){

    NumList * s = NumList_new();
    NumList_addExtra(s,10);
    NumList_print(s);
    NumList_deleteNegative(s);
    NumList_print(s);
    NumList_free(s);
    puts("Success");
    return EXIT_SUCCESS;
}

