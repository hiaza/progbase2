#include <assert.h>
#include <iterator.h>
#include <list.h>
struct __List {

    size_t capasity;
    size_t length; //заповнені, існуючі елементи;
    void ** array;
};

List * List_new(void) {
    List * self = malloc(sizeof(List));
    if (self == NULL) {
        assert(0 && "Out of memory");
        abort();
        return NULL;
    }
    self->capasity = INITIAL_CAPASITY;

    self->length = 0;

    self->array = malloc(sizeof(void *) * INITIAL_CAPASITY);

    if (self->array == NULL) {
        free(self);
        assert(0 && "Out of memory");
        abort();
        return NULL;
    }

    return self;
}

void List_free(List * self) {
    free(self->array);
    free(self);
}
void List_add(List * self, void * value) {
    if (value != NULL) {
        if (self != NULL) {
            if (self->capasity == self->length) {
                self->capasity = self->capasity * 2;
                self->array =
                    realloc(self->array, sizeof(void *) * (self->capasity));
                if (self == NULL) {
                    assert(0 && "Out of memory");
                    abort();
                }
            }

            self->array[self->length] = value;
            self->length = self->length + 1;
        }
    }
}

void List_insert(List * self, size_t index, void * value) {
    if(value != NULL){
        if (self != NULL && index < self->length && index >= 0) {
            if (index == self->length - 1) {
                List_add(self, self->array[0]);
                self->array[0] = value;
            } else {
                List_add(self, 0);
                for (int i = index; i < self->length - 1; i++) {
                    self->array[i + 1] = self->array[i];
                }
                self->array[index] = value;
            }
        } else if (index > self->length || index < 0) {
            assert(0 && "Invalid index");
        }
    }
}
void * List_removeAt(List * self, size_t index){
    if(self != NULL && index < self->length && index >= 0){
        if(self->length == 1){
            void * removed = self->array[index];
            self->length = 0;
            return removed;
        }
        else{
            void * removed = self->array[index];
            for(int i = index; i < self->length-1; i++){
                self->array[i]=self->array[i+1];
            }

            self->length--; 

            if(self->capasity > 16 && (self->capasity)/2 == (self->length)){
                self->capasity = (self->capasity)/2;
                self = realloc(self, sizeof(List)*(self->capasity));
                
            }
            return removed;
        }
    }
    else if(index>self->length || index < 0 ){
        assert(0 && "Invalid index");
    }
    return NULL;
}

size_t List_count(List * self) {
    if (self != NULL) {
        return self->length;
    } else
        return 0;
}

void * List_at(List * self, size_t index) {
    if (self == NULL) {
        assert(0);
    } else {
        if (index >= 0 && index < self->length) {
            return self->array[index];
        } else
            assert(0 && "Invalid index");
    }
    return NULL;
}

void * List_set(List * self, size_t index, void * value) {

    if (self == NULL) {
        assert(0);
    } else {
        if (index >= 0 && index < self->length) {
            void * removedData = self->array[index];
            self->array[index] = value;
            return removedData;
        } else {
            assert(0 && "Invalid index");
        }
    }
    return NULL;
}

struct __Iterator {
    List * list;
    int index;
};

static Iterator * Iterator_new(List * list, int index) {
    Iterator * self = malloc(sizeof(Iterator));
    self->list = list;
    self->index = index;
    return self;
}

Iterator * List_getNewBeginIterator(List * self) {
    return Iterator_new(self, 0);
}

Iterator * List_getNewEndIterator(List * self) {
    return Iterator_new(self, List_count(self));
}

void Iterator_free(Iterator * self) { free(self); }

void * Iterator_value(Iterator * self) {
    assert(self->index < List_count(self->list));
    return List_at(self->list, self->index);
}
void Iterator_next(Iterator * self) { self->index++; }
void Iterator_prev(Iterator * self) { self->index--; }
void Iterator_advance(Iterator * self, IteratorDistance n) { self->index += n; }
bool Iterator_equals(Iterator * self, Iterator * other) {
    return self->list == other->list && self->index == other->index;
}
IteratorDistance Iterator_distance(Iterator * begin, Iterator * end) {
    return end->index - begin->index;
}