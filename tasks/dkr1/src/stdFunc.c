#include <stdFunc.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
void cleanBuffer();

Value * std_print(Program * program, List * values){
    for (int i = 0; i< List_count(values);i++){
        Value * value = List_at(values,i);
        printf(">> "); 
        Value_print(value);
        puts(" ");
    }
    //rintf("#%s()",__func__);
    return Value_newUndefined();
} 
Value * std_strlen(Program * program,List * values){
    if(List_count(values)!= 1){
        Program_setError(program,strdup("Invalid number of strlen arguments"));
        return NULL;
    }
    Value * strVal = List_at(values,0);
    if(strVal->type != ValueType_STRING){
        Program_setError(program,strdup("Invalid strlen arguments type"));
        return NULL;
    }
    char * str = Value_string(strVal);
    int len = strlen(str);
    //printf("#%s()",__func__);
    return Value_newNumber(len);
}
Value * std_strtonum(Program * program,List * values){
    if(List_count(values)!= 1){
        Program_setError(program,strdup("Invalid number of strlen arguments"));
        return NULL;
    }
    Value * strVal = List_at(values,0);
    if(strVal->type != ValueType_STRING){
        Program_setError(program,strdup("Invalid strlen arguments type"));
        return NULL;
    }
    char * str = Value_string(strVal);
    double val = atof(str);
    //printf("#%s()",__func__);
    return Value_newNumber(val);
}

Value * std_isdigit(Program * program,List * values){
    if(List_count(values)!= 1){
        Program_setError(program,strdup("Invalid number of strlen arguments"));
        return NULL;
    }
    Value * strVal = List_at(values,0);
    if(strVal->type != ValueType_STRING){
        Program_setError(program,strdup("Invalid strlen arguments type"));
        return NULL;
    }
    char * str = Value_string(strVal);
    char temp = *str;
    //printf("\n %zu,\n",strlen(str));
    if(strlen(str)!=1){
        Program_setError(program,strdup("Invalid str"));
        return NULL;
    }
    if(temp >= '0' && temp <='9'){
        return Value_newBool(true);
    }
    //printf("#%s()",__func__);
    return Value_newBool(false);
}


Value * std_numtostr(Program * program,List * values){
    if(List_count(values)!= 1){
        Program_setError(program,strdup("Invalid number of strlen arguments"));
        return NULL;
    }
    Value * Val = List_at(values,0);
    if(Val->type != ValueType_NUMBER){
        Program_setError(program,strdup("Invalid strlen arguments type"));
        return NULL;
    }
    double x = Value_number(Val);
    char buffer[50];
    sprintf(buffer,"%lf",x);
    //printf("#%s()",__func__);
    return Value_newString(buffer);
}

Value * std_sqrt(Program * program,List * values){
    if(List_count(values)!= 1){
        Program_setError(program,strdup("Invalid number of strlen arguments"));
        return NULL;
    }
    Value * Val = List_at(values,0);
    if(Val->type != ValueType_NUMBER){
        Program_setError(program,strdup("Invalid strlen arguments type"));
        return NULL;
    }
    double x = Value_number(Val);
    double res = sqrt(x);
    //printf("#%s()",__func__);
    return Value_newNumber(res);
}

Value * std_pow(Program * program,List * values){
    if(List_count(values)!= 1){
        Program_setError(program,strdup("Invalid number of strlen arguments"));
        return NULL;
    }
    Value * Val = List_at(values,0);
    if(Val->type != ValueType_NUMBER){
        Program_setError(program,strdup("Invalid strlen arguments type"));
        return NULL;
    }
    double x = Value_number(Val);
    double res = x * x;
    //printf("#%s()",__func__);
    return Value_newNumber(res);
}

Value * std_scan(Program * program,List*values){
    if(List_count(values)!= 0){
        Program_setError(program,strdup("Invalid number of strlen arguments"));
        return NULL;
    }
    char buffer[200];
    //cleanBuffer();
    fgets(buffer,199,stdin);
    int i = 0;
    while(buffer[i] !='\n'){
        i++;
    }
    buffer[i] = '\0';
    //printf("#%s()",__func__);
    return Value_newString(buffer);
}


Value * std_substr(Program * program,List * values){
    if(List_count(values)!= 3){
        Program_setError(program,strdup("Invalid number of strlen arguments"));
        return NULL;
    }
    Value * strVal = List_at(values,0);
    if(strVal->type != ValueType_STRING){
        Program_setError(program,strdup("Invalid strlen arguments type"));
        return NULL;
    }
    Value * beginVal = List_at(values,1);
    if(beginVal->type != ValueType_NUMBER){
        Program_setError(program,strdup("Invalid strlen arguments type"));
        return NULL;
    }
    Value * lengthVal = List_at(values,2);
    if(lengthVal->type != ValueType_NUMBER){
        Program_setError(program,strdup("Invalid strlen arguments type"));
        return NULL;
    }
    char * str = Value_string(strVal);
    int begin = (int)Value_number(beginVal); 
    int length = (int)Value_number(lengthVal);
   
    if((strlen(str) < begin+length)|| begin<0 || length<=0){    ///
        Program_setError(program,strdup("Invalid index,or length"));
        return NULL;
    }
    char*tem = strdup(str);
    char*temp = tem; 
    char buffer[length+1];
    int c;
    for (c = 0 ; c < length ; c++){
       {
          buffer[c] = *(temp+begin);      
          temp++;   
       }
    } 
    buffer[length]='\0';
    free(tem);
 //   printf("#%s()",__func__);
    return Value_newString(buffer);
}

void cleanBuffer() {
    char c = 0;
    while(c != '\n') {
        c = getchar();
    }
}


Value * std_newArr(Program * program,List * values){
    if(List_count(values)!= 0){
        Program_setError(program,strdup("Invalid number of strlen arguments"));
        return NULL;
    }
    List * ar = List_new();
    return Value_new(ValueType_ARRAY,ar);
}
Value * std_push(Program * program,List * values){
    if(List_count(values)!= 2){
        Program_setError(program,strdup("Invalid number of strlen arguments"));
        return NULL;
    }
    Value * arrayVal = List_at(values,0);
    if(arrayVal->type != ValueType_ARRAY){
        Program_setError(program,strdup("Invalid strlen arguments type"));
        return NULL;
    }
    Value * Valu = List_at(values,1);
    if(Valu->type != ValueType_NUMBER){
        Program_setError(program,strdup("Invalid strlen arguments type"));
        return NULL;
    }
    double val = Value_number(Valu);
    double * numberMem = malloc(sizeof(double));
    *numberMem = val;
    
    if(arrayVal->value == NULL){
        Program_setError(program,strdup("Firstly you need to create the array (newArr)"));
       
        return NULL;
    }
    else{
        List_add(Value_array(arrayVal),numberMem);
    }
    return arrayVal;
}



Value * std_at(Program * program,List * values){
    if(List_count(values)!= 2){
        Program_setError(program,strdup("Invalid number of strlen arguments"));
        return NULL;
    }

    Value * indexVal = List_at(values,1);
    if(indexVal->type != ValueType_NUMBER){
        Program_setError(program,strdup("Invalid strlen arguments type"));
        return NULL;
    }

    int index = (int)Value_number(indexVal);

    Value * arrayVal = List_at(values,0);
    if(arrayVal->type == ValueType_ARRAY){

        List * array = Value_array(arrayVal);
        if(index >= List_count(array) || index < 0){
            Program_setError(program,strdup("Invalid index"));
            return NULL;
        }

        
        return Value_new(ValueType_NUMBER,(double*)List_at(array,index));

    } else if(arrayVal->type == ValueType_STRING){
        char * str = Value_string(arrayVal);
        char *temp = strdup(str);
        if(index >= strlen(str) || index < 0){
            Program_setError(program,strdup("Invalid index"));
            return NULL;
        }
        char * c = malloc(sizeof(char)*2);
        c[0] = temp[index];
        c[1]='\0';
        free(temp);
        return Value_new(ValueType_STRING,c);
    }else{
        Program_setError(program,strdup("Invalid strlen arguments type"));
        return NULL;
    } 
}

Value * std_count(Program * program,List * values){
    if(List_count(values)!= 1){
        Program_setError(program,strdup("Invalid number of strlen arguments"));
        return NULL;
    }
    Value * arrayVal = List_at(values,0);
    if(arrayVal->type != ValueType_ARRAY){
        Program_setError(program,strdup("Invalid strlen arguments type"));
        return NULL;
    }
    List * array = Value_array(arrayVal);
    if(array == NULL){
        Program_setError(program,strdup("Empty Array"));
        return NULL;
    }
    double x = List_count(array);
    return Value_newNumber(x);
}