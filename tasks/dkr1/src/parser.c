#include <list.h>
#include <parser.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <stdbool.h>
#include <lexer.h>
#include <iterator.h>
#include <string_buffer.h>
#include <ast.h>
typedef struct {
    Iterator * tokens;
    Iterator * tokensEnd;
    char * error;
    int level;
    // optional extra fields
} Parser;

// check current token type
static Tree * accept(Parser * self, TokenType tokenType);
// expect exactly that token type or fail
static Tree * expect(Parser * self, TokenType tokenType);
    


static Tree * prog       (Parser * parser);
static Tree * var_decl   (Parser * parser);
static Tree * st         (Parser * parser);
static Tree * ID         (Parser * parser);
static Tree * expr       (Parser * parser);
static Tree * expr_st    (Parser * parser);
static Tree * block_st   (Parser * parser);
static Tree * select_st  (Parser * parser);
static Tree * iter_st    (Parser * parser);
static Tree * assign     (Parser * parser);
static Tree * assign_ap  (Parser * parser);
static Tree * log_or     (Parser * parser);
static Tree * log_or_ap  (Parser * parser);
static Tree * log_and    (Parser * parser);
static Tree * log_and_ap (Parser * parser);
static Tree * eq         (Parser * parser);
static Tree * eq_ap      (Parser * parser);
static Tree * rel        (Parser * parser);
static Tree * rel_ap     (Parser * parser);
static Tree * add        (Parser * parser);
static Tree * add_ap     (Parser * parser);
static Tree * mult       (Parser * parser);
static Tree * mult_ap    (Parser * parser);
static Tree * unary      (Parser * parser);
static Tree * primary    (Parser * parser);
static Tree * NUMBER     (Parser * parser);
static Tree * STRING     (Parser * parser);
static Tree * PRAVDA     (Parser * parser);
static Tree * NOPE       (Parser * parser);
static Tree * var_or_call(Parser * parser);
static Tree * parentheses(Parser * parser);
static Tree * fn_call    (Parser * parser);
static Tree * arg_list   (Parser * parser);


//
#define TRACE_CALL() \
   parser->level++; \
   trace(parser,__func__);\
   Parser * parserPtr __attribute__((cleanup(Parser_decLevel))) = parser;

void Parser_decLevel(Parser** parserPtr){
    (*parserPtr)->level--;
}

#define TRACE_EXPECT(TYPE) \
   traceExpect(parser,TYPE);

static bool eoi (Parser * self){
    return Iterator_equals(self->tokens,self->tokensEnd);
 }

 static void traceLevel(int level){
     for (int i = 0; i < level; i++ ){
         putchar('.');
         putchar('.');
     }
 }
static void trace(Parser * parser, const char * fn ){
    traceLevel(parser->level);
    if(eoi(parser)) {
        printf("%s: <EOI>\n", fn);
        return;
    }
    Token * token = Iterator_value(parser -> tokens);
    TokenType type = token -> type;
    switch (type){
        case TokenType_ID: case TokenType_NUMBER: case TokenType_STRING:{
            printf("%s: <%s, %s>\n",fn, TokenType_toString(type),token->lexeme);
            break;
        }
        default: {
            printf("%s: <%s>\n",fn, TokenType_toString(type));
            break;
        }
    }
}

static void traceExpect(Parser * parser, TokenType expectedType){
    traceLevel(parser->level);
    printf("<%s>\n", TokenType_toString(expectedType));
}

Tree * Parser_buildNewAstTree(List * tokens){
    Parser parser = {
        .tokens = List_getNewBeginIterator(tokens),
        .tokensEnd = List_getNewEndIterator(tokens),
        .error = NULL,
        .level = -1
    };
    Tree * progNode = prog(&parser);
    if (parser.error){
        fprintf(stderr,">>> ERROR: %s \n",parser.error);
        free((void *) parser.error);
        free(parser.tokens);
        free(parser.tokensEnd);
        astTree_free(progNode);
        return NULL;
    }    
    else if (!eoi(&parser)){
        Token * token = Iterator_value(parser.tokens);
        fprintf(stderr,">>> ERROR: unexpected token %s \n", TokenType_toString(token->type));
    }
    
    Iterator_free(parser.tokens);
    Iterator_free(parser.tokensEnd);
    return progNode;
}

/*
bool Parser_match(List * tokens){
    Parser parser = {
        .tokens = List_getNewBeginIterator(tokens),
        .tokensEnd = List_getNewEndIterator(tokens),
        .error = NULL,
        .level = -1
    };
    bool match = prog(&parser);
    if (parser.error){
        fprintf(stderr,">>> ERROR: %s \n",parser.error);
        free((void *) parser.error);
        free(parser.tokens);
        free(parser.tokensEnd);
        return false;
    }    
    else if (!eoi(&parser)){
        Token * token = Iterator_value(parser.tokens);
        fprintf(stderr,">>> ERROR: unexpected token %s \n", TokenType_toString(token->type));
    }
    
    free(parser.tokens);
    free(parser.tokensEnd);
    return match;
}
*/

static AstNodeType TokenType_toAstNodeType(TokenType type){
    if (type == TokenType_ID) {
		return AstNodeType_ID;
	} else if (type == TokenType_NUMBER) {
		return AstNodeType_NUMBER;
	} else if (type == TokenType_STRING) {
		return AstNodeType_STRING;
	}  else if (type == TokenType_ASSIGN) {
		return AstNodeType_ASSIGN;
	} else if (type == TokenType_PLUS) {
		return AstNodeType_ADD;
	} else if (type == TokenType_MINUS) {
		return AstNodeType_SUB;
	} else if (type == TokenType_MULT) {
		return AstNodeType_MUL;
	} else if (type == TokenType_MOD) {
		return AstNodeType_MOD;
	} else if (type == TokenType_DIV) {
		return AstNodeType_DIV;
	} else if (type == TokenType_EQ) {
		return AstNodeType_EQ;
	} else if (type == TokenType_NEQ) {
		return AstNodeType_NEQ;
	}  else if (type == TokenType_NOT) {
		return AstNodeType_NOT;
	} else if (type == TokenType_LT) {
		return AstNodeType_LT;
	} else if (type == TokenType_GT) {
		return AstNodeType_GT;
	} else if (type == TokenType_LE) {
		return AstNodeType_LE;
	} else if (type == TokenType_GE) {
		return AstNodeType_GE;
	} else if (type == TokenType_AND) {
		return AstNodeType_AND;
	} else if (type == TokenType_OR) {
		return AstNodeType_OR;
	} else if (type == TokenType_PRAVDA) {
		return AstNodeType_PRAVDA;
	} else if (type == TokenType_NOPE){
		return AstNodeType_NOPE;
	}
	return AstNodeType_UNKNOWN;
}

static Tree * accept(Parser * self, TokenType tokenType){
    if (eoi(self)) return NULL;
    Token * token = Iterator_value(self ->tokens);
    if(token->type == tokenType){
        AstNodeType astType = TokenType_toAstNodeType(tokenType);
        char * astName = strdup(token->lexeme);
        AstNode * node = AstNode_new(astType,astName);
        Tree * tree = Tree_new(node);
        Iterator_next(self ->tokens);
        return tree;
    }
    return NULL;
}
 

static Tree * expect(Parser * parser, TokenType tokenType){
    Tree * tree = accept(parser,tokenType);
    if(tree != NULL){
        //TRACE_EXPECT(tokenType);
        return tree; // look
    }

    if(!parser->error){
        const char * currentTokenType = eoi(parser) 
        ? "end - of - input" 
        :TokenType_toString(((Token * )Iterator_value(parser ->tokens))->type);
      StringBuffer * sb = StringBuffer_new();
      StringBuffer_appendFormat(sb,"expected '%s' got '%s' .\n", TokenType_toString(tokenType),currentTokenType);
      parser ->error = StringBuffer_toString(sb);
      StringBuffer_free(sb);
    }
   // Tree_free(tree);
   // fprintf(stderr,"expected ... got ... .\n");
    return NULL;
}

typedef Tree * (*GrammarRule)(Parser * parser);

// EBNF: A = { B };
static bool ebnf_multiple      (Parser * parser,List * nodes, GrammarRule rule){
    Tree * node = NULL;
	while ((node = rule(parser)) && !parser->error) {
        List_add(nodes, node);
    }
    if(parser->error){
       /* while(List_count(nodes)!=0){
            Tree * x = List_removeAt(nodes,0);
            astTree_free(x);
        }
        List_free(nodes);*/
    }
	return parser->error ? false : true;
}
// EBNF: A = B | C | D;
static Tree * ebnf_select        (Parser * parser, GrammarRule rules[], size_t rulesLen){
    Tree * selected = NULL;
    for (size_t i = 0; i < rulesLen && !selected; i++) {
		GrammarRule rule = rules[i];
		selected = rule(parser);
		if (parser->error) return NULL;
	}
    return selected;
}
static Tree * ebnf_selectLexemes (Parser * parser, TokenType types[], size_t typesLen){
    Tree * node = NULL;
    for (int i = 0; i < typesLen && !node; i++){
        node = accept(parser,types[i]);
    }
    return node;
}
// EBNF: A = bA';
static Tree * ebnf_ap_main_rule(Parser * parser, GrammarRule next, GrammarRule ap){
    Tree * nextNode = next(parser);
    if(nextNode){
        Tree * apNode = ap(parser);
        if(apNode){
            List_insert(apNode->children,0,nextNode);//Look
            return apNode;
        }
        return nextNode;
    }
    return NULL;
}
// EBNF: A' = aA' | e;
static Tree * ebnf_ap_recursive_rule(
Parser * parser, 
TokenType types[], 
size_t typesLen, 
GrammarRule next, 
GrammarRule ap){
    Tree * opNode = ebnf_selectLexemes(parser,types,typesLen);
    if (opNode == NULL) return NULL;
    Tree * node = NULL;
    Tree * nextNode  = next(parser);
    Tree *apNode = ap(parser);
    if(apNode){
        List_insert(apNode->children,0,nextNode);
        node = apNode;
    } else{
        node = nextNode;
    }
    List_add(opNode->children,node);
    return opNode;
}

//
static Tree * ID         (Parser * parser){
    //TRACE_CALL();
    return accept(parser, TokenType_ID);
}
static Tree * NUMBER     (Parser * parser){
    //TRACE_CALL();
    return accept(parser,TokenType_NUMBER);
}
static Tree * STRING     (Parser * parser){
    //TRACE_CALL();
    return accept(parser,TokenType_STRING);
}
static Tree * PRAVDA     (Parser * parser){
    //TRACE_CALL();
    return accept(parser,TokenType_PRAVDA);
}
static Tree * NOPE     (Parser * parser){
    //TRACE_CALL();
    return accept(parser,TokenType_NOPE);
}
static Tree * ID_expect      (Parser * parser){
    return expect(parser, TokenType_ID);
}

static Tree * prog       (Parser * parser){
    //TRACE_CALL();
    Tree * progNode = Tree_new(AstNode_new(AstNodeType_PROGRAM,strdup("program")));
    while(!eoi(parser)){
        if(parser->error == NULL){
            (void) ebnf_multiple(parser,progNode ->children,var_decl);
        } else break;
        if(parser->error == NULL){
            (void) ebnf_multiple(parser,progNode ->children,st); 
        } else break;
    }
    return progNode;
}
/*
static Tree * expr_expect(Parser * parser){
    //TRACE_CALL();
    return expr(parser)
    ? true
    : (parser -> error = strdup("no expr"),false);
}*/
static Tree * var_decl   (Parser * parser){
    //TRACE_CALL();

    Tree * berems = accept(parser,TokenType_BEREMO);
    if(!berems) return NULL;
    astTree_free(berems);

    Tree * idNode = ID_expect(parser);
    if(!idNode) return NULL;
     
    Tree * expected = expect(parser,TokenType_ASSIGN);
    if(!expected){
        astTree_free(idNode);
        //@todo error
      return NULL;
    }
    astTree_free(expected);

    Tree * exprNode = expr(parser);
    if(exprNode == NULL){
        astTree_free(idNode);
        if(!parser -> error){
            parser->error = strdup("No expr");
        } 
       
        //@todo error
        return NULL;
    }

    if(parser->error){
        astTree_free(idNode);
        astTree_free(exprNode);
        return NULL;
    }

    Tree * expected2 = expect(parser,TokenType_SEMICOLON);
    if(!expected2){
        astTree_free(idNode);
        astTree_free(exprNode);
        //@todo error
        return NULL;
    }
    astTree_free(expected2);
    Tree * varDecl = Tree_new(AstNode_new(AstNodeType_DECLAREVAR,strdup("declareVar")));

    List_add(varDecl->children,idNode);
    List_add(varDecl->children,exprNode);
    
    return varDecl;
}
static Tree * st         (Parser * parser){
    //TRACE_CALL();
    return ebnf_select(parser,
    (GrammarRule[]){
        block_st,
        select_st,
        iter_st,
        expr_st
    },4);
}

static Tree * expr       (Parser * parser){
    //TRACE_CALL();
    return assign(parser); ///looooook
}

/*
static Tree * st_expect (Parser * parser){
    //TRACE_CALL();
    return st(parser)
    ? true
    : (parser -> error = strdup("no st"),false);
}*/

static Tree * expr_st    (Parser * parser){
    //TRACE_CALL();
    Tree * exprNode = expr(parser);

    if(exprNode) {
        Tree * expected = expect(parser,TokenType_SEMICOLON);
        if(!expected){
            astTree_free(exprNode);
            return NULL;
        } 
        astTree_free(expected);
    } else {   
        Tree * accepted = accept(parser, TokenType_SEMICOLON);
        if(accepted) astTree_free(accepted);
    }
    return exprNode;
}

static Tree * block_st   (Parser * parser){
    //TRACE_CALL();
    if(parser->error){
        return NULL;
    }
    Tree * accepted = accept(parser,TokenType_OPENBLOCK);
    if(!accepted) return NULL;
    astTree_free(accepted);
   
    Tree * blockNode = Tree_new(AstNode_new(AstNodeType_BLOCK,strdup("block")));
    if(ebnf_multiple(parser,blockNode->children, st)){
        Tree * expected = expect(parser,TokenType_CLOSEBLOCK);
        if(!expected){
            astTree_free(blockNode);
            return NULL;
        } 
        astTree_free(expected);
    }
    else{
        astTree_free(blockNode);
        return NULL;
    }
    return blockNode;
}

static Tree * select_st  (Parser * parser){
    //TRACE_CALL();
    Tree * accepted = accept(parser,TokenType_YAK);
    if(!accepted) return NULL;
    astTree_free(accepted);

    Tree * expected = expect(parser,TokenType_OPENBR);
    if(!expected) return NULL;
    astTree_free(expected);
    
    Tree * testExprNode = expr(parser);
    if(!testExprNode){
        parser->error = strdup("no expr");
        // @todo error
        return NULL;
    }
    Tree * expected2 = expect(parser,TokenType_CLOSEBR);
    if(!expected2){
        astTree_free(testExprNode);
        // @todo clear testExpr
        // @todo error
        return NULL;
    } 
    astTree_free(expected2);
    
    Tree * expectedBL = expect(parser,TokenType_OPENBLOCK);
    if(!expectedBL){
        astTree_free(testExprNode);
        return NULL;
    }   
    astTree_free(expectedBL);
    Iterator_prev(parser->tokens);

    Tree * stNode = st(parser);
    if(stNode == NULL){
        astTree_free(testExprNode);
        if(!parser->error) {
            parser->error = strdup("no st");            
        }
        // @todo error
        return NULL;
    }
    
    Tree * yakNode = Tree_new(AstNode_new(AstNodeType_YAK, strdup("yak")));
    List_add(yakNode->children,testExprNode);
    List_add(yakNode->children,stNode);
    Tree * accepted2 = accept(parser,TokenType_INACSHE);
    if(accepted2){
        astTree_free(accepted2);

        Tree * expectedBL = expect(parser,TokenType_OPENBLOCK);
        if(!expectedBL){
            astTree_free(yakNode);
            return NULL;
        }   
        astTree_free(expectedBL);
        Iterator_prev(parser->tokens);

        Tree * elseNode = st(parser);
        if(elseNode == NULL || parser->error){
            astTree_free(yakNode);
            if(parser->error == NULL){
                parser->error = strdup("no st");
            } else astTree_free(elseNode);
            //@todo error
            return NULL;
        }
        List_add(yakNode->children,elseNode);
    }
    return yakNode;
}
static Tree * iter_st    (Parser * parser){
    //TRACE_CALL();
    Tree * accepted = accept(parser,TokenType_POKI);
    if(!accepted) return NULL;
    astTree_free(accepted);

    Tree * expected = expect(parser,TokenType_OPENBR);
    if(!expected) return NULL;
    astTree_free(expected);
    Tree * testExprNode = expr(parser);
    if(!testExprNode){
        parser->error = strdup("no expr");
        // @todo error
        return NULL;
    }
    Tree * expected2 = expect(parser,TokenType_CLOSEBR);
    if(!expected2){
        // @todo clear testExpr
        // @todo error
        astTree_free(testExprNode);
        return NULL;
    } 
    astTree_free(expected2);
    
    Tree * expectedBL = expect(parser,TokenType_OPENBLOCK);
    if(!expectedBL){
        astTree_free(testExprNode);
        return NULL;
    }   
    astTree_free(expectedBL);
    Iterator_prev(parser->tokens);
    
    Tree * stNode = st(parser);
    if(stNode == NULL){
        astTree_free(testExprNode);
        if(!parser->error) {
            parser->error = strdup("no st");            
        }
        // @todo clear testExpr
        // @todo error
        return NULL;
    }
    Tree * whileNode = Tree_new(AstNode_new(AstNodeType_POKI, strdup("while")));
    List_add(whileNode->children,testExprNode);
    List_add(whileNode->children,stNode);
    return whileNode;
}
static Tree * assign     (Parser * parser){
    //TRACE_CALL();
    return ebnf_ap_main_rule(parser,log_or,assign_ap);
}
static Tree * assign_ap  (Parser * parser){
    //TRACE_CALL();
    return ebnf_ap_recursive_rule(parser,
    (TokenType[]){
        TokenType_ASSIGN
    },1,
    log_or, assign_ap);
}
static Tree * log_or     (Parser * parser){
    //TRACE_CALL();
    return ebnf_ap_main_rule(parser,log_and,log_or_ap);
}
static Tree * log_or_ap  (Parser * parser){
    //TRACE_CALL();
    return ebnf_ap_recursive_rule(parser,
        (TokenType[]){
            TokenType_OR
        },1,
        log_and, log_or_ap);
}
static Tree * log_and    (Parser * parser){
    //TRACE_CALL();
    return ebnf_ap_main_rule(parser,eq,log_and_ap);
}
static Tree * log_and_ap (Parser * parser){
    //TRACE_CALL();
    return ebnf_ap_recursive_rule(parser,
        (TokenType[]){
            TokenType_AND
        },1,
        eq, log_and_ap);
}
static Tree * eq         (Parser * parser){
    //TRACE_CALL();
    return ebnf_ap_main_rule(parser,rel,eq_ap);
}
static Tree * eq_ap      (Parser * parser){
    //TRACE_CALL();
    return ebnf_ap_recursive_rule(parser,
        (TokenType[]){
            TokenType_EQ,
            TokenType_NEQ
        },2,
        rel, eq_ap);
}
static Tree * rel        (Parser * parser){
    //TRACE_CALL();
    return ebnf_ap_main_rule(parser,add,rel_ap);
}
static Tree * rel_ap     (Parser * parser){
    //TRACE_CALL();
    return ebnf_ap_recursive_rule(parser,
        (TokenType[]){
            TokenType_LT,
            TokenType_LE,
            TokenType_GT,
            TokenType_GE
        },4,
        add, rel_ap);
}
static Tree * add        (Parser * parser){
    //TRACE_CALL();
    return ebnf_ap_main_rule(parser,mult,add_ap);
}

static Tree * add_ap     (Parser * parser){
    //TRACE_CALL();
    return ebnf_ap_recursive_rule(parser,
        (TokenType[]){
            TokenType_PLUS,
            TokenType_MINUS
        },2,
        mult, add_ap);
}
static Tree * mult       (Parser * parser){
    //TRACE_CALL();
    return ebnf_ap_main_rule(parser,unary,mult_ap);
}
static Tree * mult_ap    (Parser * parser){
    //TRACE_CALL();
    return ebnf_ap_recursive_rule(parser,
        (TokenType[]){
            TokenType_MULT,
            TokenType_DIV,
            TokenType_MOD
        },3,
        unary, mult_ap);
}

static Tree * unary      (Parser * parser){
    //TRACE_CALL();
    Tree * opNode = ebnf_selectLexemes(parser,
    (TokenType[]){
        TokenType_PLUS,
        TokenType_MINUS,
        TokenType_NOT
    },3);

    Tree * primNode = primary(parser);
    // @todo cheak NULL prim
    if(opNode && !primNode){
        astTree_free(opNode);
        parser->error = strdup("error with primary");
        return NULL;
    }
    if(opNode){
        List_add(opNode->children,primNode);
        return opNode;
    }
    return primNode;
}
static Tree * primary    (Parser * parser){
    //TRACE_CALL();
    return ebnf_select(parser,
        (GrammarRule[]){
            NUMBER,
            STRING,
            var_or_call,
            parentheses
        },4);
}

static Tree * var_or_call(Parser * parser){
    //TRACE_CALL();
    Tree * varNode = ID(parser);
    if(!varNode){
        return NULL;
    }
 
    Tree * argListNode = fn_call(parser);
    if(argListNode){
        List_add(varNode->children,argListNode);
        return varNode;
    }
    else {
        Tree * accepted = accept(parser,TokenType_ID);
        if(accepted){
            astTree_free(varNode);
            astTree_free(accepted);
            if(!parser->error){
                parser->error = strdup("mistake (expected '=')");
            }
            return NULL ;
        }
    }
    return varNode;
}
static Tree * parentheses(Parser * parser){
    //TRACE_CALL();
    Tree * accepted = accept(parser,TokenType_OPENBR);
    if(!accepted) return NULL;
    astTree_free(accepted);
    Tree * argListNode = arg_list(parser);
    Tree * expected = expect(parser,TokenType_CLOSEBR);
    if(!expected){
        //@todo mb
        if(argListNode){
            astTree_free(argListNode);
        }
        return NULL;
    } //@todo mb
    astTree_free(argListNode);
    astTree_free(expected);
    return accept(parser,TokenType_OPENBR);
}
static Tree * fn_call    (Parser * parser){
    //TRACE_CALL();
    Tree * accepted = accept(parser,TokenType_OPENBR);
    if(!accepted) return NULL;
    astTree_free(accepted);
    Tree * argListNode = arg_list(parser);
    Tree * expected = expect(parser,TokenType_CLOSEBR);
    if(!expected){//@todo mb
        if(argListNode){
            astTree_free(argListNode);
        }
        return NULL;
    } 
    astTree_free(expected); 
    return argListNode;
   
}
static Tree * arg_list   (Parser * parser){ 
    //TRACE_CALL();
    Tree * exprNode = expr(parser);
    Tree * argListNode = Tree_new(AstNode_new(AstNodeType_ARGLIST,strdup("arglist")));
    if(exprNode){
        List_add(argListNode->children,exprNode);
        while (true){
            Tree * accepted = accept(parser,TokenType_COMMA);
            if (!accepted) break;
            astTree_free(accepted);
            exprNode = expr(parser);
            if(exprNode){
                List_add(argListNode->children,exprNode);
            } else{
                break;
            }
        }
    }
    return argListNode;
}