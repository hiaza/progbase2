#include <ctype.h>
#include <iterator.h>
#include <lexer.h>
#include <list.h>
#include <stdio.h>
#include <strdup.h>
#include <string.h>
#include <string_buffer.h>

int Lexer_splitTokens(const char * input, List * tokens) {
    while (*input != '\0') {
        if (*input == '\0') { break; }
        if (isspace(*input)) {

        } else if (*input == '(') {
            List_add(tokens, Token_new(TokenType_OPENBR, "("));
        } else if (*input == ')') {
            List_add(tokens, Token_new(TokenType_CLOSEBR, ")"));
        } else if (*input == '{') {
            List_add(tokens, Token_new(TokenType_OPENBLOCK, "{"));
        } else if (*input == '}') {
            List_add(tokens, Token_new(TokenType_CLOSEBLOCK, "}"));
        } else if (*input == '[') {
            List_add(tokens, Token_new(TokenType_SQCLOSEBR, "["));
        } else if (*input == ']') {
            List_add(tokens, Token_new(TokenType_SQCLOSEBR, "]"));
        } else if (*input == ',') {
            List_add(tokens, Token_new(TokenType_COMMA, ","));
        } else if (*input == ';') {
            List_add(tokens, Token_new(TokenType_SEMICOLON, ";"));
        } else if (*input == '+') {
            List_add(tokens, Token_new(TokenType_PLUS, "+"));
        } else if (*input == '-') {
            List_add(tokens, Token_new(TokenType_MINUS, "-"));
        } else if (*input == '/') {
            List_add(tokens, Token_new(TokenType_DIV, "/"));
        } else if (*input == '%') {
            List_add(tokens, Token_new(TokenType_MOD, "%"));
        } else if (*input == '=') {
            input++;
            if (*input != '=') {
                input--;
                List_add(tokens, Token_new(TokenType_ASSIGN, "="));
            } else
                List_add(tokens, Token_new(TokenType_EQ, "=="));
        } else if (isdigit(*input)) {
            StringBuffer * sb = StringBuffer_new();
            while (isdigit(*input) || *input == '.') {
                StringBuffer_appendChar(sb, *input);
                input++;
            }
            char * tmp = strdup(StringBuffer_toNewString(sb));
            List_add(tokens, Token_new(TokenType_NUMBER, tmp));
            input--;
            StringBuffer_free(sb);
        } else if (*input == '"') {
            input++;
            StringBuffer * sb = StringBuffer_new();
            while (*input != '"') {
                if (*(input) == '\\' && *(input + 1) == '"') {
                    StringBuffer_appendChar(sb, *input);
                    input++;
                }
                StringBuffer_appendChar(sb, *input);
                input++;
            }
            char * tmp = strdup(StringBuffer_toNewString(sb));
            List_add(tokens, Token_new(TokenType_STRING, tmp));
            StringBuffer_free(sb);
        } else if (*input == '<') {
            input++;
            if (*input != '=') {
                input--;
                List_add(tokens, Token_new(TokenType_LT, "<"));
            } else
                List_add(tokens, Token_new(TokenType_LE, "<="));
        } else if (*input == '>') {
            input++;
            if (*input != '=') {
                input--;
                List_add(tokens, Token_new(TokenType_GT, ">"));
            } else
                List_add(tokens, Token_new(TokenType_GE, ">="));
        } else if (*input == '*') {
            List_add(tokens, Token_new(TokenType_MULT, "*"));
        } else if (*input == '|') {
            input++;
            if (*input != '|') {
                input--;
                return 1;
            } else {
                List_add(tokens, Token_new(TokenType_OR, "||"));
            }
        } else if (*input == '&') {
            input++;
            if (*input != '&') {
                input--;
                return 1;
            } else {
                List_add(tokens, Token_new(TokenType_AND, "&&"));
            }
        } else if (*input == '!') {
            input++;
            if (*input != '=') {
                input--;
                List_add(tokens, Token_new(TokenType_NOT, "!"));
            } else {
                List_add(tokens, Token_new(TokenType_NEQ, "!="));
            }
        } else if (isalpha(*input) || *input == '_') {
            StringBuffer * sb = StringBuffer_new();
            while (isalpha(*input) || isdigit(*input) || *input == '_') {
                StringBuffer_appendChar(sb, *input);
                input++;
            }
            char * tmp = strdup(StringBuffer_toNewString(sb));
            if (strcmp(tmp, "yak") == 0) {
                List_add(tokens, Token_new(TokenType_YAK, tmp));
            } else if (strcmp(tmp, "mod") == 0) {
                List_add(tokens, Token_new(TokenType_MOD, tmp));
            } else if (strcmp(tmp, "inacshe") == 0) {
                List_add(tokens, Token_new(TokenType_INACSHE, tmp));
            } else if (strcmp(tmp, "beremo") == 0) {
                List_add(tokens, Token_new(TokenType_BEREMO, tmp));
            } else if (strcmp(tmp, "poki") == 0) {
                List_add(tokens, Token_new(TokenType_POKI, tmp));
            } else if(strcmp(tmp,"pravda") == 0){
                List_add(tokens,Token_new(TokenType_PRAVDA,tmp));  
            }else if(strcmp(tmp,"nope") == 0){
                List_add(tokens,Token_new(TokenType_NOPE,tmp));  
            } else
                List_add(tokens, Token_new(TokenType_ID, tmp));
            input--;
            StringBuffer_free(sb);
        } else
            return 1;
        input++;
    }
    return 0;
}

void Lexer_clearTokens(List * tokens) {
    int i = 0;
    while (i < List_count(tokens)) {
        Token * cur = List_at(tokens, i);
        if (cur->type == TokenType_ID || cur->type == TokenType_YAK ||
            cur->type == TokenType_INACSHE || cur->type == TokenType_BEREMO ||
            cur->type == TokenType_POKI || cur->type == TokenType_PRAVDA ||
            cur->type == TokenType_NOPE || cur->type == TokenType_STRING ||
            cur->type == TokenType_NUMBER) {
            free(cur->lexeme);
        }
        Token_free(cur);
        i++;
    }
}

Token * Token_new(TokenType type, char * name) {
    Token * new = malloc(sizeof(Token));
    new->type = type;
    new->lexeme = name;
    return new;
}

void Token_free(Token * self) { free(self); }

bool Token_equals(Token * self, Token * other) {
    if (self != NULL && other != NULL && (self->type == other->type) &&
        (strcmp(self->lexeme, other->lexeme) == 0)) {
        return true;
    } else
        return false;
}

static const char * symbols[] = {
    "=",            // TokenType_ASSIGN,
    ";",            // TokenType_SEMICOLON,
    "+",            // TokenType_PLUS,
    "-",            // TokenType_MINUS,
    "*",            // TokenType_MULT,
    "/",            // TokenType_DIV,
    "(",            // TokenType_OPENBR,
    ")",            // TokenType_CLOSEBR,
    "{",            // TokenType_OPENBLOCK,
    "}",            // TokenType_CLOSEBLOCK,
    "[",            // TokenType_SQOPENBR,
    "]",            // TokenType_SQCLOSEBR,
    ",",            // TokenType_COMMA,
    "<",            // TokenType_LT,
    ">",            // TokenType_GT,
    "!",            // TokenType_NOT,
    "==",           // TokenType_EQ,
    "<=",           // TokenType_LE,
    ">=",           // TokenType_GE,
    "!=",           // TokenType_NEQ,
    "&&",           // TokenType_AND,
    "||",           // TokenType_OR,
    "beremo",       // TokenType_BEREMO,
    "poki",         // TokenType_POKI,
    "yak",          // TokenType_YAK,
    "inacshe",      // TokenType_INACSHE,
    "mod",          // mod
    "number",       // TokenType_NUMBER,
    "string",       // TokenType_STRING,
    "identificator" // TokenType_ID,
};

const char * TokenType_toString(TokenType type) { return symbols[type]; }

void Lexer_printTokens(List * tokens) {
    Iterator * begin = List_getNewBeginIterator(tokens);
    Iterator * end = List_getNewEndIterator(tokens);
    while (!Iterator_equals(begin, end)) {
        Token * token = Iterator_value(begin);
        if (token->type == TokenType_NUMBER) {
            printf("<NUM,%s>", token->lexeme);
        } else if (token->type == TokenType_STRING) {
            printf("<Word,"
                   "%s"
                   ">",
                   token->lexeme);
        } else if (token->type == TokenType_ID) {
            printf("<id,%s>", token->lexeme);
        } else if (strcmp(token->lexeme, "\n") != 0) {
            printf("<%s>", token->lexeme);
        } else
            printf("\n");
        Iterator_next(begin);
    }
    Iterator_free(begin);
    Iterator_free(end);
}

void Lexer_fprintTokens(List * tokens, const char * fileName) {
    int ntokens = List_count(tokens);
    FILE * file = fopen(fileName, "w");
    for (int i = 0; i < ntokens; i++) {
        Token * token = List_at(tokens, i);
        if (token->type == TokenType_NUMBER) {
            fprintf(file, "<NUM,%s>", token->lexeme);
        } else if (token->type == TokenType_STRING) {
            fprintf(file, "<Word,"
                          "%s"
                          ">",
                    token->lexeme);
        } else if (token->type == TokenType_ID) {
            fprintf(file, "<id,%s>", token->lexeme);
        } else if (strcmp(token->lexeme, "\n") != 0) {
            fprintf(file, "<%s>", token->lexeme);
        } else
            fprintf(file, "\n");
        // Token_free(token);
    }
    fclose(file);
}