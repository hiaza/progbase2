#include <stdlib.h>
#include <ast.h>

AstNode * AstNode_new(AstNodeType type, char * name){
    AstNode * self = malloc(sizeof(AstNode));
    self -> type = type;
    self -> name = name;
    return self;
}
void AstNode_free(AstNode * self){
    free((void*)self->name);
    free(self);
}