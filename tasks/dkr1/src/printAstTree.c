#include <ast.h>
#include <list.h>
#include <printAstTree.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strdup.h>

static char * str_append(const char * str,const char * append){
    size_t newLen = strlen(str) + strlen(append) + 1;
    char * buf = malloc (sizeof(char)*newLen);
    buf[0]='\0';
    sprintf(buf,"%s%s",str,append);
    return buf;
}  


void printed(Tree * node, const char * indent, int root, int last){
    printf("%s",indent);
    char * newIndent = NULL;
    if(last){
        if(!root){
            printf("➡ ");
            newIndent = str_append(indent,"၀၀");
        } else{
            newIndent = str_append(indent,"");
        }
    } else{
        printf("➡ ");
        newIndent = str_append(indent,"|၀");
    }
    AstNode*astNode = Tree_value(node);
    printf("%s\n",astNode->name);
    List * children = Tree_children(node);
    size_t count = List_count(children);
    for(int i = 0; i< count; i++){
        void * child = List_at(children,i);
        printed(child,newIndent,0,i==count-1);
    }
    free((void*)newIndent);
}

void fprint(Tree * node, const char * indent, int root, int last,FILE*file){
    fprintf(file,"%s",indent);
    char * newIndent = NULL;
    if(last){
        if(!root){
            fprintf(file,"➡ ");
            newIndent = str_append(indent,"၀၀");
        } else{
            newIndent = str_append(indent,"");
        }
    } else{
        fprintf(file,"➡ ");
        newIndent = str_append(indent,"|၀");
    }
    AstNode*astNode = Tree_value(node);
    fprintf(file,"%s\n",astNode->name);
    List * children = Tree_children(node);
    size_t count = List_count(children);
    for(int i = 0; i< count; i++){
        void * child = List_at(children,i);
        fprint(child,newIndent,0,i==count-1,file);
    }
    free((void*)newIndent);
}
void printAstTree(Tree * astTree){
    if(astTree){
        char * indent = strdup("");
        printed(astTree,indent,1,1);
        free((void *)indent);
    }
}

void filePrintAstTree(Tree * astTree,FILE * file){
    char * indent = strdup("");
    fprint(astTree,indent,1,1,file);
    free((void *)indent);
}
void astTreeFilePrint(Tree * astTree,const char*fileName) {
    FILE*file = fopen(fileName,"w");
    if(astTree){
        filePrintAstTree(astTree,file);
    }
    else fprintf(file,"ERROR");
    fclose(file);
}