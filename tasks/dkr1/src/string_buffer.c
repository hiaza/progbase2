
#define __STDC_WANT_LIB_EXT2__ 1
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <string_buffer.h>
#include <stdio.h>
#include <stdarg.h>
struct __StringBuffer {
    char * buffer;
    size_t capacity;
    size_t length;
};

enum {
    INITIAL_CAPACITY = 256
};

StringBuffer * StringBuffer_new(void) {
    StringBuffer * self = malloc(sizeof(StringBuffer));
    self->capacity = INITIAL_CAPACITY;
    self->length = 1;
    self->buffer = malloc(sizeof(char) * self->capacity);
    self->buffer[0] = '\0';
    return self;
}

void StringBuffer_free(StringBuffer * self) {
    free(self->buffer);
    free(self);
}

void StringBuffer_appendChar(StringBuffer * self, char ch) {
    //self->capacity = self->capacity+1;
    //self->buffer = realloc(self->buffer,sizeof(char) * self->capacity);
    size_t newBufferLength = self->length + 1;
    if (newBufferLength > self->capacity) {
        size_t newCapacity = self->capacity * 2;
        char * newBuffer = realloc(self->buffer, sizeof(char) * newCapacity);
        if (newBuffer == NULL) {
            // out of mem err
            fprintf(stderr, "Out of memory on StringBuffer realloc");
            abort();
        }
        self->buffer = newBuffer;
    }
    self->buffer[self->length - 1] = ch;
    self->buffer[self->length] = '\0';
    self->length++;
}

void StringBuffer_append(StringBuffer * self, const char * str) {
    size_t strLen = strlen(str);
    size_t newBufferLength = self->length + strLen;
    if (newBufferLength > self->capacity) {
        size_t newCapacity = self->capacity * 2;
        char * newBuffer = realloc(self->buffer, sizeof(char) * newCapacity);
        if (newBuffer == NULL) {
            // out of mem err
            fprintf(stderr, "Out of memory on StringBuffer realloc");
            abort();
        }
        self->buffer = newBuffer;
        self->capacity = newCapacity;
    }
    strcat(self->buffer + (self->length - 1), str);
    self->length += strLen;
}


char * StringBuffer_toNewString(StringBuffer * self) {
    return (self->buffer);
}

char * StringBuffer_toString(StringBuffer * self) {
    if (self != NULL){
        if(self -> buffer != NULL){
            char * str = self -> buffer;
            return strdup(str);
        }
    }
    return NULL;
}
void StringBuffer_appendFormat(StringBuffer * self, const char * fmt, ...){
    va_list vlist;
    va_start(vlist,fmt);
    size_t bufsz = vsnprintf(NULL,0,fmt,vlist);
    char * buf = malloc (bufsz+1);
    va_start(vlist,fmt);
    vsnprintf(buf,bufsz+1,fmt,vlist);
    va_end(vlist);
    StringBuffer_append(self,buf);
    free(buf);
}