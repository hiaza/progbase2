#include <dict.h>
#include <string.h>
typedef struct{
    char *key;
    void *value;
} keyValuePair;

struct __Dict {
    List * pairs;
};

keyValuePair* kvp_new(char*key, void * value){
    keyValuePair * self = malloc(sizeof(keyValuePair));
    self->key = key;
    self->value = value;
    return self;
}

void kvp_free(keyValuePair * self){
    free(self);
}

Dict * Dict_new(void){
    Dict * self = malloc(sizeof(Dict));
    self->pairs = List_new();
    return self;
}
void Dict_free(Dict * self){
    for(int i = 0;i<List_count(self->pairs);i++){
        keyValuePair*kvp = List_at(self->pairs,i);
        kvp_free(kvp);   
    }
    List_free(self->pairs);
    free(self);
}

void   Dict_add      (Dict * self, char * key, void * value){
    keyValuePair * pair = kvp_new(key,value);
    List_add(self->pairs,pair);
}
bool   Dict_contains (Dict * self, char * key){
    for (int i = 0; i < List_count(self->pairs);i++){
        keyValuePair * pair = List_at(self->pairs,i);
        if(0 == strcmp(pair->key,key)) return true;
    }
    return false;
}
void * Dict_get      (Dict * self, char * key){
    for (int i = 0; i < List_count(self->pairs);i++){
        keyValuePair * pair = List_at(self->pairs,i);
        if(0 == strcmp(pair->key,key)) return pair->value;
    }
    // todo error
    return NULL;
}
void * Dict_set      (Dict * self, char * key, void * value){
    for (int i = 0; i < List_count(self->pairs);i++){
        keyValuePair * pair = List_at(self->pairs,i);
        if(0 == strcmp(pair->key,key)){
            void * oldValue = pair->value;
            pair->value = value;
            return oldValue;
        }
    }
    //todo error
    return NULL;
}
void * Dict_remove   (Dict * self, char * key){
    int index = -1;
    for (int i = 0; i < List_count(self->pairs);i++){
        keyValuePair * pair = List_at(self->pairs,i);
        if(0 == strcmp(pair->key,key)){
            index = 1;
            break;
        }
    }
    if(index>-1){
        keyValuePair * removedPair = List_removeAt(self->pairs,index);
        void * oldValue = removedPair->value;
        kvp_free(removedPair);
        return oldValue;
    }

    //todo error
    return NULL;
}
void   Dict_clear    (Dict * self){
    for (int i = List_count(self->pairs)-1;i>=0;i--){
        keyValuePair * pair = List_at(self->pairs,i);
        (void) List_removeAt(self->pairs,i);
    }
}
size_t Dict_count    (Dict * self){
    return List_count(self->pairs);
}

void   Dict_keys     (Dict * self, List * keys){
    for (int i = 0; i < List_count(self->pairs);i++){
        keyValuePair * pair = List_at(self->pairs,i);
        List_add(keys,pair->key);
    }
}
void   Dict_values   (Dict * self, List * values){
    for (int i = 0; i < List_count(self->pairs);i++){
        keyValuePair * pair = List_at(self->pairs,i);
        List_add(values,pair->value);
    }
}