#include <tree.h> 
#include <list.h>
#include <ast.h> 
Tree * Tree_new (void * value){
    Tree * self = malloc(sizeof(Tree));
    self -> value = value;
    self -> children = List_new();
    return self; 
}

void astTree_free(Tree * self){
   if(self!=NULL){
       for (int i = 0; i<List_count(self->children);i++){
           astTree_free((Tree*)List_at(self->children,i));
       }
       AstNode_free((AstNode*)self->value);
       Tree_free(self);
   } else return;
}


void Tree_free(Tree * self){
    List_free(self->children);
    free(self);
}



List * Tree_children(Tree * self){
    return self->children;
}
void * Tree_value(Tree * self){
    return self->value;
}