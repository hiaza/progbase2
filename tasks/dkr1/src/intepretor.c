#include <intepretor.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <ast.h>
#include <math.h>
#include <strdup.h>
#include <dict.h>
#include <stdFunc.h>

struct __Program{
    Dict * variables;
    Dict * functions; 
    char * error;
};
void Program_setError(Program*self,char * error){
    self->error = error;
}

typedef struct {
    Function ptr;
}StdFunction;

StdFunction * StdFunction_new(Function ptr){
    StdFunction*self = malloc (sizeof(StdFunction));
    self->ptr = ptr;
    return self;
}

void StdFunction_free(StdFunction*self){
    free(self);
}

Value * eval(Program * program,Tree * node);

 Value * Value_new(ValueType type, void * value){
     Value * self = malloc(sizeof(Value));
     self->type = type;
     self->value = value;
     return self;
 }
 void Value_free(Value * self){
     if(self->type == ValueType_ARRAY){ }

     else {
         free(self->value);
         free(self);
     }
 }
 void ValueType_print(ValueType self){
     switch(self){
         case ValueType_NUMBER: printf("number\n"); break;
         case ValueType_STRING: printf("string\n");break;
         case ValueType_BOOL: printf("bool\n");break;
         case ValueType_ARRAY: printf("array\n");break;
         case ValueType_UNDEFINED: printf("undefined\n");break;
         default:printf("<?>");break;
     }
 }
 void Value_print(Value * self){
    switch(self->type){
        case ValueType_NUMBER: {
           double val = *(double*) self->value;
           printf("%.3lf | ",val); 
           break;
        }
        case ValueType_STRING:{
           char * val = (char*) self->value;
           printf("\"%s\" | ",val); 
           break;
        }
        case ValueType_BOOL:{
           bool  val = *(bool*) self->value;
           printf("%s | ",val ? "TRUE" : "FALSE"); 
           break;
        }
        case ValueType_ARRAY:{
            List * val = (List*) self->value;
            for(int i = 0; i<List_count(val);i++){
                double temp = *(double*) (List_at(val,i));
                printf("%.3lf | ",temp); 
            }
            break;
         }
        case ValueType_UNDEFINED: {
           printf("undefined | "); 
           break;
        }
        default:printf("<?>");break;
    }
    ValueType_print(self->type);
 }


Value * Value_newNumber(double number){
    double * numberMem = malloc(sizeof(double));
    *numberMem = number;
    return Value_new(ValueType_NUMBER, numberMem);
}


Value * Value_newString(const char * str){
    return Value_new(ValueType_STRING,strdup((char*)str));
}

Value * Value_newBool(bool boolean){
    bool * mem = malloc(sizeof(bool));
    *mem = boolean;
    return Value_new(ValueType_BOOL,mem);
}

Value * Value_newUndefined(void){
    return Value_new(ValueType_UNDEFINED, NULL);
}

double Value_number(Value*self){
    assert(self->type == ValueType_NUMBER);
    return *((double*)self->value);
}

bool Value_bool(Value * self){
    assert(self->type == ValueType_BOOL);
    return *((bool*)self->value);
}
char * Value_string(Value * self){
    assert(self->type == ValueType_STRING);
    return ((char*)self->value);
}
List * Value_array(Value * self){
    assert(self->type == ValueType_ARRAY);
    return ((List*)self->value);
}
bool Value_equals(Value * a, Value * b){
    if(a->type != b->type) return false;
    switch(a->type){
        case ValueType_BOOL: return Value_bool(a) == Value_bool(b);
        case ValueType_NUMBER: return fabs(Value_number(a) - Value_number(b)) < 1e-6;
        case ValueType_STRING: return 0 == strcmp(Value_string(a), Value_string(b));
        case ValueType_UNDEFINED: return true;   
        default: assert(0 && "Not supported");
    }
}
bool Value_lessThen(Value * a, Value * b){
    if((a->type != b->type)||a->type == ValueType_BOOL) return false;
    switch(a->type){
        case ValueType_NUMBER: return Value_number(a) - Value_number(b) < 0;
        case ValueType_STRING: return strcmp(Value_string(a), Value_string(b))<0;
        case ValueType_UNDEFINED: return false;   
        default: assert(0 && "Not supported");
    }
}
bool Value_lessEquals(Value * a, Value * b){
    if(a->type != b->type) return false;
    switch(a->type){
        case ValueType_BOOL: return Value_bool(a) == Value_bool(b);
        case ValueType_NUMBER: return (Value_number(a) - Value_number(b) < 0)||(fabs(Value_number(a) - Value_number(b)) < 1e-6);
        case ValueType_STRING: return strcmp(Value_string(a), Value_string(b))<=0;
        case ValueType_UNDEFINED: return true;   
        default: assert(0 && "Not supported");
    }
}
bool Value_greaterThen(Value * a, Value * b){
    if((a->type != b->type)||a->type == ValueType_BOOL) return false;
    switch(a->type){
        case ValueType_NUMBER: return Value_number(a) - Value_number(b) > 0;
        case ValueType_STRING: return strcmp(Value_string(a), Value_string(b))>0;
        case ValueType_UNDEFINED: return false;   
        default: assert(0 && "Not supported");
    }
}
bool Value_greaterEquals(Value * a, Value * b){
    if(a->type != b->type) return false;
    switch(a->type){
        case ValueType_BOOL: return Value_bool(a) == Value_bool(b);
        case ValueType_NUMBER: return (Value_number(a) - Value_number(b) > 0)||(fabs(Value_number(a) - Value_number(b)) < 1e-6);
        case ValueType_STRING: return strcmp(Value_string(a), Value_string(b))>=0;
        case ValueType_UNDEFINED: return true;   
        default: assert(0 && "Not supported");
    }
}
bool Value_asBool(Value * self){
    switch(self->type){
        case ValueType_BOOL: return Value_bool(self);
        case ValueType_NUMBER: return fabs(Value_number(self)) > 1e-6;
        case ValueType_STRING: return Value_string(self) != NULL; // todo empty string true or false 
        case ValueType_UNDEFINED: return false;   
        default: assert(0 && "Not supported");
    }
}

Value * Value_newCopy(Value * origin){
    switch(origin->type){
        case ValueType_BOOL: return Value_newBool(Value_bool(origin));
        case ValueType_NUMBER: return Value_newNumber(Value_number(origin));
        case ValueType_STRING: return Value_newString(Value_string(origin));
        case ValueType_ARRAY: return origin;  // todo empty string true or false 
        case ValueType_UNDEFINED: assert(0 && " Implement undefined");return NULL;   
        default: assert(0 && "Not supported"); return NULL;
    }
}

Value * Program_getVariableValue(Program * self,char * varId){
    if(!Dict_contains(self->variables,varId)){
        self->error = strdup("Var id not found");
   //     free(varId);
        return NULL;
    }
    return Dict_get(self->variables,varId);
}

Value * eval(Program * program,Tree * node){
    AstNode * astNode = node ->value;
    switch(astNode->type){
        case AstNodeType_NUMBER:{
            double number = atof(astNode->name);
            return Value_newNumber(number);
        }
        case AstNodeType_STRING:{
            return Value_newString(astNode->name);
        }
        case AstNodeType_PRAVDA:{
            return Value_newBool(true);
        }
        case AstNodeType_NOPE:{
            return Value_newBool(false);
        }
        case AstNodeType_ID:{
            char * varId = astNode -> name;
            if(List_count(node->children)!=0){
                Tree * argListNode = List_at(node->children,0);
                AstNode * argList = argListNode->value;
                assert(argList->type == AstNodeType_ARGLIST);
                //
                if(!Dict_contains(program->functions,varId)){
                    program->error = strdup("Call unknown function");
                    return NULL;
                }
                //
                List*arguments = List_new();
                for(int i = 0; i<List_count(argListNode->children);i++){
                    Tree * argListChildNode = List_at(argListNode ->children,i);
                    Value * argumentValue = eval(program,argListChildNode);
                    if(program->error){
                        /*Value_free(argumentValue);*/
                        List_free(arguments);
                        //@todo
                        return NULL;
                    }
                    List_add(arguments,argumentValue);
                }
                //call function
                StdFunction * func = Dict_get(program->functions,varId);
                Value * relValue = func->ptr(program,arguments);
                //@todo free all arguments values
                for(int i = List_count(arguments)-1;i >= 0;i--){
                    Value * temp = (Value*)List_at(arguments,i);
                    Value_free(temp);
                }
                List_free(arguments);
               // assert(0 && "var call not impl");
                return relValue;
            }
            Value * varValue = Program_getVariableValue(program,varId);
            if(program->error) {
                return NULL;
            }
            return Value_newCopy(varValue);
            
        }
        case AstNodeType_ADD:{
            Tree * firstChild = List_at(node->children,0);
            Value * firstValue = eval(program,firstChild);
            if(program->error) return NULL;
            
            if(firstValue->type != ValueType_NUMBER){
                program->error = strdup("Invalid operation\n");
                Value_free(firstValue);
                return NULL;
            }
            int nchild = List_count(node->children);
            if(nchild == 1){
                  return firstValue;
            } else {
                Tree * secondChild = List_at(node ->children,1);
                Value * secondValue = eval(program,secondChild);
                if(program->error) return NULL;
                if(secondValue->type != ValueType_NUMBER){
                    program->error = strdup("Invalid operation\n");
                    Value_free(firstValue);
                    Value_free(secondValue);
                    return NULL;
                }
                double res = Value_number(firstValue) + Value_number(secondValue);
                Value_free(firstValue);
                Value_free(secondValue);
                return Value_newNumber(res);
            }
        }
        case AstNodeType_SUB:{
            Tree * firstChild = List_at(node->children,0);
            Value * firstValue = eval(program,firstChild);
            if(program->error) return NULL;
            
            if(firstValue->type != ValueType_NUMBER){
                program->error = strdup("Invalid operation\n");
                Value_free(firstValue);
                return NULL;
            }
            int nchild = List_count(node->children);
            if(nchild == 1){
                double res  = 0 - Value_number(firstValue);
                  Value_free(firstValue);
                  return Value_newNumber(res);
            } else {
                Tree * secondChild = List_at(node ->children,1);
                Value * secondValue = eval(program,secondChild);
                if(program->error) return NULL;
                if(secondValue->type != ValueType_NUMBER){
                    program->error = strdup("Invalid operation\n");
                    Value_free(firstValue);
                    Value_free(secondValue);
                    return NULL;
                }
                double res = Value_number(firstValue) - Value_number(secondValue);
                Value_free(firstValue);
                Value_free(secondValue);
                return Value_newNumber(res);
            }
        }
        default:{
            break;
        }
    }
    if(List_count(node ->children)!=2){
        program->error = strdup("Incorrect number of values near the operations of porivniannya");
        return NULL;
    }
    switch(astNode->type){
        case AstNodeType_ASSIGN:{
            Tree * firstChildNode = List_at(node->children,0);
            AstNode * firstChild = firstChildNode ->value;
            if(firstChild->type != AstNodeType_ID){
                program->error = strdup("Can't assign to rvalue");
                return NULL;
            }
            char * varId = firstChild ->name;

            Tree * secondChild = List_at(node ->children,1);
            Value * secondValue = eval(program,secondChild);
            if(program->error && secondValue){
                Value_free(secondValue);
                return NULL;
            }
            if(program->error) return NULL;
            if(!Dict_contains(program -> variables,varId)){
                program ->error = strdup("Var for assign not found");
                Value_free(secondValue);
                return NULL;
            }
            Value * oldValue = Dict_set(program -> variables, varId,Value_newCopy(secondValue));
            Value_free(oldValue);

            return secondValue;    
            break;
        }
        
        case AstNodeType_MUL:{
            Tree * firstChild = List_at(node->children,0);
            Value * firstValue = eval(program,firstChild);
            if(program->error) return NULL;
            if(firstValue->type != ValueType_NUMBER){
                program->error = strdup("Invalid operation\n");
                Value_free(firstValue);
                return NULL;
            }
            Tree * secondChild = List_at(node ->children,1);
            Value * secondValue = eval(program,secondChild);
            if(program->error) return NULL;
            if(secondValue->type != ValueType_NUMBER){
                program->error = strdup("Invalid operation\n");
                Value_free(firstValue);
                Value_free(secondValue);
                return NULL;
            }
            double res = Value_number(firstValue) * Value_number(secondValue);
            Value_free(firstValue);
            Value_free(secondValue);
            return Value_newNumber(res);            
        }
        case AstNodeType_MOD:{
            Tree * firstChild = List_at(node->children,0);
            Value * firstValue = eval(program,firstChild);
            if(program->error) return NULL;
            if(firstValue->type != ValueType_NUMBER){
                program->error = strdup("Invalid operation\n");
                Value_free(firstValue);
                return NULL;
            }
            Tree * secondChild = List_at(node ->children,1);
            Value * secondValue = eval(program,secondChild);
            if(program->error) return NULL;
            if(secondValue->type != ValueType_NUMBER){
                program->error = strdup("Invalid operation\n");
                Value_free(firstValue);
                Value_free(secondValue);
                return NULL;
            }

            
            double res = (int)Value_number(firstValue) % (int)Value_number(secondValue);
            Value_free(firstValue);
            Value_free(secondValue);
            return Value_newNumber(res);            
        }
        case AstNodeType_DIV:{
            Tree * firstChild = List_at(node->children,0);
            Value * firstValue = eval(program,firstChild);
            if(program->error) return NULL;
            if(firstValue->type != ValueType_NUMBER){
                program->error = strdup("Invalid operation\n");
                Value_free(firstValue);
                return NULL;
            }
            Tree * secondChild = List_at(node ->children,1);
            Value * secondValue = eval(program,secondChild);
            if(program->error) return NULL;
            if(secondValue->type != ValueType_NUMBER){
                program->error = strdup("Invalid operation\n");
                Value_free(firstValue);
                Value_free(secondValue);
                return NULL;
            }
            if(fabs(Value_number(secondValue))< 1e-6){
                program->error = strdup("Invalid operation\n");
                Value_free(firstValue);
                Value_free(secondValue);
                return NULL;
            }
            double res = Value_number(firstValue) / Value_number(secondValue);
            Value_free(firstValue);
            Value_free(secondValue);
            return Value_newNumber(res);
            
        }
        case AstNodeType_AND:{
            Tree * firstChild = List_at(node->children,0);
            Value * firstValue = eval(program,firstChild);
            if(program->error) return NULL;
            Tree * secondChild = List_at(node ->children,1);
            Value * secondValue = eval(program,secondChild);
            if(program->error) return NULL;
            bool res = Value_asBool(firstValue) && Value_asBool(secondValue);
            Value_free(firstValue);
            Value_free(secondValue);
            return Value_newBool(res);            
        }
        case AstNodeType_OR:{
            Tree * firstChild = List_at(node->children,0);
            Value * firstValue = eval(program,firstChild);
            if(program->error) return NULL;
            Tree * secondChild = List_at(node ->children,1);
            Value * secondValue = eval(program,secondChild);
            if(program->error) return NULL;
            bool res = Value_asBool(firstValue) || Value_asBool(secondValue);
            Value_free(firstValue);
            Value_free(secondValue);
            return Value_newBool(res);            
        }
        case AstNodeType_EQ:{
            Tree * firstChild = List_at(node->children,0);
            Value * firstValue = eval(program,firstChild);
            if(program->error) return NULL;
            Tree * secondChild = List_at(node ->children,1);
            Value * secondValue = eval(program,secondChild);
            if(program->error) return NULL;
            bool res = Value_equals(firstValue,secondValue);
            Value_free(firstValue);
            Value_free(secondValue);
            return Value_newBool(res);            
        }
        case AstNodeType_NEQ:{
            Tree * firstChild = List_at(node->children,0);
            Value * firstValue = eval(program,firstChild);
            if(program->error) return NULL;
            Tree * secondChild = List_at(node ->children,1);
            Value * secondValue = eval(program,secondChild);
            if(program->error) return NULL;
            bool res = !(Value_equals(firstValue,secondValue));
            Value_free(firstValue);
            Value_free(secondValue);
            return Value_newBool(res);            
        }
        case AstNodeType_LT:{
            Tree * firstChild = List_at(node->children,0);
            Value * firstValue = eval(program,firstChild);
            if(program->error) return NULL;
            Tree * secondChild = List_at(node ->children,1);
            Value * secondValue = eval(program,secondChild);
            if(program->error) return NULL;
            bool res = Value_lessThen(firstValue,secondValue);
            Value_free(firstValue);
            Value_free(secondValue);
            return Value_newBool(res);            
        }
        case AstNodeType_GT:{
            Tree * firstChild = List_at(node->children,0);
            Value * firstValue = eval(program,firstChild);
            if(program->error) return NULL;
            Tree * secondChild = List_at(node ->children,1);
            Value * secondValue = eval(program,secondChild);
            if(program->error) return NULL;
            bool res = Value_greaterThen(firstValue,secondValue);
            Value_free(firstValue);
            Value_free(secondValue);
            return Value_newBool(res);            
        }
        case AstNodeType_LE:{
            Tree * firstChild = List_at(node->children,0);
            Value * firstValue = eval(program,firstChild);
            if(program->error) return NULL;
            Tree * secondChild = List_at(node ->children,1);
            Value * secondValue = eval(program,secondChild);
            if(program->error) return NULL;
            bool res = Value_lessEquals(firstValue,secondValue);
            Value_free(firstValue);
            Value_free(secondValue);
            return Value_newBool(res);            
        }
        case AstNodeType_GE:{
            Tree * firstChild = List_at(node->children,0);
            Value * firstValue = eval(program,firstChild);
            if(program->error) return NULL;
            Tree * secondChild = List_at(node ->children,1);
            Value * secondValue = eval(program,secondChild);
            if(program->error) return NULL;
            bool res = Value_greaterEquals(firstValue,secondValue);
            Value_free(firstValue);
            Value_free(secondValue);
            return Value_newBool(res);            
        }
        default:{
            assert(0 && "Not implemented");
            break;
        }
    }
    return NULL;
}

void executeStatement(Program*program, Tree * childNode);

void executeYak(Program * program,Tree * node){
    Tree * exprNode = List_at(node->children,0);
    Value * testValue = eval(program,exprNode); 
    if(program->error) return;
    bool testBool = Value_asBool(testValue);
    Value_free(testValue);
    if(testBool){
        executeStatement(program, List_at(node->children,1));
    }else if(List_count(node->children) > 2){
        executeStatement(program, List_at(node->children,2));
    }
}

void executePoki(Program * program,Tree * node){
    Tree * exprNode = List_at(node->children,0);
    while(true){
        Value * testValue = eval(program,exprNode); 
        if(program->error) return;
        bool testBool = Value_asBool(testValue);
        Value_free(testValue);
        if(!testBool) break;
        executeStatement(program, List_at(node->children,1));
        if(program->error) return;
    }
}

void executeBlock(Program * program,Tree * blockTreeNode){
    for (int i = 0; i < List_count(blockTreeNode->children);i++){
        Tree * childNode = List_at(blockTreeNode->children,i);
        executeStatement(program,childNode);
        if (program ->error) break;
        //    
    }
}

void executeStatement(Program*program, Tree * childNode){
    AstNode * child = childNode -> value;
    switch(child->type){
        case AstNodeType_YAK:{
            executeYak(program,childNode);
            if(program->error) return;
            break;
        }
        case AstNodeType_POKI:{
            executePoki(program,childNode);
            if(program->error) return;
            break;
        }
        case AstNodeType_BLOCK:{
            executeBlock(program,childNode);
            if(program->error) return;
            break;
        }
        default:{ // expr
            Value * val = eval(program,childNode);
            if(program->error) return;
           /* Value_print(val);
            puts(" ");*/
            Value_free(val);
        
        }
    }
}

int Interpreter_execute(Tree * astTree){
    AstNode * astNode = astTree -> value;
    assert(astNode->type == AstNodeType_PROGRAM);
    Program program = {
        .variables = Dict_new(),
        .functions = Dict_new(),
        .error = NULL
    };
    //
    
    Dict_add(program.functions,"print",StdFunction_new(std_print));
    Dict_add(program.functions,"strlen",StdFunction_new(std_strlen));
    Dict_add(program.functions,"substr",StdFunction_new(std_substr));
    Dict_add(program.functions,"scan",StdFunction_new(std_scan));
    Dict_add(program.functions,"numtostr",StdFunction_new(std_numtostr));
    Dict_add(program.functions,"strtonum",StdFunction_new(std_strtonum));
    Dict_add(program.functions,"sqrt",StdFunction_new(std_sqrt));
    Dict_add(program.functions,"pow",StdFunction_new(std_pow));
    Dict_add(program.functions,"newArr",StdFunction_new(std_newArr));
    Dict_add(program.functions,"push",StdFunction_new(std_push));
    Dict_add(program.functions,"count",StdFunction_new(std_count));
    Dict_add(program.functions,"at",StdFunction_new(std_at));
    Dict_add(program.functions,"isdigit",StdFunction_new(std_isdigit));

    
    for(int i = 0 ; i < List_count(astTree->children);i++){
        Tree * childNode = List_at(astTree->children,i);
        AstNode * child = childNode -> value;
        switch(child->type){
            case AstNodeType_DECLAREVAR:{
                //
                Tree * fChildNode = List_at(childNode->children,0);
                AstNode * fChild = fChildNode -> value;
                Tree * sChildNode = List_at(childNode->children,1);
                AstNode * sChild = sChildNode -> value;
                //
                char * varName = strdup((char *)fChild->name);
                if(Dict_contains(program.variables,varName)){
                    program.error = strdup("Duplicate variable id");
                    free(varName);                    
                    break;
                }
                else{
                    Value * varValue = eval(&program,sChildNode);
                    Dict_add(program.variables,varName,varValue);
                }
               /* */
                
                break;
            }
            case AstNodeType_YAK:{
                executeYak(&program,childNode);
                if(program.error) break;
                break;
            }
            case AstNodeType_POKI:{
                executePoki(&program,childNode);
                if(program.error) break;
                break;
            }
            case AstNodeType_BLOCK:{
                executeBlock(&program,childNode);
                if(program.error) break;
                break;
            }
            default:{ // expr
                Value * val = eval(&program,childNode);
                if(program.error) break;
                /*Value_print(val);
                puts(" ");*/
                Value_free(val);
            }
        }
    }

    List*keys2 = List_new();
    List*keys = List_new();
    Dict_keys(program.variables,keys);
    for(int i = 0 ; i < List_count(keys);i++){
        char * key = List_at(keys,i);
        Value * val = Dict_get(program.variables,key);
       // printf(" '%s' : ",key);
        //Value_print(val);
        //puts(" ");
        if(val->type == ValueType_ARRAY){
           /* while(List_count(val->value)!=0){
                double * x = List_removeAt(val->value,0);
                free(x);
            }*/
            List_free(val->value);
            free(val);
        }
        else{
            Value_free(val);
        }
    }
    
    Dict_keys(program.functions,keys2);
    for(int i = 0 ; i < List_count(keys2);i++){
        char * key = List_at(keys2,i);
        StdFunction * temp = Dict_get(program.functions,key);
        //free(*temp->ptr);
        free(temp);
   
    }

    while(List_count(keys)!=0){
        char * x = List_removeAt(keys,0);
        free(x);
    }
  
    List_free(keys);
    List_free(keys2);
    
    // free all dict values
    Dict_free(program.variables);
    Dict_free(program.functions);
    if(program.error){
        fprintf(stderr,"Run-time error: %s\n",program.error);
        free(program.error);
        return 1;
    }
    return 0;
}