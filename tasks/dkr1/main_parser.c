#include <stdlib.h>
#include <lexer.h>
#include <file.h>
#include <list.h>
#include <parser.h>
#include <printAstTree.h>
#include <dict.h>
#include <intepretor.h>


int main (void){
    char *filePath = "main.lov";
    bool exists = fileExists(filePath);
    if (!exists)
    {
        printf("Файлу не існує");  

    }else {
        long size = getFileSize(filePath);
        char gt[size + 1];
        readFileToBuffer(filePath, gt, size);
        gt[size] = '\0';

        List * tokens = List_new(); 
        char * x = gt;       
    
        if (0 == Lexer_splitTokens(x, tokens)) {
            
            Lexer_printTokens(tokens);
            Tree * root = Parser_buildNewAstTree(tokens);
            puts(" ");
            puts("=======================");
            printAstTree(root);
            astTreeFilePrint(root,"AstTree.txt");
            puts("=======================");
            astTree_free(root);
        
        }else puts("...");        
        
        Lexer_clearTokens(tokens);
        List_free(tokens);
    }
    return 0;
}