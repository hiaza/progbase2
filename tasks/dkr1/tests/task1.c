#include <stdlib.h>
#include <lexer.h>
#include <file.h>
#include <check.h>
START_TEST (split_empty_empty)
{
   List * tokens = List_new();
   int status = Lexer_splitTokens("", tokens);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens), 0);
   List_free(tokens);
}
END_TEST

START_TEST (split_oneNumber_oneNumberToken)
{
   List * tokens = List_new();
   int status = Lexer_splitTokens("13", tokens);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens), 1);
   Token * firstToken = (Token *)List_at(tokens, 0);
   Token testToken = { TokenType_NUMBER, "13" };
   ck_assert(Token_equals(firstToken, &testToken));
   Lexer_clearTokens(tokens);
   List_free(tokens);
}
END_TEST
  
START_TEST (split_oneKeyword)
{
   List * tokens = List_new();
   int status = Lexer_splitTokens("beremo", tokens);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens), 1);
   Token * firstToken = (Token *)List_at(tokens, 0);
   Token testToken1 = { TokenType_BEREMO, "beremo" };
   ck_assert(Token_equals(firstToken, &testToken1));
   Lexer_clearTokens(tokens);
   List_free(tokens);
}
END_TEST

START_TEST (split_twoNumbers_twoTokens)
{
   List * tokens = List_new();
   int status = Lexer_splitTokens("23 45", tokens);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens), 2);
   Token * firstToken = (Token *)List_at(tokens, 0);
   Token testToken1 = { TokenType_NUMBER, "23" };
   Token * thirdToken = (Token *)List_at(tokens, 1);
   Token testToken3 = { TokenType_NUMBER, "45" };
   ck_assert(Token_equals(firstToken, &testToken1));
   ck_assert(Token_equals(thirdToken, &testToken3));
   Lexer_clearTokens(tokens);
   List_free(tokens);
}
END_TEST


START_TEST (split_oneBoolSumbAndTwoID_oneBoolAndTwoIdToken)
{
   List * tokens = List_new();
   int status = Lexer_splitTokens("x>=y", tokens);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens), 3);
   Token * firstToken = (Token *)List_at(tokens, 0);
   Token testToken1 = { TokenType_ID, "x" };
   Token * secondToken = (Token *)List_at(tokens, 1);
   Token testToken2 = { TokenType_GE, ">=" };
   Token * thirdToken = (Token *)List_at(tokens, 2);
   Token testToken3 = { TokenType_ID, "y" };
   ck_assert(Token_equals(firstToken, &testToken1));
   ck_assert(Token_equals(secondToken, &testToken2));
   ck_assert(Token_equals(thirdToken, &testToken3));
   Lexer_clearTokens(tokens);
   List_free(tokens);
}
END_TEST

START_TEST (split_twoBoolSumbAndOneAriphm_threeTokens)
{
   List * tokens = List_new();
   int status = Lexer_splitTokens("<<==", tokens);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens), 3);
   Token * firstToken = (Token *)List_at(tokens, 0);
   Token testToken1 = { TokenType_LT, "<" };
   Token * secondToken = (Token *)List_at(tokens, 1);
   Token testToken2 = { TokenType_LE, "<=" };
   Token * thirdToken = (Token *)List_at(tokens, 2);
   Token testToken3 = { TokenType_ASSIGN, "=" };
   ck_assert(Token_equals(firstToken, &testToken1));
   ck_assert(Token_equals(secondToken, &testToken2));
   ck_assert(Token_equals(thirdToken, &testToken3));
   Lexer_clearTokens(tokens);
   List_free(tokens);
}
END_TEST


START_TEST (split_invalidChar_errorCode)
{
   List * tokens = List_new();
   int status = Lexer_splitTokens("&", tokens);
   ck_assert_int_eq(status, 1);
   ck_assert_int_eq(List_count(tokens), 0);
   List_free(tokens);
}
END_TEST

/*
  @todo test other cases
*/
Suite *test_suite() {
    Suite *s = suite_create("Lexer");
    TCase *tc_sample = tcase_create("SplitTest");
    //
    tcase_add_test(tc_sample, split_empty_empty);
    tcase_add_test(tc_sample, split_oneNumber_oneNumberToken);
    tcase_add_test(tc_sample, split_invalidChar_errorCode);
    tcase_add_test(tc_sample, split_twoNumbers_twoTokens);
    tcase_add_test(tc_sample, split_oneKeyword);
    tcase_add_test(tc_sample, split_oneBoolSumbAndTwoID_oneBoolAndTwoIdToken);
    tcase_add_test(tc_sample, split_twoBoolSumbAndOneAriphm_threeTokens);
    //
    suite_add_tcase(s, tc_sample);
    return s;
   }
   
   int main(void) {
    Suite *s = test_suite();
    SRunner *sr = srunner_create(s);
    srunner_set_fork_status(sr, CK_NOFORK);  // important for debugging!
    srunner_run_all(sr, CK_VERBOSE);
   
    int num_tests_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return num_tests_failed;
   }
   