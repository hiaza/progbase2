#include <stdlib.h>
#include <lexer.h>
#include <file.h>
#include <check.h>
#include <parser.h>
#include <funForTest.h>

//-------------------------------------------------------------------------------------------------------------------------  
//                     testing var_decl

START_TEST (var_decl_pos_tests)
{
   List * tokens1 = List_new();
   int status = Lexer_splitTokens("beremo x = 0;", tokens1);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens1), 5);
   ck_assert_int_eq(ifBuildTree(tokens1), 0);
   Lexer_clearTokens(tokens1);
   List_free(tokens1);

   List * tokens2 = List_new();
   int status2 = Lexer_splitTokens("beremo _x34 = \"string\";", tokens2);
   ck_assert_int_eq(status2, 0);
   ck_assert_int_eq(List_count(tokens2), 5);
   ck_assert_int_eq(ifBuildTree(tokens2), 0);
   Lexer_clearTokens(tokens2);
   List_free(tokens2);

   List * tokens3 = List_new();
   int status3 = Lexer_splitTokens("beremo _x = 5.6 + 5.3;", tokens3);
   ck_assert_int_eq(status3, 0);
   ck_assert_int_eq(List_count(tokens3), 7);
   ck_assert_int_eq(ifBuildTree(tokens3), 0);
   Lexer_clearTokens(tokens3);
   List_free(tokens3);
   
   List * tokens4 = List_new();
   int status4 = Lexer_splitTokens("beremo _x = strlen(y);", tokens4);
   ck_assert_int_eq(status4, 0);
   ck_assert_int_eq(List_count(tokens4), 8);
   ck_assert_int_eq(ifBuildTree(tokens4), 0);
   Lexer_clearTokens(tokens4);
   List_free(tokens4);
}
END_TEST

START_TEST (var_decl_neg_tests)
{
   List * tokens1 = List_new();
   int status = Lexer_splitTokens("beremo x = ;", tokens1);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens1), 4);
   ck_assert_int_eq(ifBuildTree(tokens1), 1);
   Lexer_clearTokens(tokens1);
   List_free(tokens1);

   List * tokens2 = List_new();
   int status2 = Lexer_splitTokens("beremo x 0;", tokens2);
   ck_assert_int_eq(status2, 0);
   ck_assert_int_eq(List_count(tokens2), 4);
   ck_assert_int_eq(ifBuildTree(tokens2), 1);
   Lexer_clearTokens(tokens2);
   List_free(tokens2);

   List * tokens3 = List_new();
   int status3 = Lexer_splitTokens("beremo x = 0", tokens3);
   ck_assert_int_eq(status3, 0);
   ck_assert_int_eq(List_count(tokens3), 4);
   ck_assert_int_eq(ifBuildTree(tokens3), 1);
   Lexer_clearTokens(tokens3);
   List_free(tokens3);
   
   List * tokens4 = List_new();
   int status4 = Lexer_splitTokens("beremo x;", tokens4);
   ck_assert_int_eq(status4, 0);
   ck_assert_int_eq(List_count(tokens4), 3);
   ck_assert_int_eq(ifBuildTree(tokens4), 1);
   Lexer_clearTokens(tokens4);
   List_free(tokens4);

   List * tokens5 = List_new();
   int status5 = Lexer_splitTokens("beremo = 0;", tokens5);
   ck_assert_int_eq(status5, 0);
   ck_assert_int_eq(List_count(tokens5), 4);
   ck_assert_int_eq(ifBuildTree(tokens5), 1);
   Lexer_clearTokens(tokens5);
   List_free(tokens5);
}
END_TEST

//-------------------------------------------------------------------------------------------------------------------------  
//                     testing expr

START_TEST (expr_pos_tests)
{
   List * tokens1 = List_new();
   int status = Lexer_splitTokens("x = 3;", tokens1);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens1), 4);
   ck_assert_int_eq(ifBuildTree(tokens1), 0);
   Lexer_clearTokens(tokens1);
   List_free(tokens1);

   List * tokens2 = List_new();
   int status2 = Lexer_splitTokens("x = sqrt(y);", tokens2);
   ck_assert_int_eq(status2, 0);
   ck_assert_int_eq(List_count(tokens2), 7);
   ck_assert_int_eq(ifBuildTree(tokens2), 0);
   Lexer_clearTokens(tokens2);
   List_free(tokens2);

   List * tokens3 = List_new();
   int status3 = Lexer_splitTokens("x = \"Hey There\"; ", tokens3);
   ck_assert_int_eq(status3, 0);
   ck_assert_int_eq(ifBuildTree(tokens3), 0);
   Lexer_clearTokens(tokens3);
   List_free(tokens3);
   
   List * tokens4 = List_new();
   int status4 = Lexer_splitTokens("x = newArr();", tokens4);
   ck_assert_int_eq(status4, 0);
   ck_assert_int_eq(ifBuildTree(tokens4), 0);
   Lexer_clearTokens(tokens4);
   List_free(tokens4);
}
END_TEST
//-------------------------------------------------------------------------------------------------------------------------  
//                     testing select

START_TEST (select_pos_tests)
{
   List * tokens1 = List_new();
   int status = Lexer_splitTokens("yak (x > 0){ }", tokens1);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens1), 8);
   ck_assert_int_eq(ifBuildTree(tokens1), 0);
   Lexer_clearTokens(tokens1);
   List_free(tokens1);

   List * tokens2 = List_new();
   int status2 = Lexer_splitTokens("yak (x == 0){ print(x);}", tokens2);
   ck_assert_int_eq(status2, 0);
   ck_assert_int_eq(List_count(tokens2), 13);
   ck_assert_int_eq(ifBuildTree(tokens2), 0);
   Lexer_clearTokens(tokens2);
   List_free(tokens2);

   List * tokens3 = List_new();
   int status3 = Lexer_splitTokens("yak (x <= 0){ print(x);} inacshe{ x = x + 1;}", tokens3);
   ck_assert_int_eq(status3, 0);
   ck_assert_int_eq(ifBuildTree(tokens3), 0);
   Lexer_clearTokens(tokens3);
   List_free(tokens3);
   
   List * tokens4 = List_new();
   int status4 = Lexer_splitTokens("yak (x <= 0){ print(x); yak (x <= -6){ x = x + 1;}} inacshe{ x = x + 1;}", tokens4);
   ck_assert_int_eq(status4, 0);
   ck_assert_int_eq(ifBuildTree(tokens4), 0);
   Lexer_clearTokens(tokens4);
   List_free(tokens4);
}
END_TEST


START_TEST (selectAndExpr_neg_tests)
{
   List * tokens1 = List_new();
   int status = Lexer_splitTokens("yak (x > 0){ print(x); ", tokens1);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens1), 12);
   ck_assert_int_eq(ifBuildTree(tokens1), 1);
   Lexer_clearTokens(tokens1);
   List_free(tokens1);

   List * tokens2 = List_new();
   int status2 = Lexer_splitTokens("yak (x == 0) print(x);}", tokens2);
   ck_assert_int_eq(status2, 0);
   ck_assert_int_eq(List_count(tokens2), 12);
   ck_assert_int_eq(ifBuildTree(tokens2), 1);
   Lexer_clearTokens(tokens2);
   List_free(tokens2);

   List * tokens3 = List_new();
   int status3 = Lexer_splitTokens("yak (x == 0){ x = x + 1 }", tokens3);
   ck_assert_int_eq(status3, 0);
   ck_assert_int_eq(ifBuildTree(tokens3), 1);
   Lexer_clearTokens(tokens3);
   List_free(tokens3);

   List * tokens4 = List_new();
   int status4 = Lexer_splitTokens("yak (x == 0){ x = x  1; }", tokens4);
   ck_assert_int_eq(status4, 0);
   ck_assert_int_eq(ifBuildTree(tokens4), 1);
   Lexer_clearTokens(tokens4);
   List_free(tokens4);

   List * tokens5 = List_new();
   int status5 = Lexer_splitTokens("yak (x == 0){ x  x + 1; }", tokens5);
   ck_assert_int_eq(status5, 0);
   ck_assert_int_eq(ifBuildTree(tokens5), 1);
   Lexer_clearTokens(tokens5);
   List_free(tokens5);

   List * tokens6 = List_new();
   int status6 = Lexer_splitTokens("yak (x == 0){  = x + 1; }", tokens6);
   ck_assert_int_eq(status6, 0);
   ck_assert_int_eq(ifBuildTree(tokens6), 1);
   Lexer_clearTokens(tokens6);
   List_free(tokens6);

   List * tokens7 = List_new();
   int status7 = Lexer_splitTokens("yak (x == 0 { x = x + 1; }", tokens7);
   ck_assert_int_eq(status7, 0);
   ck_assert_int_eq(ifBuildTree(tokens7), 1);
   Lexer_clearTokens(tokens7);
   List_free(tokens7);

   List * tokens8 = List_new();
   int status8 = Lexer_splitTokens("yak x == 0) { x = x + 1; }", tokens8);
   ck_assert_int_eq(status8, 0);
   ck_assert_int_eq(ifBuildTree(tokens8), 1);
   Lexer_clearTokens(tokens8);
   List_free(tokens8);

   List * tokens9 = List_new();
   int status9 = Lexer_splitTokens("if (x == 0) { x = x + 1; }", tokens9);
   ck_assert_int_eq(status9, 0);
   ck_assert_int_eq(ifBuildTree(tokens9), 1);
   Lexer_clearTokens(tokens9);
   List_free(tokens9);

   List * tokens10 = List_new();
   int status10 = Lexer_splitTokens("yak (x  0) { x = x + 1; }", tokens10);
   ck_assert_int_eq(status10, 0);
   ck_assert_int_eq(ifBuildTree(tokens10), 1);
   Lexer_clearTokens(tokens10);
   List_free(tokens10);

   List * tokens11 = List_new();
   int status11 = Lexer_splitTokens("yak ( == 0) { x = x + 1; }", tokens11);
   ck_assert_int_eq(status11, 0);
   ck_assert_int_eq(ifBuildTree(tokens11), 1);
   Lexer_clearTokens(tokens11);
   List_free(tokens11);

   List * tokens12 = List_new();
   int status12 = Lexer_splitTokens("yak (x <= 0){ print(x);} inacshe{ x = x + 1;", tokens12);
   ck_assert_int_eq(status12, 0);
   ck_assert_int_eq(ifBuildTree(tokens12), 1);
   Lexer_clearTokens(tokens12);
   List_free(tokens12);
   
   List * tokens13 = List_new();
   int status13 = Lexer_splitTokens(" yak (x <= 0){ print(x);} inacshe x = x + 1;} ", tokens13);
   ck_assert_int_eq(status13, 0);
   ck_assert_int_eq(ifBuildTree(tokens13), 1);
   Lexer_clearTokens(tokens13);
   List_free(tokens13);

   List * tokens14 = List_new();
   int status14 = Lexer_splitTokens(" yak (x <= 0); ", tokens14);
   ck_assert_int_eq(status14, 0);
   ck_assert_int_eq(ifBuildTree(tokens14), 1);
   Lexer_clearTokens(tokens14);
   List_free(tokens14);
}
END_TEST

//-------------------------------------------------------------------------------------------------------------------------  
//                     testing iter

START_TEST (iter_pos_tests)
{
   List * tokens1 = List_new();
   int status = Lexer_splitTokens("poki (x > 0){ x = x + 1; }", tokens1);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens1), 14);
   ck_assert_int_eq(ifBuildTree(tokens1), 0);
   Lexer_clearTokens(tokens1);
   List_free(tokens1);

   List * tokens2 = List_new();
   int status2 = Lexer_splitTokens("poki (x) { x=x+1; print(x);}", tokens2);
   ck_assert_int_eq(status2, 0);
   ck_assert_int_eq(ifBuildTree(tokens2), 0);
   Lexer_clearTokens(tokens2);
   List_free(tokens2);

   List * tokens3 = List_new();
   int status3 = Lexer_splitTokens("poki (1){ yak(x>1){print(x); x = x+1;}} ", tokens3);
   ck_assert_int_eq(status3, 0);
   ck_assert_int_eq(ifBuildTree(tokens3), 0);
   Lexer_clearTokens(tokens3);
   List_free(tokens3);
}
END_TEST

START_TEST (iter_neg_tests)
{
   List * tokens1 = List_new();
   int status = Lexer_splitTokens("poki (x > 0){ print(x); ", tokens1);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(ifBuildTree(tokens1), 1);
   Lexer_clearTokens(tokens1);
   List_free(tokens1);

   List * tokens2 = List_new();
   int status2 = Lexer_splitTokens("poki (x == 0) print(x);}", tokens2);
   ck_assert_int_eq(status2, 0);
   ck_assert_int_eq(List_count(tokens2), 12);
   ck_assert_int_eq(ifBuildTree(tokens2), 1);
   Lexer_clearTokens(tokens2);
   List_free(tokens2);

   List * tokens3 = List_new();
   int status3 = Lexer_splitTokens("poki (x == 0){ x = x + 1 }", tokens3);
   ck_assert_int_eq(status3, 0);
   ck_assert_int_eq(ifBuildTree(tokens3), 1);
   Lexer_clearTokens(tokens3);
   List_free(tokens3);

   List * tokens7 = List_new();
   int status7 = Lexer_splitTokens("poki (x == 0 { x = x + 1; }", tokens7);
   ck_assert_int_eq(status7, 0);
   ck_assert_int_eq(ifBuildTree(tokens7), 1);
   Lexer_clearTokens(tokens7);
   List_free(tokens7);

   List * tokens8 = List_new();
   int status8 = Lexer_splitTokens("poki x == 0) { x = x + 1; }", tokens8);
   ck_assert_int_eq(status8, 0);
   ck_assert_int_eq(ifBuildTree(tokens8), 1);
   Lexer_clearTokens(tokens8);
   List_free(tokens8);

   List * tokens9 = List_new();
   int status9 = Lexer_splitTokens("pok (x == 0) { x = x + 1; }", tokens9);
   ck_assert_int_eq(status9, 0);
   ck_assert_int_eq(ifBuildTree(tokens9), 1);
   Lexer_clearTokens(tokens9);
   List_free(tokens9);

   List * tokens10 = List_new();
   int status10 = Lexer_splitTokens("poki (x  0) { x = x + 1; }", tokens10);
   ck_assert_int_eq(status10, 0);
   ck_assert_int_eq(ifBuildTree(tokens10), 1);
   Lexer_clearTokens(tokens10);
   List_free(tokens10);

   List * tokens11 = List_new();
   int status11 = Lexer_splitTokens("poki ( == 0) { x = x + 1; }", tokens11);
   ck_assert_int_eq(status11, 0);
   ck_assert_int_eq(ifBuildTree(tokens11), 1);
   Lexer_clearTokens(tokens11);
   List_free(tokens11);

   List * tokens14 = List_new();
   int status14 = Lexer_splitTokens(" poki (x <= 0); ", tokens14);
   ck_assert_int_eq(status14, 0);
   ck_assert_int_eq(ifBuildTree(tokens14), 1);
   Lexer_clearTokens(tokens14);
   List_free(tokens14);
}
END_TEST
/*
  @todo test other cases
*/
Suite *test_suite() {
    Suite *m = suite_create("Parser");
    TCase *t_sample = tcase_create("ParserTest");
    //
    tcase_add_test(t_sample, var_decl_pos_tests);
    tcase_add_test(t_sample, var_decl_neg_tests);
    tcase_add_test(t_sample, expr_pos_tests);
    tcase_add_test(t_sample, select_pos_tests);
    tcase_add_test(t_sample, selectAndExpr_neg_tests);
    tcase_add_test(t_sample, iter_pos_tests);
    tcase_add_test(t_sample, iter_neg_tests);
    //
    suite_add_tcase(m, t_sample);
    return m;
}
   
int main(void) {
    Suite *s = test_suite();
    SRunner *sr = srunner_create(s);
    srunner_set_fork_status(sr, CK_NOFORK);  // important for debugging!
    srunner_run_all(sr, CK_VERBOSE);
   
    int num_tests_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return num_tests_failed;
}
   