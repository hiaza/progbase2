#pragma once
#include <stdlib.h>
#include <stdbool.h>
#include <list.h>
typedef enum {
    TokenType_ASSIGN,
    TokenType_SEMICOLON,
    TokenType_PLUS,
    TokenType_MINUS,
    TokenType_MULT,
    TokenType_DIV,
    TokenType_OPENBR,
    TokenType_CLOSEBR,
    TokenType_OPENBLOCK,
    TokenType_CLOSEBLOCK,
    TokenType_SQOPENBR,
    TokenType_SQCLOSEBR,
    TokenType_COMMA,
    TokenType_LT,
    TokenType_GT,
    TokenType_NOT,
    TokenType_EQ,
    TokenType_LE,
    TokenType_GE,
    TokenType_NEQ,
    TokenType_AND,
    TokenType_OR,
    TokenType_BEREMO,
    TokenType_POKI,
    TokenType_YAK,
    TokenType_INACSHE,
    TokenType_MOD,
    TokenType_NUMBER,
    TokenType_STRING,
    TokenType_ID,
    TokenType_UNK,
    TokenType_PRAVDA,
    TokenType_NOPE
 } TokenType;

typedef struct __Token Token;

struct __Token {
   TokenType type;
   char * lexeme;
   // @todo extra data
};

const char*TokenType_toString(TokenType type);
Token * Token_new(TokenType type, char * name);
void Token_free(Token * self);
bool Token_equals(Token * self, Token * other);
int Lexer_splitTokens(const char *input, List *tokens);
void Lexer_clearTokens(List *tokens);
void Lexer_printTokens(List *tokens);
void Lexer_fprintTokens(List * tokens,const char*fileName);

char *strdup (const char *s);