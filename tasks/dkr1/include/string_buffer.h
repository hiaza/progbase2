#pragma once

typedef struct __StringBuffer StringBuffer;

StringBuffer * StringBuffer_new(void);
void StringBuffer_free(StringBuffer * self);

void StringBuffer_append(StringBuffer * self, const char * str);
//void StringBuffer_appenChar(StringBuffer * self, char ch);
//void StringBuffer
void StringBuffer_appendFormat(StringBuffer * self, const char * fnt, ...);
char * StringBuffer_toNewString(StringBuffer * self);
char * StringBuffer_toString(StringBuffer * self);
void StringBuffer_appendChar(StringBuffer * self, char ch);