#pragma once

typedef enum {
    AstNodeType_UNKNOWN,
    //
    AstNodeType_ASSIGN,
    AstNodeType_ADD,
    AstNodeType_SUB,
    AstNodeType_MUL,
    AstNodeType_DIV,
    AstNodeType_MOD,
    AstNodeType_EQ,
    AstNodeType_NEQ,
    AstNodeType_NOT,
    AstNodeType_GT,
    AstNodeType_LT,
    AstNodeType_LE,
    AstNodeType_GE,
    AstNodeType_AND,
    AstNodeType_OR,

    //...
    AstNodeType_NUMBER,
    AstNodeType_STRING,
    AstNodeType_ID,
    //
    AstNodeType_ARGLIST,
    //
    AstNodeType_BLOCK,
    AstNodeType_YAK,
    AstNodeType_POKI,
    AstNodeType_DECLAREVAR,
    AstNodeType_PRAVDA,
    AstNodeType_NOPE,
    //
    AstNodeType_PROGRAM,
} AstNodeType;

typedef struct __AstNode AstNode;
struct __AstNode {
    AstNodeType type;
    char * name;
    // @todo extra data
};

AstNode * AstNode_new(AstNodeType type, char * name);
void AstNode_free(AstNode * self);