#pragma once

#include <tree.h>

int Interpreter_execute(Tree * astTree);

typedef struct __Value Value;
typedef enum {
   ValueType_NUMBER,
   ValueType_BOOL,
   ValueType_STRING,
   ValueType_ARRAY,
   ValueType_UNDEFINED
} ValueType;

struct __Value {
   ValueType type;
   void * value;
};

typedef struct __Program Program;

Value * Value_new(ValueType type, void * value);

double Value_number(Value*self);

void Program_setError(Program*self,char * error);

void Value_print(Value*self); 

typedef Value * (*Function)(Program*program,List * values);

Value * Value_newNumber(double number);

Value * Value_newArray(List * temp);

List * Value_array(Value * self);

Value * Value_newString(const char * str);

Value * Value_newBool(bool boolean);

Value * Value_newUndefined(void);

char * Value_string(Value * self);