#pragma once

#include <stdlib.h>
#include <lexer.h>
#include <file.h>
#include <check.h>
#include <parser.h>

int ifBuildTree(List*tokens);