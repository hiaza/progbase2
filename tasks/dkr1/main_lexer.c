#include <stdlib.h>
#include <lexer.h>
#include <file.h>


int main (void){
    char *filePath = "main.lov";
    bool exists = fileExists(filePath);
    if (!exists)
    {
        printf("Файлу не існує");  
    }else{
        long size = getFileSize(filePath);
        char gt[size + 1];
        readFileToBuffer(filePath, gt, size);
        gt[size] = '\0';
       // puts("success");
       // puts(gt);
        List * tokens = List_new(); 
        char * x = gt;       
        //Lexer_splitTokens(x, tokens);
    
        if (0 == Lexer_splitTokens(x, tokens)) {
            
            Lexer_fprintTokens(tokens,"main_tokens.lov");
            
            
        }else puts("...");
        
        Lexer_clearTokens(tokens);
        List_free(tokens);
    }
    return 0;
}