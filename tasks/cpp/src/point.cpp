#include <point.h>
#include <math.h>
Point :: Point(int x1, int y1, int z1, string name) {
    this -> x = x1; 
    this -> y = y1; 
    this -> z = z1; 
    this->nameOfPoint = name;
    cout << "created" << endl;
}

Point :: ~Point() {
    cout << "deleted  " << this->nameOfPoint << endl;
}

void Point :: print(){
    cout << "=========================" << endl;
    cout << "Name:" << this->nameOfPoint << endl;
    cout << "-------------------------" << endl;
    cout << "X:  " << this->x;
    cout << "|  Y:  " << this->y;
    cout << "|  Z:  " << this->z << endl;
}

float Point :: LengthOfVector(){
    float length = sqrt(pow((this->x),2)+pow((this->y),2)+pow((this->z),2));
    return length;
}