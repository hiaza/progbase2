#include <iostream>
#include <limits>
#include <point.h>
#include <progbase-cpp/console.h>
#include <vector>

using namespace std;
using namespace progbase;
using namespace console;

const char menu[4][200] = {
    "1)Вивести вміст колекції із полями об'єктів у консоль.",
    "2)Додати до колекції новий елемент із заданням значень його полів із "
    "консолі.",
    "3)Вивести всі точки, відстань яких до (0, 0, 0) менша введеного L.",
    "4)Вийти з циклу і завершити роботу програми."};

int main(void) {

    Console::hideCursor();
    vector<Point *> points;

    bool flag = true;
    while (flag) {

        Console::clear();

        cout << menu[0] << endl;
        cout << menu[1] << endl;
        cout << menu[2] << endl;
        cout << menu[3] << endl;
        cout << endl;
        cout << endl;
        cout << "Введіть номер завдання" << endl;

        int n = 0;

        cin >> n;

        switch (n) {
        case 1: {
            Console::clear();
            for (int i = 0; i < points.size(); i++) {
                points.at(i)->print();
                cout << " " << endl;
            }
            cout << " " << endl;
            cout << "Натисніть будь-яку клавішу для продовження:" << endl;
            Console::getChar();
            break;
        }
        case 2: {
            Console::clear();
            cin.clear(); // на случай, если предыдущий ввод завершился с ошибкой
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            cout << "Введіть назву нової точки:" << endl;
            string name;
            getline(std::cin, name);
            Console::clear();

            cout << "Введіть координату x нової точки:" << endl;
            int x0 = 0;
            cin >> x0;
            Console::clear();

            cout << "Введіть координату y нової точки:" << endl;
            int y0 = 0;
            cin >> y0;
            Console::clear();

            cout << "Введіть координату z нової точки:" << endl;
            int z0 = 0;
            cin >> z0;
            Point * temp = new Point(x0, y0, z0, name);
            points.push_back(temp);
            break;
        }
        case 3: {
            Console::clear();
            cout << "Введіть L:" << endl;
            float l = 0;
            cin >> l;
            Console::clear();
            for (int i = 0; i < points.size(); i++) {
                if (points.at(i)->LengthOfVector() < l) {
                    points.at(i)->print();
                    cout << " " << endl;
                }
            }
            cout << " " << endl;
            cout << "Натисніть будь-яку клавішу для продовження:" << endl;
            Console::getChar();
            break;
        }
        case 4: {
            for (int i = points.size() - 1; i >= 0; i--) {
                delete (points.at(i));
            }
            flag = false;
            break;
        }
        default: { break; }
        }
    }
    Console::showCursor();
    return 0;
}
