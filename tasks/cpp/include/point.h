#pragma once 
#include <iostream>

using namespace std;

class Point{
    int x;
    int y;
    int z;
    string nameOfPoint;
public:
    Point(int x1, int y1, int z1, string name);
    ~Point();
    float LengthOfVector();
    void print();
};