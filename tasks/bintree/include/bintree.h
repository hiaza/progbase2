#pragma once

#include <stdlib.h>
#include <stdio.h>

typedef struct __BinTree BinTree;

BinTree * BinTree_new(int key);
void BinTree_free(BinTree * self);