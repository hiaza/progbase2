#include <stdlib.h>
#include <progbase.h>
#include <bintree.h>
#include <bstree.h>

int main(void){

   int values[] = {-19, -18, 3, -3, -13, 8, -12};

   BSTree * bst = BSTree_new();

   for(int i = 0; i < sizeof(values)/sizeof(values[0]);i++){
       BSTree_insert(bst,values[i]);
   } 

   BSTree_printFormat(bst);
   BSTree_printTraverseIO(bst);
   puts(" ");
   puts("=============================");
   BSTree_printTraversePO(bst);
   puts(" ");
   BSTree_clear(bst);

   return EXIT_SUCCESS;
}

