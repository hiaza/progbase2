#include <bstree.h>
#include <assert.h>

struct __BSTree {
    BinTree * root;
};

struct __BinTree {
    int value;
    BinTree * left;
    BinTree * right;
};

BSTree * BSTree_new(void){
    BSTree * self = malloc(sizeof(BSTree));
    if(self == NULL){
        return NULL;
    }
    self->root = NULL;
    return self;
}

void BSTree_free(BSTree * self){
    free(self);
}

void BSTree_insert(BSTree * self, int key){
    if (self != NULL){
        if (self->root == NULL){
            BinTree * node = BinTree_new(key);
            self -> root = node;
        }else if(key < self->root->value) {
            if (self->root->left == NULL) {
                self->root->left = BinTree_new(key);
            } else {
                BSTree *tmp = BSTree_new();
                tmp->root = self->root->left;
                BSTree_insert(tmp, key);
                free(tmp);
            }
        } else if(key>self->root->value){
            if(self->root->right == NULL){
                self->root->right = BinTree_new(key);
            }
            else{
                BSTree * tmp = BSTree_new();
                tmp->root = self->root->right;
                BSTree_insert(tmp,key);
                free(tmp);
            }
        }else if(key == self->root->value){
            assert(0 && "error");
            abort();
        }
    }
    else{
        assert(0 && "error");
        abort();
    }
}


void BSTree_clear(BSTree * self){
    if(self!=NULL){
        BinTree_free(self->root);
        free(self);
    }
}

void printlevel(BinTree * node,int level){
    for(int i = 0; i < level; i++){
        putchar('.');
        putchar('.');
    }
    if(node == NULL){
        printf("(null)\n");
    }else{
        printf("%i\n",node->value);
        if(node ->left || node ->right){
            printlevel(node ->left,level + 1);
            printlevel(node -> right,level + 1);
        }
    }
}

void BSTree_printFormat(BSTree * self){
    printlevel(self->root,0);
}

static void postOrder(BinTree *node){
    if(node->left) postOrder(node->left);
    if(node->right) postOrder(node -> right);
    printf("%i, ",node -> value);
}

static void inOrder(BinTree *node){
    if(node->left){
        inOrder(node->left);
    }

    printf("%i, ",node -> value);

    if(node->right){ 
        inOrder(node -> right);
    }
    
}


void BSTree_printTraversePO(BSTree * self){
    if (self ->root){
        postOrder(self->root);
    }
}


void BSTree_printTraverseIO(BSTree * self){
    if (self ->root){
        inOrder(self->root);
    }
}
