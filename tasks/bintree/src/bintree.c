#include <bintree.h>
#include <assert.h>


struct __BinTree {
    int key;
    BinTree * left;
    BinTree * right;
};

BinTree * BinTree_new(int key){

    BinTree * tree = malloc(sizeof(BinTree));
    if(tree == NULL){
        return NULL;
    }
    tree->left = NULL;
    tree->right = NULL;
    tree->key = key;
    return tree;
}

void BinTree_free(BinTree * self){

    if(self!=NULL){
        BinTree_free(self->right);
        BinTree_free(self->left);
        free(self);
    }
}