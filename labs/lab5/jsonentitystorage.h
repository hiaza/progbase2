#ifndef JSONENTITYSTORAGE_H
#define JSONENTITYSTORAGE_H
#include "entitys.h"

#include <QtXml>
#include <vector>
#include <iostream>
#include "writer.h"

class JsonEntityStorage: public EntityStorage {
public:
     JsonEntityStorage(string & name):EntityStorage(name){}
     vector <Writer*> load();
     void save(std::vector<Writer*> & entities);
};

#endif // JSONENTITYSTORAGE_H
