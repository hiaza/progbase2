#ifndef MESSAGEEXCEPTION_H
#define MESSAGEEXCEPTION_H


class MessageException : public std::exception {
    string _error;
public:
    MessageException(std::string & error) {
        this->_error = error;
    }
    const char * what() const noexcept {
        return this->_error.c_str();
    }
};
#endif // MESSAGEEXCEPTION_H
