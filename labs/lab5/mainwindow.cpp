#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QListWidgetItem>
#include <QListWidget>
#include <QFileDialog>
#include "writer.h"
#include <iostream>
#include <QMessageBox>
#include <QCloseEvent>
#include "edit.h"
#include <QtXml>
#include <vector>
#include <iostream>
#include "entitys.h"
#include "jsonentitystorage.h"
#include "storagemanager.h"
#include "file.h"
#include "messageexception.h"
using namespace std;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_addButton_clicked()
{
    Edit dialog(this);
    dialog.exec();
    if(dialog.accept){
         Writer * s = new Writer(dialog.name(),dialog.surname(),dialog.poem(),dialog.year(),dialog.rate());
         QListWidgetItem * item = new QListWidgetItem();
         item->setText(s->caption());
         QVariant variant;
         variant.setValue (s);
         item->setData(Qt::UserRole,variant);
         ui->ListM->addItem(item);
         ui->listWidget->clear();
    }
    cleanLabels();
}

void MainWindow::on_ListM_itemClicked(QListWidgetItem *item)
{
    QList<QListWidgetItem*> selectedItems = ui->ListM->selectedItems();
    if(selectedItems.count()!=0){
        ui->removeButton->setEnabled(true);
        ui->editButton ->setEnabled(true);

        QVariant variant = item->data(Qt::UserRole);
        Writer * s = variant.value<Writer*>();
        ui->label_name->setText(s->getName());
        ui->label_surname->setText(s->getSurName());
        ui->label_poem->setText(s->getPoem());
        ui->label_year->setText(QString::number(s->getYear()));
        ui->label_rate->setText(QString::number(s->getRating()));
    }
    else{
        ui->removeButton->setEnabled(true);
        ui->editButton ->setEnabled(true);
    }
}





void MainWindow::cleanLabels(){
    ui->label_name->setText("Choose element");
    ui->label_surname->setText("");
    ui->label_poem->setText("");
    ui->label_year->setText("");
    ui->label_rate->setText("");
}



void MainWindow::on_removeButton_clicked()
{
    QList<QListWidgetItem *> selectedItems = ui->ListM->selectedItems();
    if(selectedItems.count()!=0){
        QListWidgetItem * selectedItem = selectedItems.at(0);
        int selectedRow = ui->ListM->row(selectedItem);
        QListWidgetItem * item = ui -> ListM ->takeItem(selectedRow);
        cleanLabels();
        ui->listWidget->clear();
        delete item;
        ui->removeButton->setEnabled(false);
        ui->editButton ->setEnabled(false);
        cleanLabels();

    }
    else{

        //
    }
}


void MainWindow::on_editButton_clicked()
{
    QList<QListWidgetItem*> selectedItems = ui->ListM->selectedItems();
    if(selectedItems.count() == 1){
        QListWidgetItem * item  = selectedItems.at(0);
        QVariant variant = item->data(Qt::UserRole);
        Writer * s = variant.value<Writer*>();
        Edit dialog(this);
        dialog.setName(s->getName());
        dialog.setSurname(s->getSurName());
        dialog.setPoem(s->getPoem());
        dialog.setYear(s->getYear());
        dialog.setRate(s->getRating());
        dialog.exec();
        if(dialog.accept){
            s->setName(dialog.name());
            s->setSurName(dialog.surname());
            s->setPoem(dialog.poem());
            s->setYear(dialog.year());
            s->setRating(dialog.rate());
            item->setText(s->caption());
            ui->listWidget->clear();
        }
        ui->removeButton->setEnabled(false);
        ui->editButton ->setEnabled(false);
        cleanLabels();
    }
    else{
      //
    }
}


void MainWindow::on_findButton_clicked()
{
    int to = ui->yearVal->value();
    int counter = ui->ListM->count();
    ui->listWidget->clear();
    for(int i = 0;i<counter;i++){
        QListWidgetItem * item  = ui->ListM->item(i);
        QVariant variant = item->data(Qt::UserRole);
        Writer * s = variant.value<Writer*>();
        if(s->getYear()<=to){
            QListWidgetItem*real = new QListWidgetItem();
            real->setText(s->caption());
            ui->listWidget->addItem(real);
        }
    }
}
void MainWindow::closeEvent(QCloseEvent*ev){
    int res = QMessageBox::question(this,
                                                            "Exit","Are you sure?",
                                                            QMessageBox::Yes |QMessageBox::No,
                                                            QMessageBox::No);
    if (res == QMessageBox::Yes){
        ev->accept();
    }
    else {
        ev->ignore();
    }
}

void MainWindow::on_actionSave_in_Json_triggered()
{
    vector<Writer*> m;
    int counter = ui->ListM->count();
    for(int i = 0;i<counter;i++){
        QListWidgetItem * item  = ui->ListM->item(i);
        QVariant variant = item->data(Qt::UserRole);
        Writer * p = variant.value<Writer*>();
        m.push_back(p);
    }

    string temp = QFileDialog::getSaveFileName(
            this,
            tr("Save Document"),
            QDir::currentPath(),
            tr("Documents (*.xml *.json)")).toStdString();

      if(temp.length()>0){
          try{
              EntityStorage * self = StorageManager::createStorage(temp);
              self->save(m);
              QMessageBox::information(
                      this,
                      tr("Information"),
                      tr("All data was saved") );
          }
          catch(MessageException & err){

                QMessageBox messageBox;
                messageBox.critical(0,"Error",err.what());
                messageBox.setFixedSize(500,200);
          }
      }

}

void MainWindow::on_actionOpen_Json_triggered()
{
    QString fileDir = QFileDialog::getOpenFileName();

    string temp = fileDir.toStdString();
    if(temp.length()>0){
        try{
            EntityStorage * self = StorageManager::createStorage(temp);
            vector <Writer*> x = self->load();
            int counter = x.size();
            ui->ListM->clear();
            ui->listWidget->clear();
            for(int i = 0;i<counter;i++){
                Writer * s  = x.at(i);
                QListWidgetItem * item = new QListWidgetItem();
                item->setText(s->caption());
                QVariant variant;
                variant.setValue (s);
                item->setData(Qt::UserRole,variant);
                ui->ListM->addItem(item);
            }
        }
        catch(MessageException & err){
            QMessageBox messageBox;
            messageBox.critical(0,"Error",err.what());
            messageBox.setFixedSize(500,200);
        }
    }
}

void MainWindow::on_actionNew_file_triggered()
{
    ui->ListM->clear();
    ui->listWidget->clear();
}
