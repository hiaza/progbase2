#ifndef WRITER_H
#define WRITER_H

#include <QObject>

using namespace std;

class Writer : public QObject
{
    Q_OBJECT
public:
    explicit Writer(QObject *parent = nullptr);
    Writer(QString _name, QString _surname, QString _theGreatestPoem, int _yearOfBirth,float _rating);
    ~Writer();
    void setName(QString name);
    QString getName();
    void setSurName(QString surname);
    QString getSurName();
    void setPoem(QString poem);
    QString getPoem();
    void setYear(int year);
    int getYear();
    void setRating(float rating);
    float getRating();
    QString caption();

private:
    QString name;
    QString surname;
    QString theGreatestPoem;
    int yearOfBirth;
    float rating;
};

#endif // WRITER_H
