#-------------------------------------------------
#
# Project created by QtCreator 2018-04-15T16:55:27
#
#-------------------------------------------------

QT       += core gui
QT += xml

CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = lab5
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    writer.cpp \
    edit.cpp \
    file.cpp \
    jsonentitystorage.cpp \
    xmlentitystorage.cpp \
    storagemanager.cpp

HEADERS  += mainwindow.h \
    writer.h \
    edit.h \
    entitys.h \
    file.h \
    jsonentitystorage.h \
    xmlentitystorage.h \
    storagemanager.h \
    messageexception.h

FORMS    += mainwindow.ui \
    edit.ui

RESOURCES += \
    resource.qrc
