#include "jsonentitystorage.h"
#include "file.h"
#include <fstream>
#include "messageexception.h"

vector <Writer*> JsonEntityStorage :: load(){

    string str = fromFileToStr(this->name());

    ifstream temp(name());
    if(!temp.is_open()){
        string error = " File didn't exist";
        throw(MessageException(error));
        return vector <Writer*>();
    }
    temp.close();
    QJsonParseError err;
    QJsonDocument doc = QJsonDocument::fromJson(
           QByteArray::fromStdString(str),
           &err);
    if (err.error != QJsonParseError::NoError) {
            string errr = err.errorString().toStdString();
            throw(MessageException(errr));
            return vector <Writer*>();
    }

    vector <Writer*> m;
    QJsonObject vectorObj = doc.object();
    QJsonArray  actorArr = vectorObj.value("writers").toArray();
        for (int i = 0; i < actorArr.size(); i++) {
              QJsonValue value = actorArr.at(i);
              QJsonObject actorObj = value.toObject();
              Writer*g = new Writer (actorObj.value("name").toString(),
                      actorObj.value("surname").toString(),
                      actorObj.value("poem").toString(),
                      actorObj.value("year").toInt(),
                      actorObj.value("rating").toDouble());
              m.push_back(g);
        }
    return m;
}

void JsonEntityStorage :: save(std::vector<Writer*> & entities){
    if (entities.empty()){
        string err = "error empty vector!";
        throw(MessageException(err));
        return;
    }
        QJsonDocument doc;
        QJsonObject actorsObj;
        QJsonArray actorsArr;
        for (Writer* & m: entities) {
              QJsonObject mo;
              mo.insert("name", m->getName());
              mo.insert("surname", m->getSurName());
              mo.insert("poem",m->getPoem());
              mo.insert("year",m->getYear());
              mo.insert("rating", m->getRating());
              actorsArr.append(mo);
        }
        actorsObj.insert("writers", actorsArr);
        doc.setObject(actorsObj);
        saveToFile(this->name(),doc.toJson().toStdString());
}
