#include "xmlentitystorage.h"
#include <QtXml>
#include <vector>
#include <iostream>
#include "writer.h"
#include "file.h"
#include "messageexception.h"

using namespace std;
vector <Writer*> XmlEntityStorage :: load(){

    string str = fromFileToStr(this->name());

    ifstream temp(name());
    if(!temp.is_open()){
        string error = " File didn't exist";
        throw(MessageException(error));
        return vector <Writer*>();
    }
    temp.close();

    QDomDocument doc;
    if (!doc.setContent(QString::fromStdString(str))) {
          string err = "error parsing xml!";
          throw(MessageException(err));
          return vector<Writer*>();
    }
    vector <Writer*> g;
    QDomElement root = doc.documentElement();

    for (int i = 0; i < root.childNodes().length(); i++) {

          QDomNode actorNode = root.childNodes().at(i);
          QDomElement actorEl = actorNode.toElement();
          Writer * x = new Writer (actorEl.attribute("name"),
                                   actorEl.attribute("surname"),
                                   actorEl.attribute("poem"),
                                   actorEl.attribute("year").toInt(),
                                   actorEl.attribute("rating").toDouble());
          g.push_back(x);
    }
    return g;
}

void XmlEntityStorage :: save(std::vector<Writer*> & entities){
    if (entities.empty()){
        string err = "error empty vector!";
        throw(MessageException(err));
        return;
    }
    QDomDocument doc;
    QDomElement actorListEl = doc.createElement("WriterList");
    for (size_t i = 0; i < entities.size(); i++) {
          QDomElement actorEl = doc.createElement("writer");
          Writer*temp = entities.at(i);
          actorEl.setAttribute("name", temp->getName());
          actorEl.setAttribute("surname", temp->getSurName());
          actorEl.setAttribute("poem", temp->getPoem());
          actorEl.setAttribute("year", temp->getYear());
          actorEl.setAttribute("rating", temp->getRating());
          actorListEl.appendChild(actorEl);
    }
    doc.appendChild(actorListEl);
    saveToFile(this->name(),doc.toString().toStdString());
}
