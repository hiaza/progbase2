#include "edit.h"
#include "ui_edit.h"
#include <QDialogButtonBox>
#include <QPushButton>
Edit::Edit(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Edit)
{
    ui->setupUi(this);
    accept = false;
    ui->buttonBox->setEnabled(true);
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
}

Edit::~Edit()
{
    delete ui;
}
void Edit::setName(QString name){
    ui->ed_name->setText(name);
}

QString Edit::name(){
    return ui->ed_name->text();
}
void Edit:: setSurname(QString surname){
    ui->ed_surname->setText(surname);
}
QString Edit:: surname(){
    return ui->ed_surname->text();
}

void Edit:: setPoem(QString poem){
    ui->ed_poem->setText(poem);
}

QString Edit:: poem(){
    return ui->ed_poem->text();
}

void Edit:: setYear(int year){
    ui->ed_year->setValue(year);
}

int Edit:: year(){
    return ui->ed_year->value();
}

void Edit::setRate(float rate){
    ui->ed_rate->setValue(rate);
}

float Edit:: rate(){
    return ui->ed_rate->value();
}

void Edit::on_buttonBox_accepted()
{
    accept = true;
}

void Edit::on_buttonBox_rejected()
{
    accept = false;
}



void Edit::on_checkBox_clicked()
{
    QPushButton * ok = ui->buttonBox->button(QDialogButtonBox ::Ok);
    if(ok->isEnabled()){
          ok->setEnabled(false);
    }
    else {
          ok->setEnabled(true);
    }
}

void Edit::on_ed_name_textChanged(const QString &arg1)
{
    validate();
}


void Edit::on_ed_surname_textChanged(const QString &arg1)
{
    validate();
}

void Edit::on_ed_poem_textChanged(const QString &arg1)
{
    validate();
}
void Edit::validate(){
    bool invalidInput = ui -> ed_name->text().isEmpty()
            || ui->ed_surname->text().isEmpty()
            || ui->ed_poem->text().isEmpty();
    ui->checkBox->setEnabled(!invalidInput);
}
