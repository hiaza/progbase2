#ifndef XMLENTITYSTORAGE_H
#define XMLENTITYSTORAGE_H

#include <QtXml>
#include <vector>
#include <iostream>
#include "writer.h"
#include "entitys.h"

using namespace std;

class XmlEntityStorage: public EntityStorage {
public:
     XmlEntityStorage(string & name):EntityStorage(name){}
     vector <Writer*> load();
     void save(std::vector<Writer*> & entities);
};
#endif // XMLENTITYSTORAGE_H
