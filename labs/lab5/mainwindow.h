#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidgetItem>
#include "edit.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_addButton_clicked();

    void on_ListM_itemClicked(QListWidgetItem *item);

    void on_removeButton_clicked();

    void on_editButton_clicked();

    void cleanLabels();

    void closeEvent(QCloseEvent*ev);

    void on_findButton_clicked();

    void on_actionSave_in_Json_triggered();

    void on_actionOpen_Json_triggered();

    void on_actionNew_file_triggered();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
