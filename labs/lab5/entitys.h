#ifndef ENTITYS_H
#define ENTITYS_H
#include <QtXml>
#include <vector>
#include <iostream>
#include "writer.h"

class EntityStorage {
     std::string _name;
protected:
     EntityStorage(std::string & name) { this->_name = name; }
public:
     std::string & name() { return this->_name; }

     virtual std::vector<Writer*> load() = 0;
     virtual void save(std::vector<Writer*> & entities) = 0;
};

#endif // ENTITYS_H
