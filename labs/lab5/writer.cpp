#include "writer.h"
#include <iostream>
#include <QObject>
Writer::Writer(QObject *parent) : QObject(parent)
{

}

Writer::Writer(QString _name,QString _surname,QString _theGreatestPoem, int _yearOfBirth,float _rating){
    this->name = _name;
    this->surname = _surname;
    this->theGreatestPoem = _theGreatestPoem;
    this->yearOfBirth = _yearOfBirth;
    this->rating = _rating;
}

Writer::~Writer(){
    std::cout << "deleted" << std::endl;
}
void Writer::setName(QString name){
    this->name = name;
}
QString Writer:: getName(){
    return this->name;
}
void Writer::setSurName(QString surname){
    this->surname = surname;
}

QString Writer:: getSurName(){
    return this->surname;
}

void Writer:: setPoem(QString poem){
    this->theGreatestPoem = poem;
}

QString Writer:: getPoem(){
    return this->theGreatestPoem;
}

void Writer:: setYear(int year){
    this->yearOfBirth = year;
}

int Writer:: getYear(){
    return this->yearOfBirth;
}

void Writer:: setRating(float rating){
    this->rating = rating;
}

float Writer:: getRating(){
    return this->rating;
}

QString Writer::caption(){
    return name + " " + surname + " ";
}
