#pragma once
#include <list.h>
#include <stdbool.h>
#include <progbase/console.h>
enum MainCommands
{
    NO,
    LEFT,
    RIGHT,
    UP,
    DOWN,
    ENTER,
    BACK
};

enum MainCommands getMainCommandsUserInput();

void main_menu();

void cleanBuffer();

void printMainMenu(const char *vc[300], int ex,int arr_length);

void shortMessageOrErrorMessage(char *str, enum conAttribute_e x, int status);

void drowABox(int y1, int y2, int x1, int x2, enum conAttribute_e x);

bool areYouSure();


