#pragma once
 #include <stdlib.h>
 #include <stdio.h>
 #include <ctype.h>
 #include <string.h>
 #include <stdbool.h>
 #include <list.h>

int fileExists(const char *fileName);

long getFileSize(const char *fileName);

int readFileToBuffer(const char *fileName, char *buffer, long bufferLength);

void saveToFile(char*text, const char*fileName);

bool ReadingFromFileToStr(char x[500]);

void getNewCSVStrFromListAndSaveToFile(List * self);