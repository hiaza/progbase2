#pragma once
#include <list.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <csv.h>
typedef struct __WritersList WritersList;
typedef struct __Writer Writer;

void WritersList_print(List *self);

Writer *Writer_new();

void Writer_print(Writer *p, int index);

void Writer_free(Writer *x);

int writerToStr(Writer *p, char *buffer, int bufferLength);

Writer *strToWriter(const char name[100], const char theGreatestPoem[100], int yearOfBirth, float rating);
Writer *strToWriter2(char *str);
bool EnterName(char *buffer);
void EnterTheFamoustPoem(char *buffer);
int EnterTheBirthYear();
float EnterTheRating();
void Change_writersField(List *self, int index, int fieldInd);
void printAllBefore(List *self);
CsvRow * fillCsvRowfromWriter(Writer * x);
void fillCsvTableFromWritersList(List*x,CsvTable * b);
void fillWritersListfromCsvTable(CsvTable*self,List * ner);
void getWritersListFromStr(char str[1000],List * listOfWriters);

//struct Writer *rewriteWriter(struct Writer **array, int index, char *str);

//struct Writer *rewriteSomething(struct Writer **array, int index, int pos);