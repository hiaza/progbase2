#include <writers_ext.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <progbase/console.h>
#include <interface.h>
#include <string_buffer.h>
#include <csv.h>

struct __WritersList
{
    Writer *data;
    size_t capasity;
    size_t length; //заповнені, існуючі елементи;
};
struct __Writer
{
    char name[100];
    char theGreatestPoem[100];
    int yearOfBirth;
    float rating;
};


static char *strdup (const char *s) {
    char *d = malloc (strlen (s) + 1);  // Space for length plus nul
    if (d == NULL) return NULL;          // No memory
    strcpy (d,s);                        // Copy the characters
    return d;                            // Return the new string
}

void Writer_print(Writer *self, int index)
{
    if (index == 0)
    {
        Console_setCursorAttribute(FG_YELLOW);
        printf(" %20s |", self->name);
        Console_setCursorAttribute(FG_DEFAULT);
        printf(" %19s |", self->theGreatestPoem);
        printf(" %13i |", self->yearOfBirth);
        printf(" %6.1f |\n", self->rating);
    }
    if (index == 1)
    {
        printf(" %20s |", self->name);
        Console_setCursorAttribute(FG_YELLOW);
        printf(" %19s |", self->theGreatestPoem);
        Console_setCursorAttribute(FG_DEFAULT);
        printf(" %13i |", self->yearOfBirth);
        printf(" %6.1f |\n", self->rating);
    }
    if (index == 2)
    {
        printf(" %20s |", self->name);
        printf(" %19s |", self->theGreatestPoem);
        Console_setCursorAttribute(FG_YELLOW);
        printf(" %13i |", self->yearOfBirth);
        Console_setCursorAttribute(FG_DEFAULT);
        printf(" %6.1f |\n", self->rating);
    }
    if (index == 3)
    {
        printf(" %20s |", self->name);
        printf(" %19s |", self->theGreatestPoem);
        printf(" %13i |", self->yearOfBirth);
        Console_setCursorAttribute(FG_YELLOW);
        printf(" %6.1f |\n", self->rating);
        Console_setCursorAttribute(FG_DEFAULT);
    }
    if (index == -1)
    {
        printf(" %20s |", self->name);
        printf(" %19s |", self->theGreatestPoem);
        printf(" %13i |", self->yearOfBirth);
        printf(" %6.1f |\n", self->rating);
    }
}
void WritersList_print(List *self)
{
    if (self != NULL)
    {
        for (int i = 0; i < List_count(self); i++)
        {
            printf("|  Writer[%i] ", i);
            Writer_print(List_at(self, i), -1);
            puts(" ");
        }
    }
}

Writer *Writer_new()
{
    Writer *x = malloc(sizeof(Writer));
    return x;
}

void Writer_free(Writer *x)
{
    free(x);
}

int writerToStr(Writer *p, char *buffer, int bufferLength)
{
    return snprintf(buffer, bufferLength, "%s,%s,%i,%.1f",
                    p->name,
                    p->theGreatestPoem,
                    p->yearOfBirth,
                    p->rating);
}

void Change_writersField(List *self, int index, int fieldInd)
{
    Writer *x = List_at(self, index);
    {
        if (fieldInd == 0)
        {
            shortMessageOrErrorMessage("Enter the name and surname of Author:", FG_INTENSITY_GREEN, 0);
            char name[100];
            bool n = EnterName(name);
            if (n == false)
            {
                cleanBuffer();
                shortMessageOrErrorMessage("Error: incorrect string", FG_INTENSITY_RED, 1);
                return;
            }
            bool question = areYouSure();
            if(question == true){
                x->name[0] = '\0';
                strcpy(x->name, name);
            }
        }
        //cleanBuffer();
        if (fieldInd == 1)
        {
            shortMessageOrErrorMessage("Enter the Greatest poem:", FG_INTENSITY_GREEN, 0);
            char poem[100];
            EnterTheFamoustPoem(poem);
            bool question = areYouSure();
            if(question == true){
               strcpy(x->theGreatestPoem, poem);
            }
        }
        if (fieldInd == 2)
        {
            shortMessageOrErrorMessage("Enter the year of birth:", FG_INTENSITY_GREEN, 0);
            int year = EnterTheBirthYear();
            if (year == -1)
            {
                cleanBuffer();
                shortMessageOrErrorMessage("Error: incorrect year", FG_INTENSITY_RED, 1);
                return;
            }
            cleanBuffer();
            bool question = areYouSure();
            if(question == true){
               x->yearOfBirth = year;
            }
        }

        //cleanBuffer();
        if (fieldInd == 3)
        {
            shortMessageOrErrorMessage("Enter the rating:", FG_INTENSITY_GREEN, 0);
            float rating = EnterTheRating();
            if (rating == -1)
            {
                cleanBuffer();
                shortMessageOrErrorMessage("Error: incorrect rating", FG_INTENSITY_RED, 1);
                return;
            }
            bool question = areYouSure();
            if(question == true){
                x->rating = rating;
            }
        }
    }
}

Writer *strToWriter2(char *str)
{
    Writer *p = Writer_new();
    sscanf(str, "%99[^,]%*c %99[^,]%*c %i,%f",
           p->name,
           p->theGreatestPoem,
           &(p->yearOfBirth),
           &(p->rating));
    return p;
}
Writer *strToWriter(const char name[100], const char theGreatestPoem[100], int yearOfBirth, float rating)
{
    Writer *p = Writer_new();
    strcpy(p->name, name);
    strcpy(p->theGreatestPoem, theGreatestPoem);
    p->yearOfBirth = yearOfBirth;
    p->rating = rating;
    return p;
}

/*void Writer_print(Writer*p){
        printf("%s,\n%s,\n%i,\n%.1f\n",
        p->name,
        p->theGreatestPoem,
        p->yearOfBirth,
        p->rating);
    }*/
// ====================================================================================
bool EnterName(char buffer[100])
{
    fgets(buffer, 100, stdin);
    buffer[strlen(buffer) - 1] = '\0';
    bool flag = true;
    for (int i = 0; i < strlen(buffer); i++)
    {
        if (!isalpha(buffer[i]) && !isspace(buffer[i]))
        {
            flag = false;
            break;
        }
    }
    if (flag)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void EnterTheFamoustPoem(char *buffer)
{
    fgets(buffer, 100, stdin);
    buffer[strlen(buffer) - 1] = '\0';
}

int EnterTheBirthYear()
{
    char str[5];
    fgets(str, 5, stdin);
    str[4] = '\0';
    int year;
    bool is = true;
    for (int i = 0; i < strlen(str) - 1; i++)
    {
        if (!isdigit(str[i]))
        {
            is = false;
        }
    }
    if (is)
    {
        year = atoi(str);
        if (year >= 0 && year <= 2018)
        {
            return year;
        }
        else
            return -1;
    }
    else
        return -1;
}

float EnterTheRating()
{
    char str[5];
    fgets(str, 5, stdin);
    str[strlen(str) - 1] = '\0';
    bool is = true;
    bool point = true;
    int countP = 0;
    for (int i = 0; i < strlen(str) - 1; i++)
    {
        if (isdigit(str[i]))
        {
        }
        else
        {
            if (str[i] == '.' && point == true)
            {
                point = false;
                countP++;
            }
            else
            {
                if (str[1] == '\0')
                {
                    point = true;
                    break;
                }
                else
                {
                    if (str[i] == '.')
                    {
                        countP++;
                    }
                    else
                    {
                        is = false;
                        break;
                    }
                }
            }
        }
        if ((str[1] != '.' && point == false))
        {
            is = false;
            break;
        }
        else if (countP > 1)
        {
            is = false;
            break;
        }
    }
    if (is)
    {
        float buffer;
        if (point)
        {
            buffer = atoi(str);
        }
        else
        {
            buffer = atof(str);
        }
        if (buffer > 0 && buffer < 10)
        {
            return buffer;
        }
        else
        {
            return -1;
        }
    }
    else
    {
        return -1;
    }
}

void printAllBefore(List *self)
{
    if (self != NULL)
    {
        if(List_count(self)>0){
            shortMessageOrErrorMessage("Enter the year:", FG_INTENSITY_GREEN, 0);
            int year = EnterTheBirthYear();
            if (year == -1)
            {
                cleanBuffer();
                shortMessageOrErrorMessage("Error: incorrect year", FG_INTENSITY_RED, 1);
                return;
            }
            cleanBuffer();

            Console_setCursorPosition(6, 85);
            puts("-----------------------------------------------------------------------------");
            Console_setCursorPosition(7, 85);
            printf("|  №  |   Name and Surname   |    Greatest poem    | year of birth |  rate  | ");
            for (int i = 0; i < List_count(self); i++)
            {
                Writer * temp = List_at(self,i);
                Console_setCursorPosition(9 + 2 * i, 85);
                if (temp->yearOfBirth<=year)
                {
                    Console_setCursorAttribute(FG_INTENSITY_YELLOW);
                    printf("| [%i] |", i);
                    Writer_print(List_at(self, i), -1);
                    Console_setCursorAttribute(FG_DEFAULT);
                    puts(" ");
                    
                }
                else
                {
                    printf("| [%i] |", i);
                    Writer_print(List_at(self, i), -1);
                    puts(" ");
                }
                Console_setCursorPosition(8 + 2 * i, 85);
                puts("-----------------------------------------------------------------------------");
            }
            drowABox(5, 40, 80, 180, BG_INTENSITY_MAGENTA);
            getchar();
        }
        else{
            shortMessageOrErrorMessage("Error: empty list", FG_INTENSITY_RED, 1);
        }

    }
        
}


CsvRow * fillCsvRowfromWriter(Writer * x){
    int bufferLength = 300;
    char buffer[bufferLength];
    char * new = buffer;
    int counter = writerToStr(x,new,bufferLength);
    buffer[counter+1] = '\0';
    //puts(buffer);
    return CsvRow_newFromString(new);
}
void fillCsvTableFromWritersList(List*x,CsvTable * b){
    for(int i = 0;i < List_count(x);i++){
       // puts("hui");
        CsvTable_add(b,fillCsvRowfromWriter(List_at(x,i)));
    }
}

void fillWritersListfromCsvTable(CsvTable*self,List * ner){
    char * nod = CsvTable_toNewString(self);
    char * temp = nod;
    StringBuffer * sb = StringBuffer_new();
    while (*temp != '\0') {
        if(*temp == '\n')
        {
           char * copy = strdup(StringBuffer_toNewString(sb));
           List_add(ner,strToWriter2(copy));
           StringBuffer_clear(sb);
           free(copy);
        }
        else{
            StringBuffer_appendChar(sb,*temp);
        }
        temp++;
   }
   char * copy = strdup(StringBuffer_toNewString(sb));
   List_add(ner,strToWriter2(copy));
   StringBuffer_free(sb);
   free(nod);
   free(copy);

}

void getWritersListFromStr(char str[1000],List * listOfWriters){
    CsvTable * conteiner = CsvTable_newFromString(str);
    fillWritersListfromCsvTable(conteiner,listOfWriters);
    CsvTable_free(conteiner);
}
