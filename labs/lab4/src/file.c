#include <file.h>
#include <stdbool.h>
#include <interface.h>
#include <progbase/console.h>
#include <writers_ext.h>
#include <csv.h>
void saveToFile(char*text, const char*fileName){
    FILE*file = fopen(fileName,"w");
    fputs(text,file);
    fclose(file);
}


int fileExists(const char *fileName)
{
    FILE *f = fopen(fileName, "rb");
    if (!f)
        return 0; // false: not exists
    fclose(f);
    return 1; // true: exists
}

long getFileSize(const char *fileName)
{
    FILE *f = fopen(fileName, "rb");
    if (!f)
        return -1;         // error opening file
    fseek(f, 0, SEEK_END); // rewind cursor to the end of file
    long fsize = ftell(f); // get file size in bytes
    fclose(f);
    return fsize;
}

int readFileToBuffer(const char *fileName, char *buffer, long bufferLength)
{
    FILE *f = fopen(fileName, "rb");
    if (!f)
        return 0; // read 0 bytes from file
    long readBytes = fread(buffer, 1, bufferLength, f);
    fclose(f);
    return readBytes; // number of bytes read
}

bool ReadingFromFileToStr(char x[500]){
    char dir[100];
    dir[0] = '\0';
    fgets(dir, 100, stdin);
    dir[strlen(dir) - 1] = '\0';
    char *filePath = dir;
    bool exists = fileExists(filePath);
    
    if (!exists)
    {
        return false;     //   якщо char * x == NULL Повідомить про це користувача + зробить блок для помилок;
        
    }
    else
    {
        int size = getFileSize(filePath);
        readFileToBuffer(filePath, x, size);
        x[size] = '\0';
    } 
    return true;
}


void getNewCSVStrFromListAndSaveToFile(List * self){
    
        if(List_count(self)==0){
            shortMessageOrErrorMessage("Error: empty list", FG_INTENSITY_RED, 1);
            return;
        }
        shortMessageOrErrorMessage("Enter the name of new file (.txt):", FG_INTENSITY_GREEN, 0);
        
        char fileName[100];
        fgets(fileName,100,stdin);
        fileName[strlen(fileName)-1]='\0';
    
        CsvTable * conteiner = CsvTable_new();
        fillCsvTableFromWritersList(self,conteiner);
        char * temp = CsvTable_toNewString(conteiner);
        saveToFile(temp,fileName);
        free(temp);
        CsvTable_free(conteiner);
    }