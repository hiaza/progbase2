#include <csv.h>
#include <file.h>
#include <interface.h>
#include <limits.h>
#include <progbase.h>
#include <progbase/console.h>
#include <stdbool.h>
#include <time.h>
#include <writers_ext.h>

const char * menu[300] = {
    "Створити новий пустий список сутностей.",
    "Зчитати список сутностей із текстового файлу.",
};

const char * readingTheNameOfFile[300] = {
    "Введіть назву файлу:",
};

const char * menu2[300] = {
    "Додати в обрану позицію списку нову сутність",
    "Видалити сутність із вказаної позиції у списку",
    "Перезаписати всі дані полів сутності у вказаній позиції",
    "Перезаписати обране поле даних сутності із вказаної позиції у списку",
    "Знайти всіх письменників, що народились до року Х",
    "Зберегти поточний список сутностей на файлову систему",
};

enum MainCommands getMainCommandsUserInput() {
    enum MainCommands commands = NO;
    do {
        char keyCode = Console_getChar();
        switch (keyCode) {
        case 127: commands = BACK; break;
        case 68: commands = LEFT; break;
        case 67: commands = RIGHT; break;
        case 65: commands = UP; break;
        case 66: commands = DOWN; break;
        case 10: commands = ENTER; break;
        }
    } while (commands == NO);
    return commands;
}

void cleanBuffer() {
    char c = 0;
    while (c != '\n') {
        c = getchar();
    }
}

void Writers_print(List * self, int index, int cas, int index2);

void drowABox(int y1, int y2, int x1, int x2, enum conAttribute_e x) {
    Console_setCursorAttribute(x);
    for (int i = x1; i <= x2; i++) {
        Console_setCursorPosition(y1, i);
        printf(" ");
        Console_setCursorPosition(y2, i);
        printf(" ");
    }
    for (int y = y1; y <= y2; y++) {
        Console_setCursorPosition(y, x1);
        printf("  \n");
        Console_setCursorPosition(y, x2);
        printf("  \n");
    }
    Console_setCursorAttribute(BG_DEFAULT);
}

bool AddingNewElement(List * name, int index);
void deleteFromList(List * self, int index);
Writer * fillWriter();
void rewriteWriter(List * self, int index);
int Chosenfield(List * writers, int menuInd, int indexWriter);

void printMainMenu(const char * vc[300], int ex, int arr_length) {
    puts(" ");
    puts(" ");
    puts(" ");
    puts(" ");
    puts(" ");
    puts(" ");
    puts(" ");
    puts(" ");
    puts(" ");
    for (int i = 0; i < arr_length; i++) {
        if (i == ex) {
            printf("         ");
            Console_setCursorAttribute(FG_BLUE);
            puts(vc[i]);
            Console_setCursorAttribute(FG_DEFAULT);
        } else {
            printf("         ");
            Console_setCursorAttribute(FG_INTENSITY_CYAN);
            puts(vc[i]);
            Console_setCursorAttribute(FG_DEFAULT);
        }
    }
    drowABox(5, 21, 5, 80, BG_INTENSITY_MAGENTA);
}

static void message() {
    puts(" ");
    puts(" ");
    puts(" ");
    printf("                                                        Welcome to "
           "our database of writers\n");
    printf("                                                        Every "
           "customer is important for us.\n");
    printf("                                                      In case of "
           "unexpected things, write me.\n");
    printf("                                                          My "
           "email: Artem.Trush@meta.ua\n");
    sleepMillis(3000);
    Console_clear();
}
void shortMessageOrErrorMessage(char * str, enum conAttribute_e x, int status) {
    Console_clear();
    Console_setCursorPosition(10, 10);
    Console_setCursorAttribute(x);
    puts(str);
    Console_setCursorAttribute(FG_DEFAULT);
    drowABox(5, 21, 5, 80, BG_INTENSITY_MAGENTA);
    if (status == 1) { sleepMillis(3000); }
    Console_setCursorPosition(11, 10);
}

int ChosenInd(List * writers, int menuInd);

static void main_menu2(int variant) {
    Console_clear();
    List * writers = List_new();
    if (variant == 1) {
        shortMessageOrErrorMessage("Enter the name of your file (.txt):",
                                   FG_INTENSITY_GREEN, 0);
        char str[1000];
        bool file = ReadingFromFileToStr(str);
        if (!file) {
            shortMessageOrErrorMessage("Error, FILE wasn't found",
                                       FG_INTENSITY_RED, 1);
            List_free(writers);
            return;
        }
        getWritersListFromStr(str, writers);
    }
    printMainMenu(menu2, 0, 6);
    Writers_print(writers, -1, 0, 0);
    int index = 0;
    enum MainCommands controle = NO;
    while (controle != BACK) {
        Console_clear();
        printMainMenu(menu2, index, 6);
        Writers_print(writers, 0, 0, 0);
        controle = getMainCommandsUserInput();
        switch (controle) {
        case UP: {
            index -= 1;
            if (index < 0) { index = 6 - 1; }
            break;
        }
        case DOWN: {
            index = (index + 1) % 6;
            break;
        }
        case ENTER: {
            if (index == 0) {
                int indOfWriter;
                if (List_count(writers) == 0)
                    indOfWriter = -1;
                else
                    indOfWriter = ChosenInd(writers, index);
                bool x = AddingNewElement(writers, indOfWriter);
                break;
            }
            if (index == 1) {
                if (List_count(writers) == 0) {
                    shortMessageOrErrorMessage("Empty List", FG_INTENSITY_RED,
                                               1);
                    break;
                } else {
                    int indOfWriter = ChosenInd(writers, index);
                    deleteFromList(writers, indOfWriter);
                }
            }
            if (index == 2) {
                if (List_count(writers) == 0) {
                    shortMessageOrErrorMessage("Empty List", FG_INTENSITY_RED,
                                               1);
                    break;
                } else {
                    int indOfWriter = ChosenInd(writers, index);
                    rewriteWriter(writers, indOfWriter);
                }
            }
            if (index == 3) {
                if (List_count(writers) == 0) {
                    shortMessageOrErrorMessage("Empty List", FG_INTENSITY_RED,
                                               1);
                    break;
                } else {
                    int indOfWriter = ChosenInd(writers, index);
                    if (indOfWriter != -2) {
                        int indOfField =
                            Chosenfield(writers, index, indOfWriter);
                        Change_writersField(writers, indOfWriter, indOfField);
                    }
                }
            }
            if (index == 4) { printAllBefore(writers); }
            if (index == 5) { getNewCSVStrFromListAndSaveToFile(writers); }
            break;
        }
        case BACK:
        case NO:
        case LEFT:
        case RIGHT:
        default: {
            for (int i = List_count(writers) - 1; i >= 0; i--) {
                Writer_free(List_at(writers, i));
            }
            List_free(writers);
            break;
        }
        }
    }
}

void main_menu() {
    Console_clear();
    message();
    printMainMenu(menu, 0, 2);
    int index = 0;
    enum MainCommands controle = NO;
    while (controle != BACK) {
        Console_clear();
        printMainMenu(menu, index, 2);
        controle = getMainCommandsUserInput();
        switch (controle) {
        case UP: {
            index -= 1;
            if (index < 0) { index = 2 - 1; }
            break;
        }
        case DOWN: {
            index = (index + 1) % 2;
            break;
        }
        case ENTER: {
            main_menu2(index);
            break;
        }
        case BACK:
        case NO:
        case LEFT:
        case RIGHT:
        default: break;
        }
    }
}

void rewriteWriter(List * self, int index) {
    if (index == -2) { return; }
    Writer * temp = fillWriter();
    if (temp != NULL) {
        bool save = areYouSure();
        if (save) {
            Writer * removed = List_at(self, index);
            List_set(self, index, temp);
            Writer_free(removed);
        } else
            Writer_free(temp);
    }
}

void deleteFromList(List * self, int index) {
    if (index == -2) { return; }
    bool x = areYouSure();
    if (x == true) {

        Writer * y = List_at(self, index);
        List_removeAt(self, index);
        Writer_free(y);
    }
}

bool AddingNewElement(List * self, int index) {
    if (index == -2) { return false; }
    Writer * temp = fillWriter();
    if (temp != NULL) {
        bool save = areYouSure();
        if (save) {
            if (List_count(self) == 0) {
                List_add(self, temp);
            } else {
                List_insert(self, index + 1, temp);
            }
        } else
            Writer_free(temp);
    }
    return true;
}

Writer * fillWriter() {
    shortMessageOrErrorMessage("Enter the name and surname of Author:",
                               FG_INTENSITY_GREEN, 0);
    char name[100];
    bool n = EnterName(name);
    if (n == false) {
        cleanBuffer();
        shortMessageOrErrorMessage("Error: incorrect string", FG_INTENSITY_RED,
                                   1);
        return NULL;
    }
    // cleanBuffer();
    shortMessageOrErrorMessage("Enter the Greatest poem:", FG_INTENSITY_GREEN,
                               0);
    char poem[100];
    EnterTheFamoustPoem(poem);
    // cleanBuffer();
    shortMessageOrErrorMessage("Enter the year of birth:", FG_INTENSITY_GREEN,
                               0);
    int year = EnterTheBirthYear();
    if (year == -1) {
        cleanBuffer();
        shortMessageOrErrorMessage("Error: incorrect year", FG_INTENSITY_RED,
                                   1);
        return NULL;
    }
    cleanBuffer();
    shortMessageOrErrorMessage("Enter the rating:", FG_INTENSITY_GREEN, 0);
    float rating = EnterTheRating();
    if (rating == -1) {
        cleanBuffer();
        shortMessageOrErrorMessage("Error: incorrect rating", FG_INTENSITY_RED,
                                   1);
        return NULL;
    }
    Writer * temp = strToWriter(name, poem, year, rating);
    return temp;
}

bool areYouSure() {
    shortMessageOrErrorMessage("Save changes?:[ENTER - yes, BACKSPACE - no]",
                               FG_INTENSITY_GREEN, 0);
    enum MainCommands controle = NO;
    while (controle != ENTER && controle != BACK) {
        enum MainCommands controle = getMainCommandsUserInput();
        if (controle == ENTER) { return true; }
        if (controle == BACK) { return false; }
    }
    return true;
}

void Writers_print(List * self, int index, int cas, int index2) {
    if (self != NULL) {
        Console_setCursorPosition(6, 85);
        puts("-----------------------------------------------------------------"
             "------------");
        Console_setCursorPosition(7, 85);
        printf("|  №  |   Name and Surname   |    Greatest poem    | year of "
               "birth |  rate  | ");
        for (int i = 0; i < List_count(self); i++) {
            Console_setCursorPosition(9 + 2 * i, 85);
            if (i == index) {
                if (cas == 1) {
                    printf("| [%i] |", i);
                    Writer_print(List_at(self, i), index2);
                    puts(" ");
                } else {
                    Console_setCursorAttribute(FG_INTENSITY_YELLOW);
                    printf("| [%i] |", i);
                    Writer_print(List_at(self, i), -1);
                    Console_setCursorAttribute(FG_DEFAULT);
                    puts(" ");
                }
            } else {
                printf("| [%i] |", i);
                Writer_print(List_at(self, i), -1);
                puts(" ");
            }
            Console_setCursorPosition(8 + 2 * i, 85);
            puts("-------------------------------------------------------------"
                 "----------------");
        }
        drowABox(5, 40, 80, 180, BG_INTENSITY_MAGENTA);
    }
}

int ChosenInd(List * writers, int menuInd) {
    int index = -1;
    int bound = 0;
    if (menuInd == 0) { bound = -1; }
    enum MainCommands controle = NO;
    while (controle != BACK) {
        Console_clear();
        printMainMenu(menu2, menuInd, 6);
        Writers_print(writers, index, 0, 0);
        controle = getMainCommandsUserInput();
        switch (controle) {
        case UP: {
            index -= 1;
            if (index < bound) { index = List_count(writers) - 1; }
            break;
        }
        case DOWN: {
            index = (index + 1) % List_count(writers);
            break;
        }
        case ENTER: {
            return index;
        }
        case BACK:
        case NO: {
            return -2;
        }
        case LEFT:
        case RIGHT:
        default: { break; }
        }
    }
    return 0;
}

int Chosenfield(List * writers, int menuInd, int indexWriter) {
    int index = 0;
    enum MainCommands controle = NO;
    while (controle != BACK) {
        Console_clear();
        printMainMenu(menu2, menuInd, 6);
        Writers_print(writers, indexWriter, 1, index);
        controle = getMainCommandsUserInput();
        switch (controle) {
        case LEFT: {
            index -= 1;
            if (index < 0) { index = 3; }
            break;
        }
        case RIGHT: {
            index = (index + 1) % 4;
            break;
        }
        case ENTER: {
            return index;
        }
        case BACK:
        case NO: {
            return -2;
        }
        case UP:
        case DOWN:
        default: { break; }
        }
    }
    return 0;
}