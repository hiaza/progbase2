
#define __STDC_WANT_LIB_EXT2__ 1
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <string_buffer.h>
#include <stdio.h>

struct __StringBuffer {
    char * buffer;
    size_t capacity;
    size_t length;
};

enum {
    INITIAL_CAPACITY = 256
};

StringBuffer * StringBuffer_new(void) {
    StringBuffer * self = malloc(sizeof(StringBuffer));
    self->capacity = INITIAL_CAPACITY;
    self->length = 1;
    self->buffer = malloc(sizeof(char) * self->capacity);
    self->buffer[0] = '\0';
    return self;
}

void StringBuffer_free(StringBuffer * self) {
    free(self->buffer);
    free(self);
}

void StringBuffer_appendChar(StringBuffer * self, char ch) {
   // self->capacity = self->capacity+1;
   // self->buffer = realloc(self->buffer,sizeof(char) * self->capacity);
    self->buffer[self->length - 1] = ch;
    self->buffer[self->length] = '\0';
    self->length++;
}

void StringBuffer_append(StringBuffer * self, char * str) {
    size_t strLen = strlen(str);
    size_t newBufferLength = self->length + strLen;
    if (newBufferLength > self->capacity) {
        size_t newCapacity = self->capacity * 2;
        char * newBuffer = realloc(self->buffer, sizeof(char) * newCapacity);
        if (newBuffer == NULL) {
            // out of mem err
            fprintf(stderr, "Out of memory on StringBuffer realloc");
            abort();
        }
        self->buffer = newBuffer;
        self->capacity = newCapacity;
    }
    strcat(self->buffer + (self->length - 1), str);
    self->length += strLen;
}


char * StringBuffer_toNewString(StringBuffer * self) {
    return (self->buffer);
}
char * StringBuffer_toString(StringBuffer * self) {
    return (self->buffer);
}

void StringBuffer_clear(StringBuffer*self){
    self->buffer[0] = '\0';
    self->length = 1;
}