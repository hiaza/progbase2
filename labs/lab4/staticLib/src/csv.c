#include <csv.h>
#include <list.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <string_buffer.h>
#include <stdbool.h>

char *strdup (const char *s) {
    char *d = malloc (strlen (s) + 1);  // Space for length plus nul
    if (d == NULL) return NULL;          // No memory
    strcpy (d,s);                        // Copy the characters
    return d;                            // Return the new string
}

struct __CsvTable {
    List *rows;
};

struct __CsvRow {
    List *values;
};

CsvRow * CsvRow_new(void) {
    CsvRow * self = malloc(sizeof(CsvRow));
    self->values = List_new();
    return self;
}

void CsvRow_free(CsvRow * self) {
    List_free(self->values);
    free(self);
}
void CsvRow_add(CsvRow *self, const char * value) {
    List_add(self->values, (void *)value);
}

static void List_copyFrom(List*self,List*source){
    int count = List_count(source);
    for(int i = 0; i < count; i++){
        List_add(self,List_at(source,i));
    }
}
void CsvTable_values(CsvRow * self, List * values) {
    List_copyFrom(values, self->values);
}



CsvTable * CsvTable_new(void) {
    CsvTable * self = malloc(sizeof(CsvTable));
    self->rows = List_new();
    return self;
}
void CsvTable_free(CsvTable * self) {
    for(int i = 0;i < List_count(self->rows);i++){
         CsvRow * forFree = List_at(self->rows,i);
           int size = List_count(forFree->values);
           for(int j = 0 ; j < size;j++){
               free(List_at(forFree->values,j));
           }
           CsvRow_free(forFree);
    }
    List_free(self->rows);
    free(self);
}

void CsvTable_add (CsvTable * self, CsvRow * row) {
    List_add(self->rows, row);
}

void CsvTable_rows(CsvTable * self, List * rows) {
    List_copyFrom(rows, self->rows);
}



CsvRow * CsvRow_newFromString(char * csvString)
{
    CsvRow * row = CsvRow_new();
    StringBuffer * sb = StringBuffer_new();
    while (*csvString != '\0') {

        if (*csvString == ',') {
    
            char * copy = strdup(StringBuffer_toNewString(sb));
            CsvRow_add(row, copy);
            StringBuffer_clear(sb);     
        } else {
            StringBuffer_appendChar(sb,*csvString);
        }
        csvString++;
   }
  char * copy = strdup(StringBuffer_toNewString(sb));
   CsvRow_add(row, copy);
   StringBuffer_free(sb);
   return row;
}

CsvTable * CsvTable_newFromString(char * csvString) {
    CsvTable * table  = CsvTable_new();
    StringBuffer * sb = StringBuffer_new();
    char *input = csvString;
    while (*input != '\0') {
        if(*input == '\n')
        {
           char * copy = strdup(StringBuffer_toNewString(sb));
            CsvTable_add(table, CsvRow_newFromString(copy));
            free(copy);
            StringBuffer_clear(sb);
        }
        else{
            StringBuffer_appendChar(sb,*input);
        }
        input++;
   }
  char * copy = strdup(StringBuffer_toNewString(sb));
   CsvTable_add(table, CsvRow_newFromString(copy));
   //free(csvString);
   free(copy);
   StringBuffer_free(sb);
    return table;
}

char * CsvTable_toNewString  (CsvTable * self) {
    StringBuffer * sb = StringBuffer_new();
    int nrows = List_count(self->rows);
    for (size_t irow = 0; irow < nrows; irow++) {
        CsvRow *row = List_at(self->rows, irow);
        if (irow != 0) {
            StringBuffer_append(sb, "\n");
        }
        size_t nvalues = List_count(row->values);
        for (size_t ivalue = 0; ivalue < nvalues; ivalue++) {
            char *value = List_at(row->values, ivalue);
            if (ivalue != 0) {
                StringBuffer_append(sb, ",");
            }
            StringBuffer_append(sb, value);
        }
    }
    char * str = strdup((const char *)StringBuffer_toNewString(sb));
    StringBuffer_free(sb);
    return str;
}
