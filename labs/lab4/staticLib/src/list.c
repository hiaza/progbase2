#include <list.h>
#include <assert.h>
#include <string.h>
#include <stdio.h>


struct __List{
    
    size_t capasity;
    size_t length;//заповнені, існуючі елементи;
    void ** array;

};


List * List_new(void){
    List * self = malloc(sizeof(List));
    if (self == NULL) {
        assert(0 && "Out of memory");
        abort();
        return NULL;
    }
    self->capasity = INITIAL_CAPASITY;

    self->length = 0;

    self->array = malloc(sizeof(void*)*INITIAL_CAPASITY);

    if(self->array == NULL){
        free(self);
        assert(0 && "Out of memory");
        abort();
        return NULL;
    }

    return self;
}

void List_free(List * self){
  /*  for(int i = List_count(self);i>0;i--){
         free(self->array[i]);
    }*/
    free(self->array);
    free(self);
}

void List_add(List * self, void * value){

    if(self != NULL){
        if(self->capasity == self->length){
            self -> capasity = self -> capasity * 2;
            self -> array = realloc(self->array, sizeof(void*)*(self->capasity));
            if (self == NULL){
                assert(0 && "Out of memory");
                abort();
            }
        }

        self -> array[self->length] = value;
        self -> length = self->length + 1;
        
    }
}

void List_insert(List * self, size_t index, void * value){
    if(self != NULL && index <= self->length && index >= 0){
        if(index == List_count(self)){
            List_add(self,value);
        }
        else{
            List_add(self,NULL);
            for(int i = self->length-1; i > index; i--){
                self->array[i] = self->array[i-1];
            }
            self->array[index] = value;
        }
    }
    else if(index>self->length || index < 0 ){
        assert(0 && "Invalid index");
    }
}

void * List_removeAt(List * self, size_t index){
    if(self != NULL && index < self->length && index >= 0){
        if(self->length == 1){
            self->length = 0;
            return NULL;
        }
        else{
            for(int i = index; i < self->length-1; i++){
                self->array[i]=self->array[i+1];
            }

            self->length--; 

            if(self->capasity > 16 && (self->capasity)/2 == (self->length)){
                self->capasity = (self->capasity)/2;
                self = realloc(self, sizeof(List)*(self->capasity));
                
            }
            return NULL;
        }
    }
    else if(index>self->length || index < 0 ){
        assert(0 && "Invalid index");
    }
    return NULL;
}

size_t List_count(List * self){
    if(self!=NULL){
        return self->length;
    }
    else return 0;
}

void * List_at(List * self, size_t index){
    if(self == NULL){
        assert(0);
    }
    else{
        if(index >= 0 && index < self->length){
            return self->array[index];
        }
        else assert(0 && "Invalid index");
    }
    return NULL; 
}

void * List_set(List * self, size_t index, void * value){

    if(self == NULL){
        assert(0);
    }
    else{
        if(index >= 0 && index < self->length){
            void * removedData = self->array[index]; 
            self->array[index] = value;
            return removedData;
        }
        else {
            assert(0 && "Invalid index");
        }
    } 
    return NULL;

}