#pragma once
#include <stdio.h>
#include <time.h>
#include <list.h>

typedef struct __CsvTable CsvTable;
typedef struct __CsvRow   CsvRow;

CsvRow * CsvRow_new(void);
void CsvRow_free(CsvRow * self);
void CsvRow_add(CsvRow*self, const char * value);
void CsvTable_values(CsvRow * self, List * values);

CsvTable * CsvTable_new(void);
void CsvTable_free(CsvTable * self);
void CsvTable_add (CsvTable * self, CsvRow * row);
void CsvTable_rows(CsvTable * self, List * rows);

CsvTable * CsvTable_newFromString(char * csvString);
char *     CsvTable_toNewString  (CsvTable * self);
CsvRow * CsvRow_newFromString(char * csvString);