#ifndef FILE_H
#define FILE_H
#include "commonlib_global.h"
#include <fstream>
#include <iostream>
using namespace std;

void saveToFile(const string & fileName, const string & strToSave);
string fromFileToStr(const string & fileName);

bool isSupportedFile(const string & fileName);

#endif // FILE_H
