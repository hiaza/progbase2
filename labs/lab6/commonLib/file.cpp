#include <file.h>

#include <fstream> /* файловий клас */
#include <iostream>
#include <string>
using namespace std;


void saveToFile(const string & fileName, const string & strToSave){
    fstream file ;

    file.open (fileName, fstream::out | fstream::trunc) ;

    file << strToSave ;

    file.close ();

    return ;
}

string fromFileToStr(const string & fileName){

    ifstream file ;

    file.open (fileName) ;

    string temp;

    getline ( file, temp, '\0' );

    file.close () ;
    return temp;
}


bool isSupportedFile(const string & fileName){
    if(fileName.length()<4){
        return false;
    }
  if(fileName.c_str()[fileName.length()-4]=='.'){
      char x[] = "xml";
      for (int i = 0 ; i<3 ; i++){
          if(x[i] != fileName.c_str()[fileName.length() - 3 + i]){
              return false;
          }
      }
      return true;
  }
  else if (fileName.c_str()[fileName.length()-5]=='.'){
      char x[] = "json";
      for (int i = 0 ; i<4 ; i++){
          if(x[i] != fileName.c_str()[fileName.length() - 4 + i]){
              return false;
          }
      }
      return true;
  } else return false;

}


