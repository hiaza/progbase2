#-------------------------------------------------
#
# Project created by QtCreator 2018-05-31T00:56:45
#
#-------------------------------------------------

QT       -= gui

TARGET = commonLib
TEMPLATE = lib

CONFIG += c++11

DEFINES += COMMONLIB_LIBRARY

SOURCES += commonlib.cpp \
    recuest.cpp \
    response.cpp \
    writer.cpp \
    file.cpp

HEADERS += commonlib.h\
        commonlib_global.h \
    file.h \
    recuest.h \
    response.h \
    writer.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
