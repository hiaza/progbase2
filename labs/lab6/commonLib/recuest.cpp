#include "recuest.h"
#include <QObject>

recuest:: recuest(QObject *parent) : QObject(parent)
{

}

recuest:: recuest(recuestType type, int id, Writer *x, QString fileName){
    this->type = type;
    this->id = id;
    this->x = x;
    this->fileName = fileName;
}

recuest::~recuest(){

}

QString recuest:: serializeRecuest(){
 //   "{\"type\":\"select\",\"params\":\"word\",\"text\":\""+ui->lineEdit->text()+"\"}"
    QJsonDocument doc;

    QJsonObject writersObj;

    writersObj.insert("type", this->type);

    if(this->id!=-1){
        writersObj.insert("id", this->id);
    }
    if(this->fileName.length()>1){
        writersObj.insert("fileName", this->fileName);

    }

    if(this->x!=NULL){
              QJsonObject mo;
              mo.insert("name", this->x->getName());
              mo.insert("surname", this->x->getSurName());
              mo.insert("rating", this->x->getRating());
              mo.insert("poem", this->x->getPoem());
              mo.insert("year", this->x->getYear());
              writersObj.insert("writer", mo);
     }

    doc.setObject(writersObj);
    QString temp = QString::fromStdString(doc.toJson().toStdString());
    return temp;
}

recuest * recuest::deserializeRecuest(QString j){
    QJsonParseError err;
    QJsonDocument doc = QJsonDocument::fromJson(
           QByteArray::fromStdString(j.toStdString()),
           &err);
    if (err.error != QJsonParseError::NoError) {
            string errr = err.errorString().toStdString();
            return NULL;
    }

    recuest * m = new recuest();
    m->type = (recuestType)doc.object().value("type").toInt();
    if(doc.object().contains("id")){
        m->id = doc.object().value("id").toInt();
    } else m->id = -1;
    if(doc.object().contains("writer")){
        QJsonObject writerObj = doc.object().value("writer").toObject();
        Writer * g = new Writer(writerObj.value("name").toString(),
                                writerObj.value("surname").toString(),
                                writerObj.value("poem").toString(),
                                writerObj.value("year").toInt(),
                                writerObj.value("rating").toDouble()
                                );
        m->x = g;
    }else m->x = NULL;
    if(doc.object().contains("fileName")){
        m->fileName = doc.object().value("fileName").toString();
    }else m->fileName = "";
    return m;
}
