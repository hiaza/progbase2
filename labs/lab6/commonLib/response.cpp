#include "response.h"

response::response(QObject *parent) : QObject(parent)
{

}
response::response(recuestType fn,int status, int id, QString files, vector<Writer*>writers){
    this->type = fn;
    this->status = status;
    this->id = id;
    this->files = files;
    this->writers = writers;
}

response::~response(){

}

void response::clear(){

}

QString response::serialize(){

        QJsonDocument doc;
        QJsonObject writersObj;
        QJsonArray writersArr;
        if (!this->writers.empty()){
            for (auto & m: this->writers) {
                  QJsonObject mo;
                  mo.insert("name", m->getName());
                  mo.insert("surname", m->getSurName());
                  mo.insert("rating", m->getRating());
                  mo.insert("poem", m->getPoem());
                  mo.insert("year", m->getYear());
                  writersArr.append(mo);
            }
            writersObj.insert("writers", writersArr);
        }

        writersObj.insert("type",this->type);
        writersObj.insert("status",this->status);
        writersObj.insert("id",this->id);

        if(this->files.length()>1){
            writersObj.insert("files",this->files);
        }

        doc.setObject(writersObj);
        QString temp = QString::fromStdString(doc.toJson().toStdString());
        return temp;

}
vector <Writer*> response::deserialiseWriters(string x){
    QJsonParseError err;
    QJsonDocument doc = QJsonDocument::fromJson(
           QByteArray::fromStdString(x),
           &err);
    if (err.error != QJsonParseError::NoError) {
            string errr = err.errorString().toStdString();
            return vector <Writer*> ();
    }
    vector <Writer *> m;
    QJsonArray writersArr = doc.object().value("writers").toArray();
        for (int i = 0; i < writersArr.size(); i++) {
              QJsonValue value = writersArr.at(i);
              QJsonObject writerObj = value.toObject();
              Writer * g = new Writer(writerObj.value("name").toString(),
                                      writerObj.value("surname").toString(),
                                      writerObj.value("poem").toString(),
                                      writerObj.value("year").toInt(),
                                      writerObj.value("rating").toDouble()
                                      );
              m.push_back(g);
        }
        return m;
}
response * response::deserialize(QString j ){
    QJsonParseError err;
    QJsonDocument doc = QJsonDocument::fromJson(
           QByteArray::fromStdString(j.toStdString()),
           &err);
    if (err.error != QJsonParseError::NoError) {
            string errr = err.errorString().toStdString();
            return NULL;
    }

    response * m = new response();

    m->type = (recuestType)doc.object().value("type").toInt();
    m->status = doc.object().value("status").toInt();
    if(doc.object().contains("id")){
        m->id = doc.object().value("id").toInt();
    } else m->id = -1;
    if(doc.object().contains("writers")){
        QJsonArray writersArr = doc.object().value("writers").toArray();
            for (int i = 0; i < writersArr.size(); i++) {
                  QJsonValue value = writersArr.at(i);
                  QJsonObject writerObj = value.toObject();
                  Writer * g = new Writer(writerObj.value("name").toString(),
                                          writerObj.value("surname").toString(),
                                          writerObj.value("poem").toString(),
                                          writerObj.value("rating").toDouble(),
                                          writerObj.value("year").toInt()
                                          );
                  m->writers.push_back(g);
            }
    }else m->writers = vector <Writer*> ();
    if(doc.object().contains("files")){
        m->files = doc.object().value("files").toString();
    }else m->files = "";
    return m;
}

