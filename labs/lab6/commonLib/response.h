#ifndef RESPONSE_H
#define RESPONSE_H
#include "commonlib_global.h"
#include <QObject>
#include <vector>
#include <writer.h>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonParseError>
#include <recuest.h>
class response : public QObject
{
    Q_OBJECT
public:
    recuestType type;
    int status;
    int id;
    QString files;
    vector <Writer*> writers;
    explicit response(QObject *parent = 0);
    response(recuestType fn, int status, int id, QString files, vector<Writer*>writers);
    ~response();
    void clear();
    QString serialize();
    response * deserialize(QString j);
    vector <Writer*> deserialiseWriters(string x);
signals:

public slots:
};

#endif // RESPONSE_H
