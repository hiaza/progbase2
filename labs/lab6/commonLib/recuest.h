#ifndef RECUEST_H
#define RECUEST_H
#include "writer.h"
#include <QObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonParseError>
#include "commonlib_global.h"
typedef enum {
    FN_NONE,
    FN_GET_FILES,
    FN_NEW_LIST,
    FN_LOAD_LIST,
    FN_SAVE_LIST,
    FN_GET_STUDENTS,
    FN_INSERT_STUDENT,
    FN_UPDATE_STUDENT,
    FN_DELETE_STUDENT
}recuestType;



class recuest : public QObject
{

    Q_OBJECT

public:

    recuestType type;
    int id;
    Writer * x;
    QString fileName;

    explicit recuest(QObject *parent = 0);
    recuest(recuestType type, int id , Writer * x, QString fileName);
    ~recuest();
    QString serializeRecuest();
    recuest * deserializeRecuest(QString j);

signals:

public slots:
};

#endif // RECUEST_H
