#include "server.h"
#include <iostream>
#include <QTcpSocket>
#include <file.h>
#include <QDir>
#include <recuest.h>
#include <response.h>
using namespace std;

Server::Server(QObject *parent) : QObject(parent)
{
    tcpServer = new QTcpServer(this);
    connect(tcpServer, SIGNAL(newConnection()), this, SLOT(onNewConnection()));

    int PORT = 3000;
    if (!tcpServer->listen(QHostAddress::Any, PORT)) {
        cerr << "error listen " << endl;
    } else {
        cout << "started at " << PORT << endl;
    }

}

Server:: ~Server(){
    delete tcpServer;
}

void Server::onNewConnection() {
    cout << "got new connection" << endl;
    QTcpSocket * clientSocket = tcpServer->nextPendingConnection();
    connect(clientSocket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
    connect(clientSocket, SIGNAL(bytesWritten(qint64)), this, SLOT(onBytesWritten(qint64)));
    connect(clientSocket, SIGNAL(disconnected()), this, SLOT(onClientDisconnected()));
}

void Server::onReadyRead() {
    QTcpSocket * clientSocket = static_cast<QTcpSocket*>(sender());
    cout << "receiving data" << endl;
    Data = clientSocket->readAll();
    doc = QJsonDocument::fromJson(Data, &docError);
    cout << "Received:" << endl << Data.toStdString() << endl;
    if(docError.errorString().toInt()==QJsonParseError::NoError){
        recuest * test = new recuest();
        if(doc.object().value("type").toInt()==FN_GET_FILES){
            test = test->deserializeRecuest(QString::fromStdString(Data.toStdString()));
                QString line="";
                QDir dir("data");
                bool ok = dir.exists();
                if (ok)
                {
                    dir.setFilter(QDir::Files | QDir::Hidden | QDir::NoSymLinks);
                    dir.setSorting(QDir::Name);
                    QFileInfoList list = dir.entryInfoList();

                    for (int i = 0; i < list.size(); ++i)
                    {
                        QFileInfo fileInfo = list.at(i);
                        line+=fileInfo.fileName()+"1";
                    }
                }
                response*self = new response(FN_GET_FILES,1,test->id, line,vector<Writer*>());
                QString respons = self->serialize();
                clientSocket->write(respons.toUtf8());
                clientSocket->flush();
                delete self;

       }else if(doc.object().value("type").toInt()==FN_LOAD_LIST){
            test = test->deserializeRecuest(QString::fromStdString(Data.toStdString()));

                QString fileN = "./data/"+test->fileName;
                string x = fromFileToStr(fileN.toStdString());
                ifstream temp(fileN.toStdString());
                response*self;
                if(!temp.is_open()){
                    string error = " File didn't exist";
                    self = new response(FN_LOAD_LIST,-1,-1," ",vector <Writer*>());
                }
                else{                  
                    writers = self->deserialiseWriters(x);
                    self = new response(FN_LOAD_LIST,1,-1," ",writers);
                }
                QString respons = self->serialize();
                clientSocket->write(respons.toUtf8());
                clientSocket->flush();
                delete self;
                temp.close();

        }else if(doc.object().value("type").toInt()==FN_SAVE_LIST){
            test = test->deserializeRecuest(QString::fromStdString(Data.toStdString()));

                response*self;
                if (writers.empty()){
                    string err = "error empty vector!";
                    self = new response(FN_SAVE_LIST,-1,-1,QString::fromStdString(err),writers);
                }
                else{
                    QJsonDocument dov;
                    QJsonObject actorsObj;
                    QJsonArray actorsArr;
                    for (Writer* & m: writers) {
                          QJsonObject mo;
                          mo.insert("name", m->getName());
                          mo.insert("surname", m->getSurName());
                          mo.insert("poem",m->getPoem());
                          mo.insert("year",m->getYear());
                          mo.insert("rating", m->getRating());
                          actorsArr.append(mo);
                    }
                    actorsObj.insert("writers", actorsArr);
                    dov.setObject(actorsObj);
                    QString fileN = "./data/"+test->fileName;
                    saveToFile(fileN.toStdString(),dov.toJson().toStdString());
                    self = new response(FN_SAVE_LIST,1,-1,"successfully saved",vector <Writer*>());
                }
                QString respons = self->serialize();
                clientSocket->write(respons.toUtf8());
                clientSocket->flush();
                delete self;

        } else if(doc.object().value("type").toInt()==FN_INSERT_STUDENT){
            test = test->deserializeRecuest(QString::fromStdString(Data.toStdString()));

                    writers.push_back(test->x);
                    response*self;
                    vector <Writer*> temp;
                    temp.push_back(test->x);
                    self = new response(FN_INSERT_STUDENT,1,writers.size()-1," ",temp);
                    QString respons = self->serialize();
                    clientSocket->write(respons.toUtf8());
                    clientSocket->flush();
                    delete self;
        }else if(doc.object().value("type").toInt()==FN_UPDATE_STUDENT){

           test = test->deserializeRecuest(QString::fromStdString(Data.toStdString()));
                    //qDebug() << test->x->getRating();
                    writers.at(test->id) = test->x;
                    vector <Writer*> temp;
                    temp.push_back(test->x);
                    response*self = new response(FN_UPDATE_STUDENT,1,test->id," ",temp);
                    QString respons = self->serialize();
                    clientSocket->write(respons.toUtf8());
                    clientSocket->flush();
                    delete self;
        }else if(doc.object().value("type").toInt()==FN_DELETE_STUDENT){

            test = test->deserializeRecuest(QString::fromStdString(Data.toStdString()));
                     //qDebug() << test->x->getRating();
                     qDebug() << writers.size();
                     writers.erase(writers.begin()+test->id);
                     response* self= new response(FN_DELETE_STUDENT,1,test->id," ",vector <Writer*>());
                     QString respons = self->serialize();
                     clientSocket->write(respons.toUtf8());
                     clientSocket->flush();
                     delete self;
         }else if(doc.object().value("type").toInt()==FN_NEW_LIST){
            test = test->deserializeRecuest(QString::fromStdString(Data.toStdString()));
                     writers.erase(writers.begin(),writers.end());
                     qDebug() << writers.size();
                     response* self= new response(FN_NEW_LIST,1,-1," ",vector <Writer*>());
                     QString respons = self->serialize();
                     clientSocket->write(respons.toUtf8());
                     clientSocket->flush();
                     delete self;
          }
          delete test;
    }

}

void Server::onBytesWritten(qint64 n) {
    cout << "bytes written" << endl;
}

void Server::onClientDisconnected() {
    cout << "Client disconnected " << endl;
    QTcpSocket * clientSocket = static_cast<QTcpSocket*>(sender());
    clientSocket->close();
}
