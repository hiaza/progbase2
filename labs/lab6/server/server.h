#ifndef SERVER_H
#define SERVER_H
#include <QTcpServer>
#include <QJsonObject>
#include <QJsonParseError>
#include <QJsonDocument>
#include <QObject>
#include "writer.h"
class Server : public QObject
{
    QTcpServer * tcpServer;
    QByteArray Data;
    QJsonDocument doc;
    QJsonParseError docError;
    Q_OBJECT
public:
    explicit Server(QObject *parent = nullptr);
    ~Server();
    std::vector <Writer*> writers;
signals:

public slots:
    void onNewConnection();
    void onReadyRead();
    void onBytesWritten(qint64);
    void onClientDisconnected();
};

#endif // SERVER_H
