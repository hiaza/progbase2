#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QListWidgetItem>
#include <QListWidget>
#include "writer.h"
#include <iostream>
#include <QMessageBox>
#include <QCloseEvent>
#include "edit.h"
#include <vector>
#include <iostream>
#include "file.h"
#include <QTcpSocket>
#include <QHostAddress>
#include <QJsonArray>
#include "recuest.h"
#include "response.h"
using namespace std;
MainWindow::MainWindow(QHostAddress addr, int port, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    client = new QTcpSocket(this);
    PORT = port;
    serverAddress = addr;

    connect(client, SIGNAL(connected()), this, SLOT(onConnected()));
    connect(client, SIGNAL(bytesWritten(qint64)), this, SLOT(onBytesWritten(qint64)));
    connect(client, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
    connect(client, SIGNAL(disconnected()), this, SLOT(onDisconnected()));

}

MainWindow::~MainWindow()
{
    delete client;
    delete ui;
}

bool MainWindow:: testConnection(){
    client->connectToHost(serverAddress, PORT);
    if(!client->waitForConnected(1000)){
        QMessageBox::critical(this,"Error","Could not connect to server",QMessageBox::Abort);
        return false;
    }
    recuest * test = new recuest(FN_NEW_LIST, 1, NULL ,""); // filename який буде оголошений в класі .h
    QString x = test->serializeRecuest();
    client->write(x.toUtf8());
    client->flush();
    return true;
}

void MainWindow::onConnected() {
    cout << "connected" << endl;
   // QMessageBox::information(this,"message","You are connected now");
}
 // if doc.object().value("type").toInt != FN_getfiles do disconect

void MainWindow::onReadyRead() {
    cout << "ready to read" << endl;

    client->waitForReadyRead(500);
    if(!ComplexData){
        Data = client->readAll();
    }else{
        Data.append(client->readAll());
        ComplexData = false;
    }

    doc = QJsonDocument:: fromJson(Data, &docError);
    if(docError.errorString().toInt() == QJsonParseError::NoError){

        response * x = new response();

        if(doc.object().value("type").toInt() == FN_GET_FILES){
            x = x->deserialize(QString::fromStdString(Data.toStdString()));
            if(x->status == 1){
                files = x->files.toStdString();
                if(files.length()>1){
                       if(!client->isOpen()){
                            client->connectToHost(serverAddress, PORT);
                            client->waitForConnected(500);
                       }
                       if(x->id==1){
                            fileMenu temp(QString::fromStdString(files),false,this);
                            temp.exec();
                            if(temp.accepted == true){
                                recuest * test = new recuest(FN_LOAD_LIST, -1, NULL , temp.FileName); // filename який буде оголошений в класі .h
                                QString x = test->serializeRecuest();
                                cout << "Sending: " << endl << x.toStdString() << endl;
                                client->write(x.toUtf8());
                                client->flush();
                                client->waitForBytesWritten(1000);
                                delete test;
                               }else client->disconnectFromHost();
                       }else if(x->id == 2){
                                fileMenu temp(QString::fromStdString(files),true,this);
                                temp.exec();
                                if(temp.accepted == true){
                                recuest * test = new recuest(FN_SAVE_LIST, -1, NULL, temp.FileName); // filename який буде оголошений в класі .h
                                QString x = test->serializeRecuest();
                                cout << "Sending: " << endl << x.toStdString() << endl;
                                client->write(x.toUtf8());
                                client->flush();
                                client->waitForBytesWritten(1000);
                                delete test;
                            }else client->disconnectFromHost();
                        }
                    }

            }
        }
        else if(doc.object().value("type").toInt() == FN_LOAD_LIST){
            x = x->deserialize(QString::fromStdString(Data.toStdString()));

            if(x->status == 1){
                vector <Writer*> m;
                ui->ListM->clear();
                for(int i = 0; i < x->writers.size();i++){
                     m.push_back(x->writers.at(i));
                }
                 int counter = m.size();

                        cout << "counter:" << endl << counter << endl;

                        for(int i = 0;i<counter;i++){
                            Writer * s  = m.at(i);
                            QListWidgetItem * item = new QListWidgetItem();
                            item->setText(s->caption());
                            QVariant variant;
                            variant.setValue (s);
                            item->setData(Qt::UserRole,variant);
                            ui->ListM->addItem(item);
                        }
                        
            }else{
                QMessageBox::critical(this,"Error","Error with file");
            }
        }else if(doc.object().value("type").toInt() == FN_SAVE_LIST){
            x = x->deserialize(QString::fromStdString(Data.toStdString()));
            if(x->status == 1){
                QMessageBox::information(this,"Success!",x->files);

            }else if (x->status == -1){
                QMessageBox::critical(this,"Error!",x->files);
            }
            files = "";

        }else if(doc.object().value("type").toInt() == FN_NEW_LIST){
            x = x->deserialize(QString::fromStdString(Data.toStdString()));
            ui->ListM->clear();
            ui->listWidget->clear();
            cleanLabels();
        }else if(doc.object().value("type").toInt() == FN_INSERT_STUDENT){
                x = x->deserialize(QString::fromStdString(Data.toStdString()));
                if(x->status == 1){
                Writer * m = x->writers.at(0);
                QListWidgetItem * item = new QListWidgetItem();
                item->setText(m->caption());
                QVariant variant;
                variant.setValue (m);
                item->setData(Qt::UserRole,variant);
                ui->ListM->addItem(item);
            }

        }else if(doc.object().value("type").toInt() == FN_UPDATE_STUDENT){
            x = x->deserialize(QString::fromStdString(Data.toStdString()));
            if(x->status == 1){
            Writer * m = x->writers.at(0);
            QListWidgetItem * item = ui->ListM->item(x->id);
            item->setText(m->caption());
            QVariant variant;
            variant.setValue (m);
            item->setData(Qt::UserRole,variant);
           }
            client->disconnectFromHost();
        }else if(doc.object().value("type").toInt() == FN_DELETE_STUDENT){
            x = x->deserialize(QString::fromStdString(Data.toStdString()));
            if(x->status == 1){
            QListWidgetItem * item = ui -> ListM ->takeItem(x->id);
            cleanLabels();
            ui->listWidget->clear();
            delete item;
           }
        }else{
            ComplexData = true;
            onReadyRead();
        }delete x;

        if (doc.object().value("type").toInt() != FN_GET_FILES && ComplexData == false){
            client->disconnectFromHost();
        }
        qDebug() << "here we go";
    }else{
        QMessageBox::critical(this,"Error","Parsing Error");
    }
    cout << "Received:" << endl << Data.toStdString() << endl;

}

void MainWindow::onBytesWritten(qint64 n) {
    cout << "bytes  written " << endl;
}

void MainWindow::onDisconnected() {
    cout << "disconnected" << endl;
}


void MainWindow::on_addButton_clicked()
{
    Edit dialog(this);
    dialog.exec();
    if(dialog.accept){
         Writer * s = new Writer(dialog.name(),dialog.surname(),dialog.poem(),dialog.year(),dialog.rate());
         client->connectToHost(serverAddress, PORT);
         client->waitForConnected(500);
         recuest * test = new recuest(FN_INSERT_STUDENT, -1, s ," "); // filename який буде оголошений в класі .h
         QString x = test->serializeRecuest();
         cout << "Sending: " << endl << x.toStdString() << endl;
         client->write(x.toUtf8());
         client->flush();
         delete test;
    }
    cleanLabels();
}

void MainWindow::on_ListM_itemClicked(QListWidgetItem *item)
{
    QList<QListWidgetItem*> selectedItems = ui->ListM->selectedItems();
    if(selectedItems.count()!=0){
        ui->removeButton->setEnabled(true);
        ui->editButton ->setEnabled(true);
        QVariant variant = item->data(Qt::UserRole);
        Writer * s = variant.value<Writer*>();
        ui->label_name->setText(s->getName());
        ui->label_surname->setText(s->getSurName());
        ui->label_poem->setText(s->getPoem());
        ui->label_year->setText(QString::number(s->getYear()));
        ui->label_rate->setText(QString::number(s->getRating()));
    }
    else{
        ui->removeButton->setEnabled(true);
        ui->editButton ->setEnabled(true);
    }
}





void MainWindow::cleanLabels(){
    ui->label_name->setText("Choose element");
    ui->label_surname->setText("");
    ui->label_poem->setText("");
    ui->label_year->setText("");
    ui->label_rate->setText("");
}



void MainWindow::on_removeButton_clicked()
{
    QList<QListWidgetItem *> selectedItems = ui->ListM->selectedItems();
    if(selectedItems.count()!=0){
        QListWidgetItem * selectedItem = selectedItems.at(0);
        int selectedRow = ui->ListM->row(selectedItem);
        client->connectToHost(serverAddress, PORT);
        client->waitForConnected(500);
        recuest * test = new recuest(FN_DELETE_STUDENT, selectedRow, NULL ," "); // filename який буде оголошений в класі .h
        QString x = test->serializeRecuest();
        cout << "Sending: " << endl << x.toStdString() << endl;
        client->write(x.toUtf8());
        client->flush();
        ui->removeButton->setEnabled(false);
        ui->editButton ->setEnabled(false);
        ui->listWidget->clear();
        cleanLabels();
        delete test;
    }
    else{
        //
    }
}


void MainWindow::on_editButton_clicked()
{
    QList<QListWidgetItem*> selectedItems = ui->ListM->selectedItems();
    if(selectedItems.count() == 1){
        int selected = ui->ListM->currentRow();
        qDebug() << selected;
        QListWidgetItem * item = selectedItems.at(0);
        QVariant variant = item->data(Qt::UserRole);
        Writer * s = variant.value<Writer*>();
        Edit dialog(this);
        dialog.setName(s->getName());
        dialog.setSurname(s->getSurName());
        dialog.setPoem(s->getPoem());
        dialog.setYear(s->getYear());
        dialog.setRate(s->getRating());
        dialog.exec();
        if(dialog.accept){

            Writer * red = new Writer(dialog.name(),dialog.surname(),dialog.poem(),dialog.year(),dialog.rate());
            client->connectToHost(serverAddress, PORT);
            client->waitForConnected(500);
            recuest * test = new recuest(FN_UPDATE_STUDENT, selected, red ," "); // filename який буде оголошений в класі .h
            QString x = test->serializeRecuest();
            cout << "Sending: " << endl << x.toStdString() << endl;
            client->write(x.toUtf8());
            client->flush();
            delete test;
            ui->listWidget->clear();
        }
        ui->removeButton->setEnabled(false);
        ui->editButton ->setEnabled(false);
        cleanLabels();
    }
    else{
      //
    }
}


void MainWindow::on_findButton_clicked()
{
    int to = ui->yearVal->value();
    int counter = ui->ListM->count();
    ui->listWidget->clear();
    for(int i = 0;i<counter;i++){
        QListWidgetItem * item  = ui->ListM->item(i);
        QVariant variant = item->data(Qt::UserRole);
        Writer * s = variant.value<Writer*>();
        if(s->getYear()<=to){
            QListWidgetItem*real = new QListWidgetItem();
            real->setText(s->caption());
            ui->listWidget->addItem(real);
        }
    }
}

void MainWindow::closeEvent(QCloseEvent*ev){
    int res = QMessageBox::question(this,
                                                            "Exit","Are you sure?",
                                                            QMessageBox::Yes |QMessageBox::No,
                                                            QMessageBox::No);
    if (res == QMessageBox::Yes){
        ev->accept();
    }
    else {
        ev->ignore();
    }
}

void MainWindow::on_actionSave_in_Json_triggered()
{
    files = " ";
    client->connectToHost(serverAddress, PORT);
    client->waitForConnected(500);
    recuest * test = new recuest(FN_GET_FILES, 2, NULL ,""); // filename який буде оголошений в класі .h
    QString x = test->serializeRecuest();
    cout << "Sending: " << endl << x.toStdString() << endl;
    client->write(x.toUtf8());
    client->flush();
    delete test;
}



void MainWindow::on_actionOpen_Json_triggered()
{
    files = " ";
    client->connectToHost(serverAddress, PORT);
    client->waitForConnected(500);
    recuest * test = new recuest(FN_GET_FILES, 1, NULL ,""); // filename який буде оголошений в класі .h
    QString x = test->serializeRecuest();
    cout << "Sending: " << endl << x.toStdString() << endl;
    client->write(x.toUtf8());
    client->flush();
    delete test;
}

void MainWindow::on_actionNew_file_triggered()
{
    client->connectToHost(serverAddress, PORT);
    client->waitForConnected(500);
    recuest * test = new recuest(FN_NEW_LIST, 1, NULL ,""); // filename який буде оголошений в класі .h
    QString x = test->serializeRecuest();
    cout << "Sending: " << endl << x.toStdString() << endl;
    client->write(x.toUtf8());
    client->flush();
}
