#include "connection.h"
#include "ui_connection.h"

connection::connection(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::connection)
{
    accepted = false;
    ui->setupUi(this);
    ui->ip_line->setText("127.0.0.1");
    ui->port_line->setText("3000");
}

connection::~connection()
{
    delete ui;
}

void connection::on_buttonBox_accepted()
{
    PORT = ui->port_line->text().toInt();
    address = ui -> ip_line->text();
    accepted = true;
}

void connection::on_ip_line_textChanged(const QString &arg1)
{
    validate();
}

void connection::on_port_line_textChanged(const QString &arg1)
{
    validate();
}

void connection::validate(){
    bool invalidInput = ui ->ip_line->text().isEmpty()
            || ui->port_line->text().isEmpty();
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(!invalidInput);
}
