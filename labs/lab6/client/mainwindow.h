#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidgetItem>
#include <QTcpSocket>
#include <QJsonObject>
#include <QJsonParseError>
#include <QJsonDocument>
#include <QHostAddress>
#include <QJsonArray>
#include <filemenu.h>
#include "edit.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    QHostAddress serverAddress;
    int PORT;
    QJsonDocument doc;
    QJsonParseError docError;
    QByteArray Data;
    std::string files;
    Q_OBJECT

public:
    explicit MainWindow(QHostAddress addr, int port, QWidget *parent = 0);
    ~MainWindow();
    bool testConnection();
    bool ComplexData = false;
private slots:


    void onConnected();

    void onReadyRead();

    void onBytesWritten(qint64);

    void onDisconnected();

    void on_addButton_clicked();

    void on_ListM_itemClicked(QListWidgetItem *item);

    void on_removeButton_clicked();

    void on_editButton_clicked();

    void cleanLabels();

    void closeEvent(QCloseEvent*ev);

    void on_findButton_clicked();

    void on_actionSave_in_Json_triggered();

    void on_actionOpen_Json_triggered();

    void on_actionNew_file_triggered();

private:
    QTcpSocket * client;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
