#include "mainwindow.h"
#include <QApplication>
#include <connection.h>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    connection x;
    int i = x.exec();
    while(i != x.Rejected){
        if(x.accepted){
            //authorization.hide();
            //qDebug() << id;
            MainWindow w(x.address, x.PORT);
            if(w.testConnection()){
                w.show();
                a.exec();
            }
            else return 0;
            break;
        }
        i = x.exec();
    }
    return 0;
}
