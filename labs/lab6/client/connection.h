#ifndef CONNECTION_H
#define CONNECTION_H

#include <QDialog>
#include <QHostAddress>
#include <QPushButton>
namespace Ui {
class connection;
}

class connection : public QDialog
{
    Q_OBJECT

public:
    explicit connection(QWidget *parent = 0);
    ~connection();
    bool accepted;
    QHostAddress address;
    int PORT;
private slots:
    void validate();

    void on_buttonBox_accepted();

    void on_ip_line_textChanged(const QString &arg1);

    void on_port_line_textChanged(const QString &arg1);

private:
    Ui::connection *ui;
};

#endif // CONNECTION_H
