#ifndef FILEMENU_H
#define FILEMENU_H

#include <QDialog>
#include <QListWidgetItem>
namespace Ui {
class fileMenu;
}

class fileMenu : public QDialog
{
    Q_OBJECT

public:
    explicit fileMenu(QString files, bool status, QWidget *parent = 0);
    ~fileMenu();
    QString FileName;
    bool accepted;


private slots:

    void on_ListM_itemClicked(QListWidgetItem *item);

    void on_buttonBox_accepted();

    void on_pushButton_clicked();

    void on_lineEdit_textChanged(const QString &arg1);

private:
    Ui::fileMenu *ui;
};

#endif // FILEMENU_H
