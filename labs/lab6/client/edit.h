#ifndef EDIT_H
#define EDIT_H

#include <QDialog>

namespace Ui {
class Edit;
}

class Edit : public QDialog
{
    Q_OBJECT

public:
    explicit Edit(QWidget *parent = 0);
    void setName(QString name);
    QString name();
    void setSurname(QString surname);
    QString surname();
    void setPoem(QString poem);
    QString poem();
    void setYear(int year);
    int year();
    void setRate(float rate);
    float rate();
    ~Edit();
    bool accept;

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

    void on_checkBox_clicked();

    void on_ed_name_textChanged(const QString &arg1);

    void on_ed_surname_textChanged(const QString &arg1);

    void on_ed_poem_textChanged(const QString &arg1);

    void validate();

private:
    Ui::Edit *ui;
};

#endif // EDIT_H
