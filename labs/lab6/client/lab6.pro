#-------------------------------------------------
#
# Project created by QtCreator 2018-04-15T16:55:27
#
#-------------------------------------------------

QT       += core gui network

CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = lab6
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    edit.cpp \
    filemenu.cpp \
    connection.cpp

HEADERS  += mainwindow.h \
    edit.h \
    filemenu.h \
    connection.h


FORMS    += mainwindow.ui \
    edit.ui \
    filemenu.ui \
    connection.ui

RESOURCES += \
    resource.qrc

INCLUDEPATH += ../commonLib
LIBS += -L ../build-commonLib-Desktop-Debug/ -lcommonLib


