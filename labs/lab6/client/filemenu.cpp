#include "filemenu.h"
#include "ui_filemenu.h"
#include <vector>
#include <iostream>
#include <QListWidgetItem>
#include <QVariant>
using namespace std;


vector <QString> cropped(QString files);

fileMenu::fileMenu(QString files, bool status, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::fileMenu)
{
    accepted = false;
    ui->setupUi(this);
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
    vector <QString> x = cropped(files);
    if(x.size()>0){
        for(int i = 0; i<x.size();i++){
            QListWidgetItem * item = new QListWidgetItem();
            item->setText(x.at(i));
            ui->ListM->addItem(item);
        }
    }
    ui->pushButton->setEnabled(status);
    ui->lineEdit->setEnabled(status);
}

fileMenu::~fileMenu()
{
    delete ui;
}

vector <QString> cropped(QString files){
    int i = 0;
    int j = 0;
    int counter = 0;
    QString self = files;
    vector <QString> temp;
    while(i!=files.size()){
        if(files.at(i)=='1'){
            temp.push_back(self.mid(counter, j));
            j = 0;
            self = files;
            counter = i+1;
            i++;
        }
        else{
            i++;
            j++;
        }
    }
    return temp;
}


void fileMenu::on_ListM_itemClicked(QListWidgetItem *item)
{
    QList<QListWidgetItem*> selectedItems = ui->ListM->selectedItems();
    if(selectedItems.count()!=0){
         ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
         FileName = item->text();
    }
    else{
         ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
    }
}

void fileMenu::on_buttonBox_accepted()
{
    accepted = true;
}

void fileMenu::on_pushButton_clicked()
{
    QString file = ui -> lineEdit -> text();
    FileName = file + ".json";
    if(FileName.length()>5){
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
    }else{
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
    }
}

void fileMenu::on_lineEdit_textChanged(const QString &arg1)
{
    bool invalidInput = ui ->lineEdit->text().isEmpty();
    ui->pushButton->setEnabled(!invalidInput);
}
